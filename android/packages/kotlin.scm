;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android packages kotlin)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages java)
  #:use-module (gnu packages java-xml)
  #:use-module (gnu packages xml)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system ant)
  #:use-module (guix build-system trivial)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (android packages intellij)
  #:use-module (android packages java))

;; This module defines the bootstrap path of Kotlin, using archeology.
;; The initial bootstrap is inspired by the work of Emanuel Bourg at
;; https://github.com/ebourg/kotlin-bootstrapping.
;;
;; We use a chain of compilers, starting from the first version that does not
;; require an existing version of Kotlin.

;; Some kotlin-specific dependencies

(define-public ant-contrib
  (package
    (name "ant-contrib")
    (version "0.6")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://downloads.sourceforge.net/ant-contrib/ant-contrib/ant-contrib-"
                                  version "/ant-contrib-" version "-src.tar.gz"))
              (sha256
               (base32
                "082pjgr7m4jzn5w0ixhbqvr3l7i978dxcbz5mhy6hgcrwbxpq2n2"))))
    (build-system ant-build-system)
    (arguments
     `(#:build-target "all"
       #:make-flags '("-Dversion=0.6")
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'chdir
           (lambda _
             (chdir "..")))
         (replace 'install (install-jars "build/lib")))))
    (inputs
     `(("java-xerces" ,java-xerces)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl1.1)))

(define-public java-dart-ast-for-kotlin-0.6.786
  (package
    (name "java-dart-ast")
    (version "0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/JetBrains/kotlin/raw/"
                                  "build-0.6.786/js/js.translator/lib/src/"
                                  "dart-ast-src.jar"))
              (sha256
               (base32
                "01b8wpddxk7rpvhak82z025jcam4jqr0x7grj1hkz8iaq8haqyw1"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "dart-ast.jar"
       ;; No tests
       #:tests? #f))
    (propagated-inputs
     `(("intellij-sdk-133-util" ,intellij-sdk-133-util)
       ("intellij-sdk-133-annotations" ,intellij-sdk-133-annotations)))
    (inputs
     `(("java-jaxen" ,java-jaxen)
       ("unzip" ,unzip)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:bsd-3)))

;; XXX: This is probably bad, as the URLs are unversionned.
(define-public kotlin-jdk-annotations
  (origin
    (method url-fetch)
    (uri "https://teamcity.jetbrains.com/guestAuth/repository/download/Kotlin_KAnnotator_InferJdkAnnotations/shipWithKotlin.tcbuildtag/kotlin-jdk-annotations.jar")
    (sha256
     (base32
      "0sqvnizrm7k4vif9ywxglyrdvswsafsv2is3xqv91lz7zy4qgc54"))))

(define-public kotlin-android-sdk-annotations
  (origin
    (method url-fetch)
    (uri "https://teamcity.jetbrains.com/guestAuth/repository/download/Kotlin_KAnnotator_InferJdkAnnotations/shipWithKotlin.tcbuildtag/kotlin-android-sdk-annotations.jar")
    (sha256
     (base32
      "1bw2xb1lvyybkgpk2r6qxb51qygnmy5lxl29sips2ybjc11y5k7b"))))

(define* (kotlin-source version hash directories-to-create #:key (patches '()))
  "Return the origin record for the sources of a given version of Kotlin."
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://github.com/JetBrains/kotlin")
           (commit (string-append "build-" version))))
    (file-name (git-file-name "kotlin" version))
    (sha256 (base32 hash))
    (patches patches)
    (modules '((guix build utils)))
    (snippet
      `(begin
         (for-each delete-file (find-files "." ".*.jar$"))
         ;; TODO: Remove these files, but they are needed by the
         ;; process that generate them...
         ;(for-each delete-file (find-files "." ".*Generated.java$"))

         ;; jslib requires closure, wich we do not have, and is not
         ;; needed for the bootstrap chain.
         (substitute* "build.xml"
           ((",jslib") "")
           (("duplicate=\"fail\"") "duplicate=\"preserve\""))

         (for-each
           mkdir-p
           (list ,@directories-to-create))))))

(define (dependency-name dependency)
  (match dependency
    (((? package? dependency) _ ...)
     (package-name dependency))
    (((? origin? dependency) _ ...)
     (basename (origin-uri dependency)))
    ((? package? dependency)
     (package-name dependency))
    ((? origin? dependency)
     (basename (origin-uri dependency)))))

(define* (kotlin-package version hash directories-to-create flags inputs javac2
                         #:key (patches '()) (bootstrap #f) (use-annotations? #t)
                               (noverify? #t) (ant-directory "dependencies/ant-1.7/lib"))
  "Return a package for a given Kotlin version."
  (package
    (name "kotlin")
    (version version)
    (source (kotlin-source version hash directories-to-create #:patches patches))
    (build-system ant-build-system)
    (arguments
     `(#:build-target "dist"
       #:make-flags
       ,#~(list
            #$@(if bootstrap
                   (list
                     #~(string-append "-Dbootstrap.compiler.home="
                                      #$bootstrap "/lib/kotlin"))
                   '())
            #$@flags)
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         ,@(if noverify?
                `((add-before 'configure 'no-verify
                    (lambda _
                      (setenv "ANT_OPTS" "-noverify"))))
               '())
         (replace 'configure
           (lambda* (#:key inputs #:allow-other-keys)
             (setenv "JAVA_HOME" (assoc-ref inputs "jdk"))
             (setenv "CLASSPATH" (string-append
                                   (assoc-ref inputs "ant-contrib")
                                   "/share/java/ant-contrib-0.6.jar"))))
         (add-before 'build 'copy-jars
           (lambda* (#:key inputs #:allow-other-keys)
             (for-each
               (lambda (file target)
                 (copy-file file target))
               (list
                 ,@(map (match-lambda
                         ((package jar target)
                          `(car (find-files (assoc-ref inputs ,(dependency-name package)) ,jar))))
                       inputs))
               (list
                 ,@(map (match-lambda
                          ((package jar target) target))
                        inputs)))
             (copy-file
               (car (find-files (assoc-ref inputs "ant") "ant.jar$"))
               (string-append ,ant-directory "/ant.jar"))
             (for-each
               (lambda (file)
                 (copy-file file (string-append "ideaSDK/lib/" (basename file))))
               (find-files (assoc-ref inputs ,(package-name javac2)) "\\.jar$"))))
         ,@(if use-annotations?
               `((add-before 'build 'copy-annotations
                   (lambda* (#:key inputs #:allow-other-keys)
                     (copy-file
                       (assoc-ref inputs "kotlin-android-sdk-annotations.jar")
                       "dependencies/annotations/kotlin-android-sdk-annotations.jar")
                     (copy-file
                       (assoc-ref inputs "kotlin-jdk-annotations.jar")
                       "dependencies/annotations/kotlin-jdk-annotations.jar"))))
               '())
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin"))
                    (kotlin (string-append out "/lib/kotlin")))
               (mkdir-p bin)
               (mkdir-p kotlin)
               (install-file "dist/kotlinc/bin/kotlinc-jvm" bin)
               (copy-recursively "dist/kotlinc" kotlin)
               (substitute* (string-append bin "/kotlinc-jvm")
                 (("/lib/") "/lib/kotlin/"))))))))
    (inputs
      (map
        (lambda (input)
          (list (dependency-name input) input))
        (let* ((inputs (map car inputs))
               (inputs (if bootstrap (cons bootstrap inputs) inputs))
               (inputs (if use-annotations?
                           (cons* kotlin-android-sdk-annotations
                                  kotlin-jdk-annotations
                                  inputs)
                           inputs)))
          inputs)))
    (native-inputs
     `(("ant-contrib" ,ant-contrib)
       ,(list (package-name javac2) javac2)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

;; In the following, we define the set of dependencies for kotlin as a list
;; of triples.  Each triple is a package, the exact name of the jar file we
;; want from this package, and the path to which we copy the jar file in the
;; Kotlin build directory.
;;
;; Following versions of Kotlin have different dependencies, that extend or
;; modify this first one.
;;
;; Each version of kotlin is defined in terms of KOTLIN-PACKAGE.

(define kotlin-dependencies-0.6.786
  `((,intellij-sdk-133-annotations "annotations.jar" "ideaSDK/core/annotations.jar")
    (,intellij-sdk-133-intellij-core "intellij-core.jar" "ideaSDK/core/intellij-core.jar")
    (,java-asm4-for-intellij-133 "asm-6.0.jar" "ideaSDK/lib/jetbrains-asm-debug-all-4.0.jar")
    (,java-asm4-for-intellij-133 "asm-6.0.jar" "ideaSDK/jps/jetbrains-asm-debug-all-4.0.jar")
    (,java-asm4-for-intellij-133 "asm-6.0.jar" "ideaSDK/core/jetbrains-asm-debug-all-4.0.jar")
    (,java-dart-ast-for-kotlin-0.6.786 "dart-ast.jar" "lib/dart-ast.jar")
    (,java-guava "guava-20.0.jar" "ideaSDK/core/guava-14.0.1.jar")
    (,java-javax-inject "javax.inject-1.jar" "lib/javax.inject.jar")
    (,java-jline-2 "jline.jar" "dependencies/jline.jar")
    (,java-picocontainer-1 "picocontainer.jar" "ideaSDK/core/picocontainer.jar")
    (,java-protobuf-2.5 "protobuf.jar" "ideaSDK/lib/protobuf-2.5.0.jar")
    (,java-spullara-cli-parser "spullara-cli-parser.jar" "dependencies/cli-parser-1.1.1.jar")
    (,java-trove4j-intellij "trove.jar" "ideaSDK/core/trove4j.jar")
    (,java-trove4j-intellij "trove.jar" "ideaSDK/lib/trove4j.jar")))

;; oct. 11 2013
(define-public kotlin-0.6.786
  (kotlin-package "0.6.786" "0igjrppp9nd7jb10ydx65xjml96ll9pbbvn8zvidzlbpgyz1mqqi"
                  '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "dependencies")
                  '("-Dshrink=false" "-Dgenerate.javadoc=false")
                  kotlin-dependencies-0.6.786
                  intellij-sdk-133-javac2
                  #:patches (search-patches "patches/kotlin-0.6.786.patch")
                  #:ant-directory "dependencies"
                  #:noverify? #f
                  #:use-annotations? #f))

(define kotlin-bootstrap-flags
  '("-Dshrink=false" "-Dgenerate.javadoc=false"))

(define kotlin-dependencies-0.6.1364
  `((,java-log4j-api "log4j-api.jar" "lib/log4j-api.jar")
    (,java-log4j-1.2-api "log4j-1.2-api.jar" "ideaSDK/core/log4j.jar")
    ,@kotlin-dependencies-0.6.786))

(define-public kotlin-0.6.1364
  (kotlin-package "0.6.1364" "1xz5q5cf866mcz0i6c24qnx9qldvm6nmfqbdfg8hk0ig2j9ygf15"
                  '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "dependencies/ant-1.7/lib")
                  kotlin-bootstrap-flags kotlin-dependencies-0.6.1364
                  intellij-sdk-133-javac2
                  #:bootstrap kotlin-0.6.786
                  #:noverify? #f
                  #:use-annotations? #f))

(define kotlin-dependencies-0.6.1932
  `((,intellij-sdk-133-jps-model "jps-model.jar" "ideaSDK/jps/jps-model.jar")
    ,@kotlin-dependencies-0.6.1364))

(define-public kotlin-0.6.1932
  (let ((base
          (kotlin-package "0.6.1932" "1r1psqwa5k7klkcyk96rva208zvy76j7nv69v9dfsak2zs1c43ip"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib")
                          kotlin-bootstrap-flags kotlin-dependencies-0.6.1932
                          intellij-sdk-133-javac2
                          #:patches (search-patches "patches/kotlin-0.6.1932.patch")
                          #:bootstrap kotlin-0.6.1364
                          #:noverify? #f
                          #:use-annotations? #f)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
        ((#:phases phases)
         `(modify-phases ,phases
            (add-before 'configure 'copy-missing-files
              (lambda _
                ;; These files is used by the compiler, before the runtime is
                ;; built, so copy them to the compiler sources
                (copy-recursively
                  "runtime/src/org/jetbrains/annotations"
                  "core/util.runtime/src/org/jetbrains"))))))))))

(define-public kotlin-0.6.2107
  (kotlin-package "0.6.2107" "1cg1r5xzd0sr8m592j1vyqdqid4plyr3f01lsmg7wal2hfl68mr1"
                  '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                    "dependencies/ant-1.7/lib")
                  kotlin-bootstrap-flags kotlin-dependencies-0.6.1932
                  intellij-sdk-133-javac2
                  #:patches (search-patches "patches/kotlin-0.6.2107.patch")
                  #:bootstrap kotlin-0.6.1932
                  #:noverify? #t
                  #:use-annotations? #f))

(define kotlin-dependencies-0.6.2338
  (alist-delete java-dart-ast-for-kotlin-0.6.786 kotlin-dependencies-0.6.1932))

(define-public kotlin-0.6.2338
  (kotlin-package "0.6.2338" "16b6z1xw8m0iwizxrqyfg49fii04q2dx9j00fxdvk6rxjyw0l48c"
                  '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                    "dependencies/ant-1.7/lib")
                  kotlin-bootstrap-flags kotlin-dependencies-0.6.2338
                  intellij-sdk-133-javac2
                  #:bootstrap kotlin-0.6.2107
                  #:noverify? #t
                  #:use-annotations? #f))

(define kotlin-dependencies-0.6.2451
  (map (match-lambda
         ((input jar tgt)
          (list (intellij-134-package input) jar tgt)))
       kotlin-dependencies-0.6.2338))

(define-public kotlin-0.6.2451
  (kotlin-package "0.6.2451" "1ihk7nxdfhird7ai2l3xvjqpb0a717hqvm9g9697w4xq3jil8fla"
                  '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                    "dependencies/ant-1.7/lib" "dependencies/annotations")
                  kotlin-bootstrap-flags kotlin-dependencies-0.6.2451
                  intellij-sdk-134-javac2
                  #:bootstrap kotlin-0.6.2107
                  #:patches (search-patches "patches/kotlin-0.6.2451.patch")
                  #:noverify? #t
                  #:use-annotations? #t))

(define kotlin-bootstrap-flags2
  (cons "-Dbootstrap.build.no.tests=true" kotlin-bootstrap-flags))

(define-public kotlin-0.6.2516
  (let ((base
          (kotlin-package "0.6.2516" "1phxi82gzp7fx7jgf3n7zh2ww7lzi0ih7zn6q249z43jz23wvhr5"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.6.2451
                          intellij-sdk-134-javac2
                          #:bootstrap kotlin-0.6.2451
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.7.333
  (let ((base
          (kotlin-package "0.7.333" "02xx4w65lbmqhxyffzhazg0fl378k81gq1rpidmfqz553smv0z2j"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.6.2451
                          intellij-sdk-134-javac2
                          #:bootstrap kotlin-0.6.2516
                          #:patches (search-patches "patches/kotlin-0.7.333.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define kotlin-dependencies-0.7.638
  `((,java-asm5-for-intellij-135 "asm-6.0.jar" "ideaSDK/core/asm-all.jar")
    ,@(map (match-lambda
             ((input jar tgt)
              (list (intellij-135-package input) jar tgt)))
           kotlin-dependencies-0.6.2451)))

(define-public kotlin-0.7.638
  (let ((base
          (kotlin-package "0.7.638" "1b0jc3w2s844b338hxvy10vilp3397djmb18xkckl7msg8yvgx0i"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.7.638
                          intellij-sdk-135-javac2
                          #:bootstrap kotlin-0.7.333
                          #:patches (search-patches "patches/kotlin-0.7.638.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.7.1189
  (let ((base
          (kotlin-package "0.7.1189" "1zbl2hsknb2zg6nk722qssj36ijrdc872zf93mzcrl1y1nfxdlp8"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.7.638
                          intellij-sdk-135-javac2
                          #:bootstrap kotlin-0.7.638
                          #:patches (search-patches "patches/kotlin-0.7.1189.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define kotlin-dependencies-0.8.84
  `((,java-asm5-for-intellij-135 "asm-6.0.jar" "ideaSDK/lib/asm-all.jar")
    ,@(map (match-lambda
             ((input jar tgt)
              (list (intellij-138-package input) jar tgt)))
           kotlin-dependencies-0.7.638)))

(define-public kotlin-0.8.84
  (let ((base
          (kotlin-package "0.8.84" "0bmdvnj9bz0d8bfj8rwk6rjdy9bi5g5mcfji0lxy5w5rx7rsmsjj"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.8.84
                          intellij-sdk-138-javac2
                          #:bootstrap kotlin-0.7.1189
                          #:patches (search-patches "patches/kotlin-0.8.84.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.8.409
  (let ((base
          (kotlin-package "0.8.409" "1lgxil6w68761nm4yh3irismmb0c0xpyalygg4svwnx5xd368k81"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.8.84
                          intellij-sdk-138-javac2
                          #:bootstrap kotlin-0.8.84
                          #:patches (search-patches "patches/kotlin-0.8.409.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.8.418
  (let ((base
          (kotlin-package "0.8.418" "0gf8l9asfrwhl67ysw1c71wmp8acb7l2j41lb97l8scbwl2lvnn9"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.8.84
                          intellij-sdk-138-javac2
                          #:bootstrap kotlin-0.8.409
                          #:patches (search-patches "patches/kotlin-0.8.418.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.8.422
  (let ((base
          (kotlin-package "0.8.422" "0drjphsr54ly5s0p5ix7ra4npx9gsa8m3alq54qqh4kaxz1r9q78"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.8.84
                          intellij-sdk-138-javac2
                          #:bootstrap kotlin-0.8.418
                          #:patches (search-patches "patches/kotlin-0.8.422.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.8.1444
  (let ((base
          (kotlin-package "0.8.1444" "08hb29909gglyq2l3jz8cs80qkqcdk23f8i2fww93df7qvil8mrp"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.8.84
                          intellij-sdk-138-javac2
                          #:bootstrap kotlin-0.8.422
                          #:patches (search-patches "patches/kotlin-0.8.1444.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.9.21
  (let ((base
          (kotlin-package "0.9.21" "1zsqgh5kn0d1nljdbyx8hz0nlfg7si34qlhjssmcdnh73dly2cqq"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.8.84
                          intellij-sdk-138-javac2
                          #:bootstrap kotlin-0.8.1444
                          #:patches (search-patches "patches/kotlin-0.9.21.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.9.738
  (let ((base
          (kotlin-package "0.9.738" "0q4715z4l6v773ad58s1k6gcdv202djay9lbawwf3v9dds3bdhkf"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.8.84
                          intellij-sdk-138-javac2
                          #:bootstrap kotlin-0.9.21
                          #:patches (search-patches "patches/kotlin-0.9.738.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define kotlin-dependencies-0.9.1204
  (cons
    (list java-jdom-for-intellij-133 "jdom.jar" "ideaSDK/core/jdom.jar")
    kotlin-dependencies-0.8.84))

(define-public kotlin-0.9.1204
  (let ((base
          (kotlin-package "0.9.1204" "0is72gbf8b38awlxp5slngxp5afyxazmk4c38lnl5hjg160m60a7"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.9.1204
                          intellij-sdk-138-javac2
                          #:bootstrap kotlin-0.9.738
                          #:patches (search-patches "patches/kotlin-0.9.1204.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.10.300
  (let ((base
          (kotlin-package "0.10.300" "1gbca97sj3lp1f5jf9qaiwjjqcvkl35n7sy225xa6v4lyxslqaw4"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.9.1204
                          intellij-sdk-138-javac2
                          #:bootstrap kotlin-0.9.1204
                          #:patches (search-patches "patches/kotlin-0.10.300.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define kotlin-dependencies-0.10.823
  (map (match-lambda
         ((input jar tgt)
          (list (intellij-139-package input) jar tgt)))
       kotlin-dependencies-0.9.1204))

(define-public kotlin-0.10.823
  (let ((base
          (kotlin-package "0.10.823" "18600328nfr33vqvmwrs7gq2w15hfbwb8p6l5a3rwg8s9s3aiwqw"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.10.823
                          intellij-sdk-139-javac2
                          #:bootstrap kotlin-0.10.300
                          #:patches (search-patches "patches/kotlin-0.10.823.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define kotlin-dependencies-0.10.1023
  (map (match-lambda
         ((input jar tgt)
          (list (intellij-141-package input) jar tgt)))
       kotlin-dependencies-0.10.823))

(define-public kotlin-0.10.1023
  (let ((base
          (kotlin-package "0.10.1023" "1hj6i1d6w64vr7q8xbxr0mq41rqln70ciimsl20p34jrrwc3cx4r"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.10.1023
                          intellij-sdk-141-javac2
                          #:bootstrap kotlin-0.10.823
                          #:patches (search-patches "patches/kotlin-0.10.1023.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.10.1336
  (let ((base
          (kotlin-package "0.10.1336" "0pfcwcpnv94gbzmzbv3g53yblwr5ifrxw7d4jnrgbm58qvzw4l47"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.10.1023
                          intellij-sdk-141-javac2
                          #:bootstrap kotlin-0.10.1023
                          #:patches (search-patches "patches/kotlin-0.10.1336.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define kotlin-dependencies-0.10.1426
  (cons* (list java-native-access-for-intellij-133
               "jna.jar" "lib/jna.jar")
         (list java-native-access-platform-for-intellij-133
               "platform.jar" "ideaSDK/lib/jna-utils.jar")
         (list java-jakarta-oro
               "jakarta-oro-2.0.8.jar" "ideaSDK/lib/oromatcher.jar")
         (list java-protobuf-2.5 "protobuf.jar" "dependencies/protobuf-2.5.0-lite.jar")
         kotlin-dependencies-0.10.1023))

(define-public kotlin-0.10.1426
  (let ((base
          (kotlin-package "0.10.1426" "1yklmxcficslqw960nxwkwb07ywsd3bl52315x4bbcg0pg8nz3pl"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.10.1426
                          intellij-sdk-141-javac2
                          #:bootstrap kotlin-0.10.1336
                          #:patches (search-patches "patches/kotlin-0.10.1426.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.11.153
  (let ((base
          (kotlin-package "0.11.153" "1nfiz7kcqlxn2q1347mw494fxq83mkipswsq4pwair02akaagnyp"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.10.1426
                          intellij-sdk-141-javac2
                          #:bootstrap kotlin-0.10.1426
                          #:patches (search-patches "patches/kotlin-0.11.153.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.11.873
  (let ((base
          (kotlin-package "0.11.873" "0kx9v0i0c9vywcbsaifc6xc0jwharn2p1qnpnxhylk1kicjfb1yw"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.10.1426
                          intellij-sdk-141-javac2
                          #:bootstrap kotlin-0.11.153
                          #:patches (search-patches "patches/kotlin-0.11.873.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.11.992
  (let ((base
          (kotlin-package "0.11.992" "0ilbjiwfwryrn3107z2y3arljw6p0ahdr0wxrfqki0xw7h6iajay"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.10.1426
                          intellij-sdk-141-javac2
                          #:bootstrap kotlin-0.11.873
                          #:patches (search-patches "patches/kotlin-0.11.992.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.11.1014
  (let ((base
          (kotlin-package "0.11.1014" "097c1ng38qv9c4a4kp8gh5slqsy9426blf15ilgb6rvmhsmyh0p3"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.10.1426
                          intellij-sdk-141-javac2
                          #:bootstrap kotlin-0.11.992
                          #:patches (search-patches "patches/kotlin-0.11.1014.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.11.1201
  (let ((base
          (kotlin-package "0.11.1201" "00h8jsmlj94h64sf451f8c35xknxisbw9zlkh38akpn1fnls9xnv"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.10.1426
                          intellij-sdk-141-javac2
                          #:bootstrap kotlin-0.11.1014
                          #:patches (search-patches "patches/kotlin-0.11.1201.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.11.1393
  (let ((base
          (kotlin-package "0.11.1393" "0awx55k4mcc2l04dnlz43kfdjq959zr0qhpqd1zrnls5n58r6rch"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.10.1426
                          intellij-sdk-141-javac2
                          #:bootstrap kotlin-0.11.1201
                          #:patches (search-patches "patches/kotlin-0.11.1393.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define kotlin-dependencies-0.12.108
  `((,java-jarjar "jarjar-1.4.jar" "dependencies/jarjar.jar")
    ,@kotlin-dependencies-0.10.1426))

(define-public kotlin-0.12.108
  (let ((base
          (kotlin-package "0.12.108" "0zvbmmlajzw8z3zwjj5p87mw2k4kip7lyaq8nw26k78ybgbcl3kz"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.12.108
                          intellij-sdk-141-javac2
                          #:bootstrap kotlin-0.11.1393
                          #:patches (search-patches "patches/kotlin-0.12.108.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.12.115
  (let ((base
          (kotlin-package "0.12.115" "1k0dwb6xkqjpg8m8g2220v9nbp1fqr7ixkks3p0papkfc0x6h07g"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.12.108
                          intellij-sdk-141-javac2
                          #:bootstrap kotlin-0.12.108
                          #:patches (search-patches "patches/kotlin-0.12.115.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.12.176
  (let ((base
          (kotlin-package "0.12.176" "0357bg7729j4lpvd063d3ar7lhm4zxg1rjf9rp5yr72cl17yqi91"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.7/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.12.108
                          intellij-sdk-141-javac2
                          #:bootstrap kotlin-0.12.115
                          #:patches (search-patches "patches/kotlin-0.12.176.patch")
                          #:noverify? #t
                          #:use-annotations? #t)))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.12.470
  (let ((base
          (kotlin-package "0.12.470" "1ilbgyjl7qqvs4fzx9x9aji3b5vdh00qzw32r8k5sr2zwq2zcw82"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.12.108
                          intellij-sdk-141-javac2
                          #:bootstrap kotlin-0.12.176
                          #:patches (search-patches "patches/kotlin-0.12.470.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define kotlin-dependencies-0.12.1077
  `((,java-jansi-1 "jansi-1.16.jar" "dependencies/jansi.jar")
    (,java-jansi-native "jansi-native-1.7.jar" "dependencies/jansi-native.jar")
    ,@(map (match-lambda
             ((input jar tgt)
              (if (equal? tgt "ideaSDK/lib/jna-utils.jar")
                (list input jar "ideaSDK/lib/jna-platform.jar")
                (list (intellij-143-package input) jar tgt))))
           kotlin-dependencies-0.12.108)))

(define-public kotlin-0.12.1077
  (let ((base
          (kotlin-package "0.12.1077" "06glx32g8r3pzgyij52spgyz9w9drq9bghl6c92n2w1rmf2afd7j"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.12.1077
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-0.12.470
                          #:patches (search-patches "patches/kotlin-0.12.1077.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.12.1250
  (let ((base
          (kotlin-package "0.12.1250" "0rq1lq1hhilfdvay0k24vwv5hf7mlvf18df547nh9hz8zickciqi"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.12.1077
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-0.12.1077
                          #:patches (search-patches "patches/kotlin-0.12.1250.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.12.1306
  (let ((base
          (kotlin-package "0.12.1306" "04s8byvvp1lil0ckpwl3qqsxwn0nm22j23g29v2r7pblkpk3h6ww"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.12.1077
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-0.12.1250
                          #:patches (search-patches "patches/kotlin-0.12.1306.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.13.177
  (let ((base
          (kotlin-package "0.13.177" "1pzhm5qxaq149ca5qba2brr6vhmky6ip6j9hc0a03g47j6814f2z"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.12.1077
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-0.12.1306
                          #:patches (search-patches "patches/kotlin-0.13.177.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.13.791
  (let ((base
          (kotlin-package "0.13.791" "1y4ywggvbbj1kl8anz8kx3ccnsp0gkvfz67ppd2gphdd86fmad71"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.12.1077
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-0.13.177
                          #:patches (search-patches "patches/kotlin-0.13.791.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.13.899
  (let ((base
          (kotlin-package "0.13.899" "10x2vsz8nzixqsr7scgchnmqqg6w6v90xmyz73p9zrpwx8bqwscw"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.12.1077
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-0.13.791
                          #:patches (search-patches "patches/kotlin-0.13.899.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.13.1118
  (let ((base
          (kotlin-package "0.13.1118" "0q03ym8gy9wk242pna8jvixvkbw886ba7mwwi50waml2wh18warh"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.12.1077
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-0.13.899
                          #:patches (search-patches "patches/kotlin-0.13.1118.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.13.1304
  (let ((base
          (kotlin-package "0.13.1304" "15p3c4qimbkaf5q3wdik6q6jlsh5an1rjh71s3iab86382gfms37"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.12.1077
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-0.13.1118
                          #:patches (search-patches "patches/kotlin-0.13.1304.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.14.209
  (let ((base
          (kotlin-package "0.14.209" "1f933a0xb1w57qqwbnb0f3fsksk1h0aj19knz257qnrzx5c6w16q"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.12.1077
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-0.13.1304
                          #:patches (search-patches "patches/kotlin-0.14.209.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.14.398
  (let ((base
          (kotlin-package "0.14.398" "1cjjsnk1q4dx51rqmjs00fxqc5nxfz2qwr08y4xnhb10clpkb1r3"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.12.1077
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-0.14.209
                          #:patches (search-patches "patches/kotlin-0.14.398.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define kotlin-dependencies-0.15.8
  `((,java-native-access-for-intellij-133 "jna.jar" "ideaSDK/lib/jna.jar")
    (,java-native-access-platform-for-intellij-133 "platform.jar" "ideaSDK/lib/jna-platform.jar")
    ,@(alist-delete java-native-access-for-intellij-133
        (alist-delete java-native-access-platform-for-intellij-133
          kotlin-dependencies-0.12.1077))))

(define-public kotlin-0.15.8
  (let ((base
          (kotlin-package "0.15.8" "1vpn76dzgm8swgw66j77i84i1zx05zpw9y219ff9mvmmvj5wwbih"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.15.8
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-0.14.398
                          #:patches (search-patches "patches/kotlin-0.15.8.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define kotlin-dependencies-0.15.394
  `((,java-native-platform "native-platform.jar" "dependencies/native-platform-uberjar.jar")
     ,@kotlin-dependencies-0.15.8))

(define-public kotlin-0.15.394
  (let ((base
          (kotlin-package "0.15.394" "17cvq7c1c0j3w2hs14b3cmv1385zyv0y76vmlc35yljay0cj2wgm"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.15.394
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-0.15.8
                          #:patches (search-patches "patches/kotlin-0.15.394.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.15.541
  (let ((base
          (kotlin-package "0.15.541" "0szngpigb69vj58xjiww5j66si9fahsp6crk6jky09hllrqfb1dk"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.15.394
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-0.15.394
                          #:patches (search-patches "patches/kotlin-0.15.541.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.15.604
  (let ((base
          (kotlin-package "0.15.604" "1crrxgg2c0cr8n4xxxs4y73ximin3nb205fimq911hvm3zk3r6lc"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.15.394
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-0.15.541
                          #:patches (search-patches "patches/kotlin-0.15.604.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-0.15.723
  (let ((base
          (kotlin-package "0.15.723" "1hfrsvlzq3z7kb14z8kn2if5r3gjjini1l09d4px6jlcdys3jydn"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.15.394
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-0.15.604
                          #:patches (search-patches "patches/kotlin-0.15.723.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-1.0.0-beta-2055
  (let ((base
          (kotlin-package "1.0.0-beta-2055" "048a4xiamvycmkz66hj0rqyq9dpi78dd7iy8szhfq5mzx6w7i8vh"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.15.394
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-0.15.723
                          #:patches (search-patches "patches/kotlin-1.0.0-beta-2055.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-1.0.0-beta-3070
  (let ((base
          (kotlin-package "1.0.0-beta-3070" "0qcj8gaf1kyiy03isgr4938s293w6jnxbqh7b1gg25dr5ck789gh"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.15.394
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-1.0.0-beta-2055
                          #:patches (search-patches "patches/kotlin-1.0.0-beta-3070.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-1.0.0-beta-4091
  (let ((base
          (kotlin-package "1.0.0-beta-4091" "19l7jp0vd9ypvm231dv78cmw46ygm7nrzcf78qa8xfnc1b4n6kpq"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.15.394
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-1.0.0-beta-3070
                          #:patches (search-patches "patches/kotlin-1.0.0-beta-4091.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-1.0.0-beta-5010
  (let ((base
          (kotlin-package "1.0.0-beta-5010" "0c97418sq2hb1d9xh15nckhp2ikqk6mp3bcc4wi93sgnjsb09m8p"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.15.394
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-1.0.0-beta-4091
                          #:patches (search-patches "patches/kotlin-1.0.0-beta-5010.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-1.0.0-beta-5604
  (let ((base
          (kotlin-package "1.0.0-beta-5604" "18cp0b88r3dscdj9sy7wzl363gx77pxi0gzjjl1sbjsg5dnagv0p"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.15.394
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-1.0.0-beta-5010
                          #:patches (search-patches "patches/kotlin-1.0.0-beta-5604.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-1.0.0-dev-162
  (let ((base
          (kotlin-package "1.0.0-dev-162" "1m7fv14nd1qcyy5yc26sg3n7d84s4m01vn0svjl4gqlv7n3nqcgg"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.15.394
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-1.0.0-beta-5604
                          #:patches (search-patches "patches/kotlin-1.0.0-dev-162.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))

(define-public kotlin-1.0.0
  (let ((base
          (kotlin-package "1.0.0" "0wi32jdk6k81kjwz216k7p5k9kmvkpxfp9588dpr2541hrljcgkb"
                          '("ideaSDK/core" "ideaSDK/lib" "ideaSDK/jps" "ideaSDK/core-analysis"
                            "dependencies/ant-1.8/lib" "dependencies/annotations")
                          kotlin-bootstrap-flags2 kotlin-dependencies-0.15.394
                          intellij-sdk-143-javac2
                          #:bootstrap kotlin-1.0.0-dev-162
                          #:patches (search-patches "patches/kotlin-1.0.0.patch")
                          #:noverify? #t
                          #:use-annotations? #t
                          #:ant-directory "dependencies/ant-1.8/lib")))
    (package
      (inherit base)
      (arguments
       (substitute-keyword-arguments (package-arguments base)
         ((#:phases phases)
          `(modify-phases ,phases
            (delete 'generate-jar-indices))))))))
