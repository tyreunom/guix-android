;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android packages android-headers)
  #:use-module (android build blueprint)
  #:use-module (android packages android-sources)
  #:use-module (gnu packages)
  #:use-module (guix build-system copy)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:))

;; This file defines headers packages that are needed globally when building
;; for the "host".
;;
;; Quoting soong/cc/config/global.go:
;; "Everything in these lists is a crime against abstraction and dependency tracking.
;;  Do not add anything to this list."
;;
;; Instead of using a global path, this means we have to provide an input for
;; each of these paths, which currently are:
;;
;; - system/core/include
;; - system/logging/liblog/include
;; - system/media/audio/include
;; - hardware/libhardware/include
;; - hardware/libhardware_legacy/include
;; - hardware/ril/include
;; - frameworks/native/include
;; - frameworks/native/opengl/include
;; - frameworks/av/include
;;
;; Additionaly, there is a path added for "host" modules to get jni.h:
;; - libnativehelper/include_jni
;;
;; These packages are listed at the bottom of the file, so they can more
;; easily be added from the soong-build-system.

(define-public android-system-core-include
  (package
    (name "android-system-core-include")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system copy-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (define (install-entry dir file out)
                 (mkdir-p out)
                 (let* ((full-file (string-append dir "/" file))
                        (st (stat full-file))
                        (type (stat:type st)))
                   (cond
                     ((equal? file ".") #t)
                     ((equal? file "..") #t)
                     ((equal? type 'symlink)
                      (install-entry (string-append dir "/" (readlink full-file) out)))
                     ((equal? type 'regular)
                      (copy-file full-file (string-append out "/" (basename file))))
                     ((equal? type 'directory)
                      (install-dir full-file (string-append out "/" (basename file)))))))

               (define (install-dir dir out)
                 (let loop ((directory (opendir dir)))
                   (let ((file (readdir directory)))
                     (unless (eof-object? file)
                       (install-entry dir file out)
                       (loop directory)))))

               (install-dir "include" (string-append out "/include"))))))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public android-system-logging-liblog-include
  (package
    (name "android-system-logging-liblog-include")
    (version %latest-android-13)
    (source android-platform-system-logging-13-source)
    (build-system copy-build-system)
    (arguments
     `(#:install-plan '(("liblog/include" "include"))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public android-system-media-audio-include
  (package
    (name "android-system-media-audio-include")
    (version %latest-android-13)
    (source android-platform-system-media-13-source)
    (build-system copy-build-system)
    (arguments
     `(#:install-plan '(("audio/include" "include"))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public android-hardware-libhardware-include
  (package
    (name "android-hardware-libhardware-include")
    (version %latest-android-13)
    (source android-platform-hardware-libhardware-13-source)
    (build-system copy-build-system)
    (arguments
     `(#:install-plan '(("include" "include"))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public android-hardware-libhardware-legacy-include
  (package
    (name "android-hardware-libhardware-legacy-include")
    (version %latest-android-13)
    (source android-platform-hardware-libhardware-legacy-13-source)
    (build-system copy-build-system)
    (arguments
     `(#:install-plan '(("include" "include"))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public android-hardware-ril-include
  (package
    (name "android-hardware-ril-include")
    (version %latest-android-13)
    (source android-platform-hardware-ril-13-source)
    (build-system copy-build-system)
    (arguments
     `(#:install-plan '(("include" "include"))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public android-frameworks-native-include
  (package
    (name "android-frameworks-native-include")
    (version %latest-android-13)
    (source android-platform-frameworks-native-13-source)
    (build-system copy-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (define (install-entry dir file out)
                 (mkdir-p out)
                 (let ((full-file (string-append dir "/" file)))
                   (when (file-exists? full-file)
                     (let* ((full-file (string-append dir "/" file))
                            (st (stat full-file))
                            (type (stat:type st)))
                       (cond
                         ((equal? file ".") #t)
                         ((equal? file "..") #t)
                         ((equal? type 'symlink)
                          (install-entry (string-append dir "/" (readlink full-file) out)))
                         ((equal? type 'regular)
                          (copy-file full-file (string-append out "/" (basename file))))
                         ((equal? type 'directory)
                          (install-dir full-file (string-append out "/" (basename file)))))))))

               (define (install-dir dir out)
                 (let loop ((directory (opendir dir)))
                   (let ((file (readdir directory)))
                     (unless (eof-object? file)
                       (install-entry dir file out)
                       (loop directory)))))

               (install-dir "include" (string-append out "/include"))))))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public android-frameworks-native-opengl-include
  (package
    (name "android-frameworks-native-opengl-include")
    (version %latest-android-13)
    (source android-platform-frameworks-native-13-source)
    (build-system copy-build-system)
    (arguments
     `(#:install-plan '(("opengl/include" "include"))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public android-frameworks-av-include
  (package
    (name "android-frameworks-av-include")
    (version %latest-android-13)
    (source android-platform-frameworks-av-13-source)
    (build-system copy-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (define (install-entry dir file out)
                 (mkdir-p out)
                 (let ((full-file (string-append dir "/" file)))
                   (when (file-exists? full-file)
                     (let* ((full-file (string-append dir "/" file))
                            (st (stat full-file))
                            (type (stat:type st)))
                       (cond
                         ((equal? file ".") #t)
                         ((equal? file "..") #t)
                         ((equal? type 'symlink)
                          (install-entry (string-append dir "/" (readlink full-file) out)))
                         ((equal? type 'regular)
                          (copy-file full-file (string-append out "/" (basename file))))
                         ((equal? type 'directory)
                          (install-dir full-file (string-append out "/" (basename file)))))))))

               (define (install-dir dir out)
                 (let loop ((directory (opendir dir)))
                   (let ((file (readdir directory)))
                     (unless (eof-object? file)
                       (install-entry dir file out)
                       (loop directory)))))

               (install-dir "include" (string-append out "/include"))))))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public android-libnativehelper-include
  (package
    (name "android-libnativehelper-include")
    (version %latest-android-13)
    (source android-platform-libnativehelper-13-source)
    (build-system copy-build-system)
    (arguments
     `(#:install-plan '(("include" "include"))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public %global-android-host-headers
  `(("android-system-core-include" ,android-system-core-include)
    ("android-system-logging-liblog-include" ,android-system-logging-liblog-include)
    ("android-system-media-audio-include" ,android-system-media-audio-include)
    ("android-hardware-libhardware-include" ,android-hardware-libhardware-include)
    ("android-hardware-libhardware-legacy-include" ,android-hardware-libhardware-legacy-include)
    ("android-hardware-ril-include" ,android-hardware-ril-include)
    ("android-frameworks-native-include" ,android-frameworks-native-include)
    ("android-frameworks-native-opengl-include" ,android-frameworks-native-opengl-include)
    ("android-frameworks-av-include" ,android-frameworks-av-include)
    ("android-libnativehelper-include" ,android-libnativehelper-include)))

;; Additional header packages needed by some android packages

(define-public android-frameworks-base-tools-include
  (package
    (name "android-frameworks-base-tools-include")
    (version %latest-android-13)
    (source android-platform-frameworks-base-13-source)
    (build-system copy-build-system)
    (arguments
     `(#:install-plan '(("tools" "include" #:include (".h")))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))
