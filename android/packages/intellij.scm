;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021, 2022 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android packages intellij)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system ant)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages batik)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages java)
  #:use-module (gnu packages java-compression)
  #:use-module (gnu packages java-xml)
  #:use-module (gnu packages xml)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (android packages java))

;; First, define all the versions we need to bootstrap Kotlin

(define intellij-community-133-commit "bc220ce6434e2fb0b0515e068b2c570b9a3a055e")
(define intellij-community-133-version
  (git-version "133" "0" intellij-community-133-commit))

(define intellij-community-134-commit "1168c7b8cb4dc8318b8d24037b372141730a0d1f")
(define intellij-community-134-version
  (git-version "134" "0" intellij-community-134-commit))

(define intellij-community-135-commit "00f6eceb5a187dab9a664632233aa392b9d70fe0")
(define intellij-community-135-version
  (git-version "135" "0" intellij-community-135-commit))

(define intellij-community-138-commit "070c64f86da3bfd3c86f151c75aefeb4f67870c8")
(define intellij-community-138-version
  (git-version "138" "0" intellij-community-138-commit))

(define intellij-community-139-commit "26e72feacf91bfb222bec00b3139ed05aa3084b5")
(define intellij-community-139-version
  (git-version "139" "0" intellij-community-139-commit))

(define intellij-community-141-commit "3ac0edbb89188dd24c473322140ebc97bedc0934")
(define intellij-community-141-version
  (git-version "141" "0" intellij-community-141-commit))

(define intellij-community-143-commit "65d73acda17908887bd0cefbac22a2f36c2c7ef2")
(define intellij-community-143-version
  (git-version "143" "0" intellij-community-143-commit))

(define (get-intellij-community-source commit version hash patches)
  "Return the sources for a given version of the Intellij SDK."
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://github.com/JetBrains/intellij-community")
           (commit commit)))
    (file-name (git-file-name "intellij" version))
    (sha256 (base32 hash))
    (patches patches)
    (modules '((guix build utils)))
    (snippet
      `(begin
         (for-each
           (lambda (file)
             (if (file-exists? file)
                 (delete-file-recursively file)))
           (append (find-files "." "^lib$" #:directories? #t)
                   (find-files "." "^bin$" #:directories? #t)))
         (delete-file "build.xml")
         (for-each delete-file (find-files "." ".*.jar$"))
         #t))))

;; Then, we define the origin record for each version we need for the Kotlin
;; bootstrap.

(define intellij-community-133-source
  (get-intellij-community-source
    intellij-community-133-commit
    intellij-community-133-version
    "0k4b1y8dpy3qza7hw5rms4afhjsgr5i8y7qx32fhyf3yybyg8npm"
    (search-patches "patches/sdk-133.patch")))

(define intellij-community-134-source
  (get-intellij-community-source
    intellij-community-134-commit
    intellij-community-134-version
    "1rxqimq4kxvn5w96sdcd53hll0f9d17qx4wlgwkiwc2ffrdc1bb9"
    (search-patches "patches/sdk-134.patch")))

(define intellij-community-135-source
  (get-intellij-community-source
    intellij-community-135-commit
    intellij-community-135-version
    "1dx7lqfnlz33rnjnmvl5cmv8fjblzhkxv5i1ia842y7c4s6k205a"
    (search-patches "patches/sdk-135.patch")))

(define intellij-community-138-source
  (get-intellij-community-source
    intellij-community-138-commit
    intellij-community-138-version
    "00gazs9v2r5f4hkxbmgsyajbspsz9wzg8xgywnlwl57b1ra8gqp3"
    (search-patches "patches/sdk-138.patch")))

(define intellij-community-139-source
  (get-intellij-community-source
    intellij-community-139-commit
    intellij-community-139-version
    "0mq12m2vdcxklsymda25dxvmn844i96iwdgl1hqyzg6hnli71isf"
    (search-patches "patches/sdk-139.patch")))

(define intellij-community-141-source
  (get-intellij-community-source
    intellij-community-141-commit
    intellij-community-141-version
    "1gms34s7n06iir6j393jj3cp1fiyc5g7vpkkn86ynzhb25dnid7n"
    (search-patches "patches/sdk-141.patch")))

(define intellij-community-143-source
  (get-intellij-community-source
    intellij-community-143-commit
    intellij-community-143-version
    "1m21jy288p1izhgc88khbhbgfhma2yp9ry9fbfyp87al9z08m665"
    '()))

;; Intellij/Kotlin-specific dependencies

(define-public java-jdom-for-intellij-133
  (package
    (inherit java-jdom)
    (version "0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/JetBrains/"
                                  "intellij-community/raw/"
                                  intellij-community-133-commit
                                  "/lib/src/jdom.zip"))
              (sha256
               (base32
                "0yvs1bxxbpa11bnvhdhi11mnps1qbgg3qw7s8zw24s8wabznjn6a"))))
    (arguments
     `(#:jar-name "jdom.jar"
       #:source-dir ".."
       #:tests? #f))
    (inputs
     `(("java-jaxen" ,java-jaxen)))
    (native-inputs
     `(("unzip" ,unzip)))))

(define-public java-asm4-for-intellij-133
  (package
    (inherit java-asm)
    (version "4.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/JetBrains/intellij-deps-asm")
                     (commit "ASM_4_1")))
              (file-name (git-file-name "asm" version))
              (patches (search-patches "patches/asm4-icedtea3.patch"))
              (sha256
               (base32
                "15bdkwr42kc7lc9xr9yspgcfsjnk3yl5a18vlbm44pcvy1db188z"))
              (modules '((guix build utils)))
              (snippet
               `(for-each delete-file (find-files "." "\\.jar$")))))
    (arguments
      (substitute-keyword-arguments (package-arguments java-asm)
        ((#:phases phases)
         `(modify-phases ,phases
           (delete 'remove-bnd-dependency)
           (add-after 'build-jars 'shade
             (lambda* (#:key inputs #:allow-other-keys)
               (let ((jarjar (car (find-files (assoc-ref inputs "java-jarjar") ".*.jar$")))
                     (injar "dist/asm-6.0.jar")
                     (outjar "dist/asm-shaded.jar"))
                   (with-output-to-file "rules"
                     (lambda _
                       (display "rule org.objectweb.asm.** org.jetbrains.asm4.@1\n")))
                   (invoke "java" "-jar" jarjar "process" "rules" injar outjar)
                   (delete-file injar)
                   (rename-file outjar injar))))))))
    (native-inputs
     `(("java-junit" ,java-junit)
       ("java-jarjar" ,java-jarjar)))))

(define-public java-asm5-for-intellij-135
  (package
    (inherit java-asm)
    (version "5.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/JetBrains/intellij-deps-asm")
                     (commit "ASM_5_0")))
              (file-name (git-file-name "asm" version))
              (sha256
               (base32
                "07riq9fm555h0rky5602s4z8ggn2lc1j4ph6hfb8015dw2xdsqgk"))
              (modules '((guix build utils)))
              (snippet
               `(for-each delete-file (find-files "." "\\.jar$")))))
    (arguments
      (substitute-keyword-arguments (package-arguments java-asm)
        ((#:phases phases)
         `(modify-phases ,phases
           (delete 'remove-bnd-dependency)
           (add-after 'build-jars 'shade
             (lambda* (#:key inputs #:allow-other-keys)
               (let ((jarjar (car (find-files (assoc-ref inputs "java-jarjar") ".*.jar$")))
                     (injar "dist/asm-6.0.jar")
                     (outjar "dist/asm-shaded.jar"))
                   (with-output-to-file "rules"
                     (lambda _
                       (display "rule org.objectweb.asm.** org.jetbrains.org.objectweb.asm.@1\n")))
                   (invoke "java" "-jar" jarjar "process" "rules" injar outjar)
                   (delete-file injar)
                   (rename-file outjar injar))))))))
    (native-inputs
     `(("java-junit" ,java-junit)
       ("java-jarjar" ,java-jarjar)))))

(define-public java-asm-for-intellij-133
  (package
    (inherit java-asm)
    (name "java-asm3")
    (version "3.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/JetBrains/intellij-deps-asm")
                     (commit "ASM_3_3")))
              (file-name (git-file-name "asm" "3.3"))
              (sha256
               (base32
                "1f7sgw92b1f4bbfavv7d4352klws1ynrb6ddlyzfz35lrc57j496"))))
    (arguments
     `(#:build-target "jar"
       #:test-target "test"
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (replace 'install (install-jars "output/dist"))
         (delete 'generate-jar-indices))))
    (native-inputs
     `(("java-junit" ,java-junit)))))

(define-public java-native-access-for-intellij-133
  (package
    (inherit java-native-access)
    (version "3.3.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/JetBrains/intellij-deps-jna")
                     (commit version)))
              (file-name (git-file-name "jna" version))
              (sha256
               (base32
                "1xh5v2fqrlm8nk3076i0jn4xxdfrnk3a3bkk636yzn4dj1cqgh1n"))
              (modules '((guix build utils)))
              (snippet
               `(begin
                  (for-each delete-file (find-files "." ".*.jar"))
                  (delete-file-recursively "jnalib/native/libffi")
                  (delete-file-recursively "jnalib/dist")
                  (substitute* "jnalib/build.xml"
                    (("build\\$\\{vm.arch\\}") "build"))))))
    (arguments
     (substitute-keyword-arguments (package-arguments java-native-access)
       ((#:phases phases)
        `(modify-phases ,phases
           (add-before 'configure 'chdir
             (lambda _
               (chdir "jnalib")))))))))

(define-public java-native-access-platform-for-intellij-133
  (package
    (inherit java-native-access-platform)
    (version "3.3.0")
    (source (package-source java-native-access-for-intellij-133))
    (arguments
     (substitute-keyword-arguments (package-arguments java-native-access-platform)
       ((#:phases phases)
        `(modify-phases ,phases
           (add-before 'configure 'chdir-root
             (lambda _
               (chdir "jnalib")))))))
    (inputs
     `(("java-native-access" ,java-native-access-for-intellij-133)))))


(define-public java-jsr166e-for-intellij-133
  (package
    (name "java-jsr166e")
    (version "0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/JetBrains/"
                                  "intellij-community/raw/"
                                  intellij-community-133-commit
                                  "/lib/src/jsr166e_src.jar"))
              (sha256
               (base32
                "0p011fq9nclvscwjhc5df9bybba1chnvlnk4gynqihdkqxqd0hzp"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "jsr166e.jar"
       #:source-dir ".."
       #:jdk ,icedtea-7
       #:tests? #f))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:cc0)))

(define java-jgoodies-forms-for-intellij-133
  (package
    (inherit java-jgoodies-forms)
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/JetBrains/"
                                  "intellij-community/raw/"
                                  intellij-community-133-commit
                                  "/lib/src/jgoodies-forms-src.zip"))
              (sha256
               (base32
                "0jxhd3wbq01lcryirlq3wc406igb6dazpkqx1m3wrqyi90j9fdqf"))))
    (arguments
     `(#:jar-name "jgoodies-forms.jar"
       #:source-dir "."
       #:tests? #f))))

;; Procedures to relate a version of the SDK to another.

(define (strip-intellij-variant variant-property base)
  (package
    (inherit base)
    (properties (alist-delete variant-property (package-properties base)))))

(define (package-intellij-for-explicit-version version source variant-property base package-transform)
  (define variant
    (assq-ref (package-properties base) variant-property))

  (define (map-inputs inputs)
    (map (match-lambda
           ((name input)
            `(,name ,(package-transform input))))
         inputs))

  (cond
    (variant => force)
    (else
      (package
        (inherit base)
        (version version)
        (source source)
        (propagated-inputs (map-inputs (package-propagated-inputs base)))
        (inputs (map-inputs (package-inputs base)))
        (arguments
         (substitute-keyword-arguments (package-arguments base)
           ((#:jdk jdk)
            (if (version>? version "136")
                icedtea-8
                jdk))))))))

;; The base package is the version 133 variant (usually).
;; These procedures specialize the package transformation to a specific version
;; of the SDK.

(define-public (intellij-134-package base)
  (let ((name (if (package? base) (package-name base) "")))
    (cond
      ((and (> (string-length name) 8)
            (equal? (substring name 0 8) "intellij"))
       (package-intellij-for-explicit-version intellij-community-134-version
                                              intellij-community-134-source
                                              'intellij-134-variant
                                              base
                                              intellij-134-package))
      (else
       base))))

(define-public (strip-134-variant base)
  (strip-intellij-variant 'intellij-134-variant base))

(define-public (intellij-135-package base)
  (let ((name (if (package? base) (package-name base) "")))
    (cond
      ((and (> (string-length name) 8)
            (equal? (substring name 0 8) "intellij"))
       (package-intellij-for-explicit-version intellij-community-135-version
                                              intellij-community-135-source
                                              'intellij-135-variant
                                              base
                                              intellij-135-package))
      ((equal? name "java-asm") java-asm5-for-intellij-135)
      (else
       base))))

(define-public (strip-135-variant base)
  (strip-intellij-variant 'intellij-135-variant base))

(define-public (intellij-138-package base)
  (let ((name (if (package? base) (package-name base) "")))
    (cond
      ((and (> (string-length name) 8)
            (equal? (substring name 0 8) "intellij"))
       (package-intellij-for-explicit-version intellij-community-138-version
                                              intellij-community-138-source
                                              'intellij-138-variant
                                              base
                                              intellij-138-package))
      ((equal? name "java-asm") java-asm5-for-intellij-135)
      (else
       base))))

(define-public (strip-138-variant base)
  (strip-intellij-variant 'intellij-138-variant base))

(define-public (intellij-139-package base)
  (let ((name (if (package? base) (package-name base) "")))
    (cond
      ((and (> (string-length name) 8)
            (equal? (substring name 0 8) "intellij"))
       (package-intellij-for-explicit-version intellij-community-139-version
                                              intellij-community-139-source
                                              'intellij-139-variant
                                              base
                                              intellij-139-package))
      ((equal? name "java-asm") java-asm5-for-intellij-135)
      (else
       base))))

(define-public (strip-139-variant base)
  (strip-intellij-variant 'intellij-139-variant base))

(define-public (intellij-141-package base)
  (let ((name (if (package? base) (package-name base) "")))
    (cond
      ((and (> (string-length name) 8)
            (equal? (substring name 0 8) "intellij"))
       (package-intellij-for-explicit-version intellij-community-141-version
                                              intellij-community-141-source
                                              'intellij-141-variant
                                              base
                                              intellij-141-package))
      ((equal? name "java-asm") java-asm5-for-intellij-135)
      (else
       base))))

(define-public (strip-141-variant base)
  (strip-intellij-variant 'intellij-141-variant base))

(define-public (intellij-143-package base)
  (let ((name (if (package? base) (package-name base) "")))
    (cond
      ((and (> (string-length name) 8)
            (equal? (substring name 0 8) "intellij"))
       (package-intellij-for-explicit-version intellij-community-143-version
                                              intellij-community-143-source
                                              'intellij-143-variant
                                              base
                                              intellij-143-package))
      ((equal? name "java-asm") java-asm5-for-intellij-135)
      (else
       base))))

(define-public (strip-143-variant base)
  (strip-intellij-variant 'intellij-143-variant base))

;; The SDK
;; We define each module as a separate package.  We define them for version
;; 133 explicitely, and use the package transformation when possible, so
;; we don't have to explicitely define other versions.

;; Each module is found in a separate directory.  Given a module name, we can
;; create a package by:
;; - findin the module directory
;; - opening `<module>.iml` file that lists dependencies in `orderEntry`
;;   whose type is `module`.

(define-public intellij-sdk-133-util
  (package
    (name "intellij-sdk-util")
    (version intellij-community-133-version)
    (source intellij-community-133-source)
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "javac2.jar"
       #:tests? #f
       #:source-dir "SOURCEDIR"
       #:jdk ,icedtea-7
       #:imported-modules ((android build java-intellij)
                           ,@%ant-build-system-modules)
       #:modules ((guix build ant-build-system)
                  (guix build utils)
                  (android build java-intellij))
       #:phases
       (modify-phases %standard-phases
         (add-after 'configure 'set-source-dir
           (set-source-dir '("util" "util-rt"))))))
    (propagated-inputs
     (list java-asm-for-intellij-133
           java-cglib
           java-eawtstub
           java-jakarta-oro
           java-jdom-for-intellij-133
           java-jsr166e-for-intellij-133
           java-log4j-1.2-api
           java-log4j-api
           java-native-access-for-intellij-133
           java-native-access-platform-for-intellij-133
           java-picocontainer-1
           java-trove4j-intellij))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

#;(define-public intellij-sdk-133-openapi
  (package
    (name "intellij-sdk-openapi")
    (version intellij-community-133-version)
    (source intellij-community-133-source)
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "openapi.jar"
       #:tests? #f
       #:source-dir "SOURCEDIR"
       #:jdk ,icedtea-7
       #:imported-modules ((android build java-intellij)
                           ,@%ant-build-system-modules)
       #:modules ((guix build ant-build-system)
                  (guix build utils)
                  (android build java-intellij))
       #:phases
       (modify-phases %standard-phases
         (add-after 'configure 'set-source-dir
           (set-source-dir
             '("analysis-api" "core-api" "editor-ui-api" "external-system-api"
               "indexing-api" "jps-model-api" "lang-api" "lvcs-api"
               "projectModel-api" "platform-api" "usageView" "vcs-api"
               "vcs-log-api" "xdebugger-api" "xml-analysis-api" "xml-openapi"
               "xml-psi-api" "compiler-openapi" "debugger-openapi" "dom-openapi"
               "execution-openapi" "java-analysis-api" "java-indexing-api"
               "java-psi-api" "jsp-openapi" "jsp-base-openapi" "openapi"
               "remote-servers-api" "remote-servers-java-api"
               "testFramework-java" "xml-analysis-api"))))))
    (propagated-inputs
     (list java-jdom-for-intellij-133
           java-jgoodies-forms-for-intellij-133
           java-asm-for-intellij-133
           java-asm4-for-intellij-133
           ;java-log4j-api
           ;java-log4j-1.2-api
           java-trove4j-intellij
           java-picocontainer-1
           ;java-native-access-for-intellij-133
           ;java-native-access-platform-for-intellij-133
           java-jakarta-oro
           ;java-cglib
           ;java-jsr166e-for-intellij-133
           #;java-eawtstub))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public intellij-sdk-133-annotations
  (package
    (name "intellij-sdk-annotations")
    (version intellij-community-133-version)
    (source intellij-community-133-source)
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "annotations.jar"
       #:tests? #f
       #:source-dir "SOURCEDIR"
       #:imported-modules ((android build java-intellij)
                           ,@%ant-build-system-modules)
       #:modules ((guix build ant-build-system)
                  (guix build utils)
                  (android build java-intellij))
       #:phases
       (modify-phases %standard-phases
         (add-after 'configure 'set-source-dir
           (set-source-dir '("annotations"))))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public intellij-sdk-133-extensions
  (package
    (name "intellij-sdk-extensions")
    (version intellij-community-133-version)
    (source intellij-community-133-source)
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "extensions.jar"
       #:jdk ,icedtea-7
       #:tests? #f
       #:source-dir "SOURCEDIR"
       #:imported-modules ((android build java-intellij)
                           ,@%ant-build-system-modules)
       #:modules ((guix build ant-build-system)
                  (guix build utils)
                  (android build java-intellij))
       #:phases
       (modify-phases %standard-phases
         (add-after 'configure 'set-source-dir
           (set-source-dir '("extensions"))))))
    (propagated-inputs
     (list java-asm-for-intellij-133
           java-cglib
           java-eawtstub
           java-jakarta-oro
           java-jdom-for-intellij-133
           java-jsr166e-for-intellij-133
           java-log4j-1.2-api
           java-log4j-api
           java-native-access-for-intellij-133
           java-native-access-platform-for-intellij-133
           java-picocontainer-1
           java-trove4j-intellij
           java-xstream))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public intellij-sdk-133-bootstrap
  (package
    (name "intellij-sdk-bootstrap")
    (version intellij-community-133-version)
    (source intellij-community-133-source)
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "bootstrap.jar"
       #:jdk ,icedtea-7
       #:tests? #f
       #:source-dir "SOURCEDIR"
       #:imported-modules ((android build java-intellij)
                           ,@%ant-build-system-modules)
       #:modules ((guix build ant-build-system)
                  (guix build utils)
                  (android build java-intellij))
       #:phases
       (modify-phases %standard-phases
         (add-after 'configure 'set-source-dir
           (set-source-dir '("bootstrap"))))))
    (propagated-inputs
     (list java-asm-for-intellij-133
           java-cglib
           java-eawtstub
           java-jakarta-oro
           java-jdom-for-intellij-133
           java-jsr166e-for-intellij-133
           java-log4j-1.2-api
           java-log4j-api
           java-native-access-for-intellij-133
           java-native-access-platform-for-intellij-133
           java-picocontainer-1
           java-trove4j-intellij))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public intellij-sdk-133-jps-launcher
  (package
    (name "intellij-sdk-jps-launcher")
    (version intellij-community-133-version)
    (source intellij-community-133-source)
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "jps-launcher.jar"
       #:tests? #f
       #:source-dir "SOURCEDIR"
       #:imported-modules ((android build java-intellij)
                           ,@%ant-build-system-modules)
       #:modules ((guix build ant-build-system)
                  (guix build utils)
                  (android build java-intellij))
       #:phases
       (modify-phases %standard-phases
         (add-after 'configure 'set-source-dir
           (set-source-dir '("jps-launcher")))
         (add-before 'build 'copy-resources
           (copy-resources '("jps-launcher"))))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public intellij-sdk-133-javac2
  (package
    (name "intellij-sdk-javac2")
    (version intellij-community-133-version)
    (source intellij-community-133-source)
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "javac2.jar"
       #:tests? #f
       #:source-dir "SOURCEDIR"
       #:imported-modules ((android build java-intellij)
                           ,@%ant-build-system-modules)
       #:modules ((guix build ant-build-system)
                  (guix build utils)
                  (android build java-intellij))
       #:phases
       (let ((modules '("javac2" "forms-compiler" "forms_rt" "instrumentation-util")))
         (modify-phases %standard-phases
           (add-after 'configure 'set-source-dir
             (set-source-dir modules))
           (add-before 'build 'copy-resources
             (copy-resources modules))))))
    (propagated-inputs
     (list java-jdom-for-intellij-133
           java-jgoodies-forms-for-intellij-133
           java-asm4-for-intellij-133))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public intellij-sdk-134-javac2
  (intellij-134-package intellij-sdk-133-javac2))

(define-public intellij-sdk-135-javac2
  (intellij-135-package intellij-sdk-133-javac2))

(define-public intellij-sdk-138-javac2
  (intellij-138-package intellij-sdk-133-javac2))

(define-public intellij-sdk-139-javac2
  (intellij-139-package intellij-sdk-133-javac2))

(define-public intellij-sdk-141-javac2
  (intellij-141-package intellij-sdk-133-javac2))

(define-public intellij-sdk-143-javac2
  (intellij-143-package intellij-sdk-133-javac2))

(define-public intellij-sdk-133-jps-model
  (package
    (name "intellij-sdk-jps-model")
    (version intellij-community-133-version)
    (source intellij-community-133-source)
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "jps-model.jar"
       #:jdk ,icedtea-7
       #:tests? #f
       #:source-dir "SOURCEDIR"
       #:imported-modules ((android build java-intellij)
                           ,@%ant-build-system-modules)
       #:modules ((guix build ant-build-system)
                  (guix build utils)
                  (android build java-intellij))
       #:phases
       (let ((modules '("jps-model-api" "jps-model-impl"
                        "jps-model-serialization")))
         (modify-phases %standard-phases
           (add-after 'configure 'set-source-dir
             (set-source-dir modules))
           (add-before 'build 'copy-resources
             (copy-resources modules))))))
    (propagated-inputs
     (list java-asm-for-intellij-133
           java-cglib
           java-eawtstub
           java-imgscalr
           java-iq80-snappy
           java-jakarta-oro
           java-jdom-for-intellij-133
           java-jsr166e-for-intellij-133
           java-log4j-1.2-api
           java-log4j-api
           java-native-access-for-intellij-133
           java-native-access-platform-for-intellij-133
           java-picocontainer-1
           java-trove4j-intellij))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public intellij-sdk-133-intellij-core
  (package
    (name "intellij-sdk-intellij-core")
    (version intellij-community-133-version)
    (source intellij-community-133-source)
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "intellij-core.jar"
       #:jdk ,icedtea-7
       #:tests? #f
       #:source-dir "SOURCEDIR"
       #:imported-modules ((android build java-intellij)
                           ,@%ant-build-system-modules)
       #:modules ((guix build ant-build-system)
                  (guix build utils)
                  (android build java-intellij))
       #:phases
       (let ((modules
              '("util-rt" "util" "core-api" "core-impl" "boot"
                "extensions" "java-psi-api" "java-psi-impl")))
         (modify-phases %standard-phases
           (add-after 'configure 'set-source-dir
             (set-source-dir modules))
           (add-before 'build 'copy-resources
             (copy-resources modules))))))
    (propagated-inputs
     (list java-asm-for-intellij-133
           java-asm4-for-intellij-133
           java-automaton
           java-cglib
           java-eawtstub
           java-guava
           java-imgscalr
           java-iq80-snappy
           java-jakarta-oro
           java-jdom-for-intellij-133
           java-jsr166e-for-intellij-133
           java-log4j-1.2-api
           java-log4j-api
           java-native-access-for-intellij-133
           java-native-access-platform-for-intellij-133
           java-picocontainer-1
           java-snappy
           java-trove4j-intellij
           java-xstream))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public intellij-sdk-139-intellij-core-analysis
  (package
    (name "intellij-sdk-intellij-core-analysis")
    (version intellij-community-139-version)
    (source intellij-community-139-source)
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "intellij-core-analysis.jar"
       #:tests? #f
       #:source-dir "SOURCEDIR"
       #:imported-modules ((android build java-intellij)
                           ,@%ant-build-system-modules)
       #:modules ((guix build ant-build-system)
                  (guix build utils)
                  (android build java-intellij))
       #:phases
       (let ((modules
              '("analysis-api" "boot" "core-api" "duplicates-analysis"
                "editor-ui-api" "editor-ui-ex" "extensions" "indexing-api"
                "java-analysis-api" "java-indexing-api" "java-psi-api"
                "java-structure-view" "jps-model-api" "jps-model-serialization"
                "projectModel-api" "structure-view-api" "util" "util-rt"
                "xml-analysis-api" "xml-psi-api" "xml-structure-view-api"
                "analysis-impl" "core-impl" "indexing-impl" "java-analysis-impl"
                "java-indexing-impl" "java-psi-impl" "projectModel-impl"
                "structure-view-impl" "xml-analysis-impl" "xml-psi-impl"
                "xml-structure-view-impl")))
         (modify-phases %standard-phases
           (add-after 'configure 'set-source-dir
             (set-source-dir modules))
           (add-before 'build 'copy-resources
             (copy-resources modules))))))
    (propagated-inputs
      (list java-apache-xml-commons-resolver
            java-automaton
            java-asm5-for-intellij-135
            java-asm-for-intellij-133
            java-cglib
            java-commons-codec
            java-eawtstub
            java-guava
            java-iq80-snappy
            java-jakarta-oro
            java-jaxp
            java-jdom-for-intellij-133
            java-jsr166e-for-intellij-133
            java-log4j-1.2-api
            java-log4j-api
            java-nanoxml
            java-native-access-for-intellij-133
            java-native-access-platform-for-intellij-133
            java-picocontainer-1
            java-snappy
            java-trove4j-intellij
            java-xerces
            java-xmlbeans-binary
            java-xstream))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(map
  intellij-143-package
  (list intellij-sdk-133-annotations intellij-sdk-133-intellij-core
        intellij-sdk-133-jps-model intellij-sdk-133-javac2))
