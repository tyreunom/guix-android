;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android packages clang)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages cross-base)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages python)
  #:use-module (guix build-system cmake)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix memoization)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:export (cross-clang
            cross-llvm
            clang-for-target))

(define llvm-uri (@@ (gnu packages llvm) llvm-uri))

(define (cross-llvm target llvm)
  (package
    (inherit llvm)
    (arguments
     (substitute-keyword-arguments (package-arguments llvm)
      ((#:configure-flags flags)
       #~(append
           (list
             (string-append "-DLLVM_DEFAULT_TARGET_TRIPLE=" #$target)
             (string-append "-DLLVM_TARGET_ARCH="
                            #$(system->llvm-target (gnu-triplet->nix-system target))))
           #$flags))))))

(define* (cross-clang target
                      #:key
                      (libc (cross-libc target))
                      (xgcc (cross-gcc target
                              #:xbinutils (cross-binutils target)
                              #:libc libc))
                      (clang clang))
  "Return a cross-clang compiler for target."
  (define cross-clang-aux
    (mlambda (target libc xgcc clang)
      (package
        (inherit clang)
        (name (string-append "clang-cross-" target))
        (version (package-version clang))
        ;; Support the same variables as clang, even in cross-compilation context
        ;; Clang does not make a difference between native and cross-compilation.
        (search-paths
          (list
            (search-path-specification
              (variable "CROSS_LIBRARY_PATH")
              (files '("lib")))
            (search-path-specification
              (variable "CROSS_C_INCLUDE_PATH")
              (files '("include")))
            (search-path-specification
              (variable "CROSS_CPLUS_INCLUDE_PATH")
              (files '("include/c++" "include")))))
        (native-search-paths '())
        (arguments
         (substitute-keyword-arguments (package-arguments clang)
           ((#:configure-flags _)
            `(list "-DCLANG_INCLUDE_TESTS=True"
                   (string-append "-DGCC_INSTALL_PREFIX="
                                  (assoc-ref %build-inputs "cross-gcc-lib"))
                   (string-append "-DC_INCLUDE_DIRS="
                                  (or (assoc-ref %build-inputs "target-libc")
                                      (assoc-ref %build-inputs "libc"))
                                  "/include")))
           ((#:phases phases)
            #~(modify-phases #$phases
               (add-after 'unpack 'add-missing-libdir
                 (lambda _
                   ;; cross-gcc installs its libraries in <target>/lib instead of
                   ;; lib.
                   (substitute* #$(if (version>=? version "14")
                                      "clang/lib/Driver/ToolChain.cpp"
                                      "lib/Driver/ToolChain.cpp")
                     (("\"-L\"\\) \\+ LibPath\\)\\);")
                      #$(string-append "\"-L\") + LibPath));
  CmdArgs.push_back(Args.MakeArgString(StringRef(\"-L\") + "
                                     "StringRef(GCC_INSTALL_PREFIX) + StringRef(\"/"
                                     target "/lib\")));
  CmdArgs.push_back(Args.MakeArgString(StringRef(\"-rpath=\") + "
                                     "StringRef(GCC_INSTALL_PREFIX) + StringRef(\"/"
                                     target "/lib\")));")))))
               (add-after 'unpack 'support-cross-library-path
                 (lambda _
                   ;; LIBRARY_PATH is only supported for native builds, but we still
                   ;; need it (or CROSS_LIBRARY_PATH to be precise) when
                   ;; cross-compiling
                   #$(cond
                      ((version>=? version "10")
                       `(substitute* ,(if (version>=? version "14")
                                          "clang/lib/Driver/ToolChains/CommonArgs.cpp"
                                          "lib/Driver/ToolChains/CommonArgs.cpp")
                          ((", \"LIBRARY_PATH\"")
                           ", \"LIBRARY_PATH\");
  else
    addDirectoryList(Args, CmdArgs, \"-L\", \"CROSS_LIBRARY_PATH\"")))
                      ((version>=? version "6.0")
                       `(substitute* "lib/Driver/ToolChains/CommonArgs.cpp"
                          (("LIBRARY_PATH\"")
                           "LIBRARY_PATH\");
  } else {
    addDirectoryList(Args, CmdArgs, \"-L\", \"CROSS_LIBRARY_PATH\"")))
                      (else
                        `(substitute* "lib/Driver/Tools.cpp"
                          (("LIBRARY_PATH\"")
                           "LIBRARY_PATH\");
  else
    addDirectoryList(Args, CmdArgs, \"-L\", \"CROSS_LIBRARY_PATH\""))))))
               (replace 'set-glibc-file-names
                 (lambda* (#:key inputs #:allow-other-keys)
                   (let ((libc (or (assoc-ref inputs "target-libc")
                                   (assoc-ref inputs "libc")))
                         (compiler-rt (assoc-ref inputs "clang-runtime"))
                         (gcc (assoc-ref inputs "cross-gcc")))
                     (setenv "LIBRARY_PATH"
                             (string-append
                               (assoc-ref inputs "libc") "/lib:" (getenv "LIBRARY_PATH")))
                     #@(cond
                        ((version>=? version "6.0")
                         `(;; Link to libclang_rt files from clang-runtime.
                           (substitute* "lib/Driver/ToolChain.cpp"
                             (("getDriver\\(\\)\\.ResourceDir")
                              (string-append "\"" compiler-rt "\"")))

                           ;; Make "LibDir" refer to <glibc>/lib so that it
                           ;; uses the right dynamic linker file name.
                           (substitute* "lib/Driver/ToolChains/Linux.cpp"
                             (("(^[[:blank:]]+LibDir = ).*" _ declaration)
                              (string-append declaration "\"" libc "/lib\";\n"))

                             ;; Make clang look for libstdc++ in the right
                             ;; location.
                             (("LibStdCXXIncludePathCandidates\\[\\] = \\{")
                              (string-append
                               "LibStdCXXIncludePathCandidates[] = { \"" gcc
                               "/include/c++\","))

                             ;; Make sure libc's libdir is on the search path, to
                             ;; allow crt1.o & co. to be found.
                             (("@GLIBC_LIBDIR@")
                              (string-append libc "/lib")))))
                        (else
                         `((substitute* "lib/Driver/Tools.cpp"
                             ;; Patch the 'getLinuxDynamicLinker' function so that
                             ;; it uses the right dynamic linker file name.
                             (("/lib64/ld-linux-x86-64.so.2")
                              (string-append libc ,(glibc-dynamic-linker))))

                           ;; Link to libclang_rt files from clang-runtime.
                           ;; This substitution needed slight adjustment in 3.8.
                           ,(if (version>=? version "3.8")
                                 '((substitute* "lib/Driver/Tools.cpp"
                                     (("TC\\.getDriver\\(\\)\\.ResourceDir")
                                      (string-append "\"" compiler-rt "\""))))
                                 '((substitute* "lib/Driver/ToolChain.cpp"
                                     (("getDriver\\(\\)\\.ResourceDir")
                                      (string-append "\"" compiler-rt "\"")))))

                           ;; Make sure libc's libdir is on the search path, to
                           ;; allow crt1.o & co. to be found.
                           (substitute* "lib/Driver/ToolChains.cpp"
                             (("@GLIBC_LIBDIR@")
                              (string-append libc "/lib")))))))))))))
        (inputs
         `(,@(if libc
                 `(("target-libc" ,libc))
                 '())
           ("cross-gcc-lib" ,xgcc "lib")
           ("cross-gcc" ,xgcc)
           ,@(package-inputs clang)))
        (propagated-inputs
          (modify-inputs (package-propagated-inputs clang)
            (replace "clang-runtime"
              (let ((base (car (assoc-ref (package-propagated-inputs clang)
                                          "clang-runtime"))))
                (if libc
                  (with-parameters ((%current-target-system target)) base)
                  base)))
            (replace "llvm"
              (cross-llvm
                target
                (car (assoc-ref (package-propagated-inputs clang) "llvm")))))))))
  (cross-clang-aux target libc xgcc clang))

(define* (clang-for-target #:optional
                           (clang clang)
                           (libc (if (%current-target-system)
                                     (cross-libc (%current-target-system))
                                     #f)))
  (if (%current-target-system)
      (cross-clang (%current-target-system)
                   #:libc libc
                   #:xgcc (cross-gcc (%current-target-system)
                            #:xbinutils (cross-binutils (%current-target-system))
                            #:libc libc)
                   #:clang clang)
      clang))

(define-public libcxx-9
  (package
    (name "libcxx")
    (version "9.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (llvm-uri "libcxx" version))
       (sha256
        (base32
         "0d2bj5i6mk4caq7skd5nsdmz8c2m5w5anximl5wz3x32p08zz089"))))
    (build-system cmake-build-system)
    (arguments
     `(#:phases
       (modify-phases (@ (guix build cmake-build-system) %standard-phases)
         (add-after 'set-paths 'adjust-CPLUS_INCLUDE_PATH
           (lambda* (#:key inputs native-inputs #:allow-other-keys)
             (format #t "environment variable `CPLUS_INCLUDE_PATH` set to ~a~%"
                     (getenv "CPLUS_INCLUDE_PATH"))
             (let ((gcc (or (assoc-ref (or native-inputs inputs) "gcc")))
                   (cross-gcc (assoc-ref native-inputs "cross-gcc")))
               ;; Hide GCC's C++ headers so that they do not interfere with
               ;; the ones we are attempting to build.
               (setenv "CPLUS_INCLUDE_PATH"
                       (string-join (delete (string-append gcc "/include/c++")
                                            (string-split (getenv "CPLUS_INCLUDE_PATH")
                                                          #\:))
                                    ":"))
               (when cross-gcc
                 (setenv "CROSS_CPLUS_INCLUDE_PATH"
                         (string-join (delete (string-append cross-gcc "/include/c++")
                                              (string-split (getenv "CPLUS_INCLUDE_PATH")
                                                            #\:))
                                      ":")))
               (format #t
                       "environment variable `CPLUS_INCLUDE_PATH' changed to ~a~%"
                       (getenv "CPLUS_INCLUDE_PATH")))
             #t)))))
    (inputs
     `(("llvm" ,llvm-9)))
    (native-inputs
     `(("clang" ,(clang-for-target clang-9))))
    (home-page "https://libcxx.llvm.org")
    (synopsis "C++ standard library")
    (description
     "This package provides an implementation of the C++ standard library for
use with Clang, targeting C++11, C++14 and above.")
    (license license:expat)))

(define-public libcxx-12
  (package
    (inherit libcxx-9)
    (version "12.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (llvm-uri "llvm-project" version))
       (sha256
        (base32
         "03ps7akvbkxxa4xzal49v7lw3bz41zmjsp6fa7cslxrns5fb570j"))
       (patches (search-patches "patches/libcxx-disable-uncaught-exception.patch"))))
    (arguments
     (substitute-keyword-arguments (package-arguments libcxx-9)
       ((#:phases phases)
        `(modify-phases ,phases
          (add-after 'unpack 'cd-libcxx
            (lambda _
              (chdir "libcxx")))))))
    (inputs
      `(("llvm" ,llvm-12)))
    (native-inputs
      `(("clang" ,(clang-for-target clang-12))
        ("python" ,python)))))

(define-public libcxxabi-9
  (package
    (inherit libcxxabi-6)
    (name "libcxxabi")
    (version (package-version clang-9))
    (source
     (origin
       (method url-fetch)
       (uri (llvm-uri "libcxxabi" version))
       (sha256
        (base32
         "1b4aiaa8cirx52vk2p5kfk57qmbqf1ipb4nqnjhdgqps9jm7iyg8"))))
    (arguments
     `(#:configure-flags
       (list (string-append "-DLIBCXXABI_LIBCXX_INCLUDES="
                            (assoc-ref %build-inputs "libcxx")
                            "/include")
             "-DCMAKE_C_COMPILER=clang"
             "-DCMAKE_CXX_COMPILER=clang++")
       #:phases
       (modify-phases (@ (guix build cmake-build-system) %standard-phases)
         (add-after 'unpack 'adjust-CPLUS_INCLUDE_PATH
           (lambda* (#:key inputs native-inputs #:allow-other-keys)
             (define (delete* what lst)
               (if (null? what)
                   lst
                   (delete* (cdr what) (delete (car what) lst))))

             (let ((gcc (or (assoc-ref inputs  "gcc") (assoc-ref native-inputs "gcc")))
                   (cross-gcc (assoc-ref native-inputs "cross-gcc")))
               ;; Hide GCC's C++ headers so that they do not interfere with
               ;; the ones we are attempting to build.
               (setenv "CPLUS_INCLUDE_PATH"
                       (string-join
                        (cons (string-append
                               (assoc-ref inputs "libcxx") "/include/c++/v1")
                              (delete*
                                `(,(string-append gcc "/include/c++")
                                  ,@(if cross-gcc
                                        `(,(string-append cross-gcc "/include/c++"))
                                        '()))
                                (string-split (getenv "CPLUS_INCLUDE_PATH")
                                              #\:)))
                        ":"))
               (when cross-gcc
                 (setenv "CROSS_CPLUS_INCLUDE_PATH"
                         (string-join
                          (cons (string-append
                                  (assoc-ref inputs "libcxx") "/include/c++/v1")
                                (delete*
                                  (list (string-append cross-gcc "/include/c++")
                                        (string-append gcc "/include/c++"))
                                  (string-split (getenv "CROSS_CPLUS_INCLUDE_PATH")
                                                #\:)))
                          ":")))
               (format #true
                       "environment variable `CROSS_CPLUS_INCLUDE_PATH' changed to ~a~%"
                       (getenv "CROSS_CPLUS_INCLUDE_PATH"))
               (format #true
                       "environment variable `CPLUS_INCLUDE_PATH' changed to ~a~%"
                       (getenv "CPLUS_INCLUDE_PATH")))))
         (add-after 'install 'install-headers
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((include-dir (string-append
                                 (assoc-ref outputs "out") "/include")))
               (mkdir-p include-dir)
               (install-file ,(string-append "../libcxxabi-" version
                                             ".src/include/__cxxabi_config.h")
                             include-dir)
               (install-file ,(string-append "../libcxxabi-" version
                                             ".src/include/cxxabi.h")
                             include-dir)))))))
    (inputs
     (list llvm-9 libcxx-9))
    (native-inputs
     (list (clang-for-target clang-9)))))

(define-public libcxxabi-12
  (package
    (inherit libcxxabi-9)
    (version "12.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (llvm-uri "llvm-project" version))
       (sha256
        (base32
         "03ps7akvbkxxa4xzal49v7lw3bz41zmjsp6fa7cslxrns5fb570j"))))
    (arguments
     (substitute-keyword-arguments (package-arguments libcxxabi-9)
       ((#:phases phases)
        `(modify-phases ,phases
          (add-after 'unpack 'cd-libcxx
            (lambda _
              (chdir "libcxxabi")))
          (replace 'install-headers
            (lambda* (#:key outputs #:allow-other-keys)
              (pk 'current-dir (getcwd))
              (let ((include-dir (string-append
                                  (assoc-ref outputs "out") "/include")))
                (mkdir-p include-dir)
                (install-file "../libcxxabi/include/__cxxabi_config.h" include-dir)
                (install-file "../libcxxabi/include/cxxabi.h" include-dir))))))))
    (inputs
      (list llvm-12 libcxx-12))
    (native-inputs
      `(("clang" ,(clang-for-target clang-12))
        ("python" ,python)))))
