;;; GNU Guix --- Functional package management for GNU
;;; Automatically imported by (android import repo).
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
;;; Copyright © 2021,2022 Julien Lepiller <julien@lepiller.eu>

(define-module (android packages android-sources)
  #:use-module (android build blueprint)
  #:use-module (guix packages)
  #:use-module (guix git-download))

(define-public android-platform-art-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/art")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-art-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1xqbspynxs4375l8v2xn2f67q9821x1spkc68fhy0n8fwnz0pngq"))
    (modules '((guix build utils)))
    (snippet
      `(begin
         (substitute* "libdexfile/dex/dex_file.cc"
           (("void EncodedArrayValueIterator::Next")
            "std::ostream& operator<<(std::ostream& os, EncodedArrayValueIterator::ValueType code) {
  os << \"ValueType[\" << static_cast<int>(code) << \"]\";
  return os;
}
void EncodedArrayValueIterator::Next"))))))

(define-public android-platform-build-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/build")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-build-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1x7v4amarshm74azgxnbbwfm92m7xsrj605r3nn7ss5r9y7355lc"))))

(define-public android-platform-build-soong-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/build/soong")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-build-soong-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1lsqr0fb0s1k8ngl56f3qrygc2q79p5034h7xh6akp5lnj71nqix"))))

(define-public android-platform-dalvik-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/dalvik")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-dalvik-13-source"
        %latest-android-13))
    (sha256
      (base32
        "03p5l8hqm108h9l5ppcsj0g7m0nzgaprzrxlm97adk4km20sq2yz"))))

(define-public android-platform-development-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/development")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-development-13-source"
        %latest-android-13))
    (sha256
      (base32
        "07lf8sd21b16hn6hignqawf3661dvfwcc7p4sbpnbwb4a50w4myr"))))

(define-public android-platform-external-avb-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/external/avb")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-external-avb-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1p1zgmg398ggcsvij4s7aa18i4piggc0h5zpzv6wq5q48wigws56"))))

(define-public android-platform-external-boringssl-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/external/boringssl")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-external-boringssl-13-source"
        %latest-android-13))
    (sha256
      (base32
        "19vywdbicjs85dflvyssjpjxdwjqnp8yjszjyb5sbyzmqzxi9js4"))))

(define-public android-platform-external-brotli-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/external/brotli")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-external-brotli-13-source"
        %latest-android-13))
    (sha256
      (base32
        "0jjahi9515i5fchmym5rc9fk63hh8dmmbacpvf5q8bfb96vc52bg"))))

(define-public android-platform-external-conscrypt-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/external/conscrypt")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-external-conscrypt-13-source"
        %latest-android-13))
    (sha256
      (base32
        "05iw31cw72x3akkky0v5flgv56y1dj18y6l5v4yhi1bnjbpyvwy3"))))

(define-public android-platform-external-expat-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/external/expat")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-external-expat-13-source"
        %latest-android-13))
    (sha256
      (base32
        "0349i6a0lgp34v2l8zw24w3gcvvxz3axjkfsnl482x1cga2k7w4q"))))

(define-public android-platform-external-fmtlib-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/external/fmtlib")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-external-fmtlib-13-source"
        %latest-android-13))
    (sha256
      (base32
        "02pn8c5qp6rgqkq4nmycz5lbdkg312yi75n1szk81xbbzvvdn1fb"))))

(define-public android-platform-external-googletest-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/external/googletest")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-external-googletest-13-source"
        %latest-android-13))
    (sha256
      (base32
        "0rklkpj9y4kl3q27qqi6h9xj5r5n9r583v44i3qk4439q6p0by7j"))))

(define-public android-platform-external-libpng-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/external/libpng")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-external-libpng-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1x2c2al3b20k6irkx7ggxh7hxii177hsfv7niqvi58pglqc7qa2k"))))

(define-public android-platform-external-libusb-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/external/libusb")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-external-libusb-13-source"
        %latest-android-13))
    (sha256
      (base32
        "0r53lqdf5c6j2954l2r7c1i1caizl5r0vxs73g0m8ylhrpl1gkf5"))))

(define-public android-platform-external-lz4-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/external/lz4")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-external-lz4-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1fkqdrcr6g509md86zd3k7x1qvz9cvd6z8n9rpdkfn06k5mmcx3k"))))

(define-public android-platform-external-mdnsresponder-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/external/mdnsresponder")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-external-mdnsresponder-13-source"
        %latest-android-13))
    (sha256
      (base32
        "0r2pv3zwb2hba4snbf0lxza91z5fnhari2cbnryi5l9q5gq5k82f"))))

(define-public android-platform-external-openscreen-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/external/openscreen")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-external-openscreen-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1f0ywgficigqqikb8wxfvd7k2aca8121lcs8whh68b5mgihrpmm7"))))

(define-public android-platform-external-protobuf-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/external/protobuf")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-external-protobuf-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1g41av30i917ck19jvnrhdm1cmbpwhyxvqycbhfq8azfcn4w30bm"))))

(define-public android-platform-external-zlib-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/external/zlib")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-external-zlib-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1m6mnd1sa77fzsm2cywqgvk93vjiw6gsrjy04kj51n0hhs6kfml8"))))

(define-public android-platform-external-zopfli-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/external/zopfli")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-external-zopfli-13-source"
        %latest-android-13))
    (sha256
      (base32
        "12s7zms6qd5494rpq9v8smx094br4ziggn9ja171ina4dqbdjvai"))))

(define-public android-platform-external-zstd-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/external/zstd")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-external-zstd-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1hwg0yl0f5mzryfv59a3p9633hhzpgd2hnrz22hlgpcfysy0vby5"))))

(define-public android-platform-frameworks-av-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/frameworks/av")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-frameworks-av-13-source"
        %latest-android-13))
    (sha256
      (base32
        "0gmqjzrnb69bgsk8zj3pl2zp105b3xmngrxnilm0wawb7biknl7c"))))

(define-public android-platform-frameworks-base-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/frameworks/base")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-frameworks-base-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1j65vg0hypzyps6mxbfmqwgz2ii2xmf9vrhamj4h0b1hlmz3wbn6"))
    (modules '((guix build utils)))
    (snippet
     `(begin
        (substitute* (find-files "tools/aapt2" "\\.proto$")
          (("frameworks/base/tools/aapt2/") ""))))))

(define-public android-platform-frameworks-native-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/frameworks/native")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-frameworks-native-13-source"
        %latest-android-13))
    (sha256
      (base32
        "070r8wdj885wxvk4r2bm6l0vahv47hyia4i0d8nkc250ljx00ms2"))))

(define-public android-platform-hardware-libhardware-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/hardware/libhardware")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-hardware-libhardware-13-source"
        %latest-android-13))
    (sha256
      (base32
        "0na6qclc6kzxzzvrsc9z7hdbq3rzqvjwz2yc7158b0rv6nq9hmbz"))))

(define-public android-platform-hardware-libhardware-legacy-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/hardware/libhardware_legacy")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-hardware-libhardware-legacy-13-source"
        %latest-android-13))
    (sha256
      (base32
        "0a5hqxs1z2qcnzgzghhx817p26fin8arkhgglxrq0c8j3ymyippl"))))

(define-public android-platform-hardware-ril-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/hardware/ril")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-hardware-ril-13-source"
        %latest-android-13))
    (sha256
      (base32
        "0cmm9nx9wrnk151sn256wd9sf29085vcrg56h6nkfldlcz96c3v9"))))

(define-public android-platform-libnativehelper-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/libnativehelper")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-libnativehelper-13-source"
        %latest-android-13))
    (sha256
      (base32
        "091c752ys3rzd7v0pw7znjl28cl5hkqkrskba1zj63xd2bd0l65v"))))

(define-public android-platform-packages-modules-adb-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/packages/modules/adb")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-packages-modules-adb-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1265m28kiszw7y243gyi64hs9vi423y9flgyqjw7y9i6fxvxc05y"))
    (modules '((guix build utils)
               (ice-9 textual-ports)))
    (snippet
     `(begin
        (define (has-pragma-once file)
          (string-contains (call-with-input-file file get-string-all) "#pragma once"))
        (define (cut-on file char)
          (string-join (string-split file char) "_"))
        (define (replace-pragma file)
          (with-output-to-file (string-append file ".tmp")
            (lambda _
              (let ((guard (cut-on (cut-on file #\.) #\/)))
                (format #t "#ifndef ~a~%" guard)
                (format #t "#define ~a~%" guard)
                (display (call-with-input-file file get-string-all))
                (format #t "#endif~%"))))
          (rename-file (string-append file ".tmp") file))
        ;; Remove any reference to the SDK
        (substitute* "fastdeploy/deployagent/src/com/android/fastdeploy/ApkArchive.java"
          (("import android.util.Log;") "")
          (("Log.e.*") ""))
        ;; Rewrite #pragma once into header guards
        (for-each
          (lambda (file)
            (when (has-pragma-once file)
              (replace-pragma file)))
          (find-files "." "\\.h$"))))))

(define-public android-platform-system-core-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/system/core")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-system-core-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1npx891yn4nwyvhajzm1cnb89syk7fzz9gz5wwy6bglgn4pbgvcm"))))

(define-public android-platform-system-extras-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/system/extras")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-system-extras-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1xcrvrli71czq00jqrhpa57z05w0hhrg6h474r4i1r3dmwm9i32m"))))

(define-public android-platform-system-incremental-delivery-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/system/incremental_delivery")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-system-incremental-delivery-13-source"
        %latest-android-13))
    (sha256
      (base32
        "02wmczggprl4w2rx335pssxqwvhp2yiycg72afy9y176b16q8140"))))

(define-public android-platform-system-libbase-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/system/libbase")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-system-libbase-13-source"
        %latest-android-13))
    (sha256
      (base32
        "12nhyizrsm33mvrma4yfm8hs6bmpkm76ffrdxlryvp85qqpx8ym6"))))

(define-public android-platform-system-libziparchive-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/system/libziparchive")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-system-libziparchive-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1zkcr7ffbmhxjhbmwac50da7ndcvxixhysd05x6cj1y9kf17lfb1"))))

(define-public android-platform-system-logging-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/system/logging")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-system-logging-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1fd9c8xs1v47rbnf42nx5n3lzz8z7nmiby6vyxfn1f9j0a3in94i"))))

(define-public android-platform-system-media-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/system/media")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-system-media-13-source"
        %latest-android-13))
    (sha256
      (base32
        "0ha1k89bmvv178alcp2wrlgvn60plvmzqq44vv2lmhirwbb505d7"))))

(define-public android-platform-system-tools-aidl-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/system/tools/aidl")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-system-tools-aidl-13-source"
        %latest-android-13))
    (sha256
      (base32
        "0486mv1qsa20dsx4v1799xmnvycm7pcac4ws3i129wx76vfm7fxf"))))

(define-public android-platform-system-tools-mkbootimg-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/system/tools/mkbootimg")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-system-tools-mkbootimg-13-source"
        %latest-android-13))
    (sha256
      (base32
        "03kgm1q4mbwbw8ixqqqj3cq6z7lfgq7wc27yxxsx99s1sqyxvb4g"))))

(define-public android-platform-system-unwinding-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/system/unwinding")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-system-unwinding-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1mgfvwcn22aamwx4af1jzvg0b2q74fhcylq70dk81b3znajlpzxf"))))

(define-public android-platform-tools-apksig-13-source
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/tools/apksig")
           (commit %latest-android-13)))
    (file-name
      (git-file-name
        "android-platform-tools-apksig-13-source"
        %latest-android-13))
    (sha256
      (base32
        "1vmxxnz27bj4rdshk8r651d5114018w15p5zlgcy6c87sf3f5j3a"))))
