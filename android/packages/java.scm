;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021, 2023 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android packages java)
  #:use-module (android packages intellij)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages apr)
  #:use-module (gnu packages batik)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages java)
  #:use-module (gnu packages java-xml)
  #:use-module (gnu packages javascript)
  #:use-module (gnu packages maven-parent-pom)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages protobuf)
  #:use-module (gnu packages xml)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system ant)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system maven))

(define-public java-xmlbeans-binary
  (package
    (name "java-xmlbeans")
    (version "5.0.3")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://apache/poi/xmlbeans/release/bin/"
                                  "xmlbeans-bin-" version "-20211229.tgz"))
              (sha256
               (base32
                "1zq84lk1wrsjq5mf4g4maqdchbs4d9dra5zcnzip0gzf7si0yy5c"))))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       '(("lib/xmlbeans-5.0.3.jar" "lib/"))))
    (home-page "https://xmlbeans.apache.org")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public java-paranamer
  (package
    (name "java-paranamer")
    (version "2.8")
    (source (origin
              (method git-fetch)
              ;; This repository is a mirror of the svn directory that disappeared
              ;; along with codehaus.org
              (uri (git-reference
                     (url "https://github.com/paul-hammant/paranamer")
                     (commit (string-append "paranamer-parent-" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1jfick1sfjmqdxak72h5sx7xcyq0njwsg64jp4xaz06365ghbiz9"))
              (modules '((guix build utils)))
              (snippet
                `(for-each delete-file (find-files "." ".*.jar")))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "paranamer.jar"
       #:source-dir "paranamer/src/java"
       #:tests? #f))
    (inputs
     `(("java-javax-inject" ,java-javax-inject)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:bsd-3)))

(define-public java-picocontainer
  (package
    (name "java-picocontainer")
    (version "2.15")
    (source (origin
              (method git-fetch)
              ;; This repository is a mirror of the svn directory that disappeared
              ;; along with codehaus.org
              (uri (git-reference
                     (url "https://github.com/codehaus/picocontainer")
                     (commit "7be6b8b0eb33421dc7a755817628e06b79bd879d")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "06ygwa1wkk70lb9abc0shh1lzyysjaciqb5917qxsyn4rx43sjdg"))
              (modules '((guix build utils)))
              (snippet
                `(for-each delete-file (find-files "." ".*.jar")))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "picocontainer.jar"
       #:source-dir "container/src/java"
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'chdir
           (lambda _
             (chdir "java/2.x/tags/picocontainer-2.15")
             #t)))))
    (inputs
     `(("java-paranamer" ,java-paranamer)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:bsd-3)))

(define-public java-picocontainer-1
  (package
    (inherit java-picocontainer)
    (version "1.3")
    (arguments
     `(#:jar-name "picocontainer.jar"
       #:source-dir "container/src/java"
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'chdir
           (lambda _
             (chdir "java/1.x/picocontainer/tags/picocontainer-1_3")
             #t)))))))

(define-public java-batik
  (package
    (name "java-batik")
    (version "1.12")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://apache/xmlgraphics/batik/source/"
                                  "batik-src-" version ".tar.gz"))
              (sha256
               (base32
                "1g68mh5f57ap7827zb5y96ic587hf351f892fk80x2750drnw8zi"))))
    (build-system ant-build-system)
    (arguments
     `(#:test-target "regard"; FIXME: no test is actually run
       #:build-target "all-jar"
       #:phases
       (modify-phases %standard-phases
         (add-before 'check 'remove-failing
           (lambda _
             ;; This file looks for w3c.dom.Window, but it has been moved to
             ;; org.apache.batik.w3c.dom.Window.
             (delete-file "samples/tests/resources/java/sources/com/untrusted/script/UntrustedScriptHandler.java")
             #t))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((dir (string-append (assoc-ref outputs "out") "/share/java/")))
               (mkdir-p dir)
               (copy-file (string-append "batik-" ,version "/lib/batik-all-" ,version ".jar")
                          (string-append dir "batik-all.jar"))))))))
    (inputs
     `(("java-xmlgraphics-commons" ,java-xmlgraphics-commons)))
    (native-inputs
     `(("java-junit" ,java-junit)))
    (home-page "https://xmlgraphics.apache.org/batik")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public java-imagescalr
  (package
    (name "java-imagescalr")
    (version "4.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/rkalla/imgscalr")
                     (commit (string-append version "-release"))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1vbfx2wa9lavagmg2pgy5jq4m7msp1dkc4pwr7vv5a746fx24pg9"))))
    (build-system ant-build-system)
    (arguments
     `(#:tests? #f; no tests
       #:phases
       (modify-phases %standard-phases
         (replace 'install
           (install-jars ".")))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))


(define-public java-trove4j
  (package
    (name "java-trove4j")
    (version "3.0.3")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://bitbucket.org/trove4j/trove/downloads/"
                                  "trove-" version ".tar.gz"))
              (sha256
               (base32
                "1hlvhaivyw880bld5l8cf2z0s33vn9bb84w0m5n025c7g8fdwhk2"))
              (modules '((guix build utils)))
              (snippet
                `(begin
                   ;; Delete bundled jar archives.
                   (for-each delete-file (find-files "." ".*\\.jar"))
                   #t))))
    (build-system ant-build-system)
    (native-inputs
     `(("java-junit" ,java-junit)))
    (arguments
     `(#:test-target "test"
       #:phases
       (modify-phases %standard-phases
         (replace 'install
           (install-jars ".")))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:lgpl2.1+)))

(define-public java-trove4j-2
  (package
    (inherit java-trove4j)
    (version "2.1.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://sourceforge.net/projects/trove4j/"
                                  "files/trove/" version "/trove-" version ".tar.gz"))
              (sha256
               (base32
                "0vkbgq20xina558vymq6gxwjw5svhxsncizh3p3wdq5fjcgrx4m5"))
              (modules '((guix build utils)))
              (snippet
                `(begin
                   ;; Delete bundled jar archives.
                   (for-each delete-file (find-files "." ".*\\.jar"))
                   #t))))))

(define-public java-trove4j-intellij
  (package
    (inherit java-trove4j)
    (version "1.0-20191502")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/JetBrains/intellij-deps-trove4j")
                     (commit "5967df06f16dacca5a37493ae464f14696dc6f9d")))
              (file-name (git-file-name "java-trove4j" version))
              (sha256
               (base32
                "00swjb2yq85k6sh5hq1nfixv20hjnza4hd1b8frr3fb53ibsry4s"))
              (modules '((guix build utils)))
              (snippet
                `(begin
                   ;; Delete bundled jar archives.
                   (for-each delete-file (find-files "." ".*\\.jar"))
                   #t))))
    (arguments
     `(#:jar-name "trove.jar"
       #:tests? #f
       #:source-dir "core/src/main/java"
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'generate
           (lambda _
             (with-directory-excursion "generator/src/main/java"
               (invoke "javac" "gnu/trove/generate/Generate.java"))
             (invoke "java" "-cp" "generator/src/main/java"
                     "gnu.trove.generate.Generate"
                     "core/src/main/templates" "core/src/main/java")))
         (add-before 'build 'utf-to-iso
           (lambda _
             (substitute* "build.xml"
               (("<javac ") "<javac encoding=\"iso-8859-1\" "))
             #t)))))))


(define-public java-eawtstub
  (let ((commit "ae5d21ccbc62754eb0353b5b1d57abf1da954652"))
    (package
      (name "java-eawtstub")
      (version (git-version "0.0.0" "0" commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                       (url "https://github.com/consulo/eawtstub")
                       (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0l0xng9qxs3b5l86nb77vr65a924ginxd32sjiqjrb9lbh2yp7ap"))))
      (build-system ant-build-system)
      (arguments
       `(#:jar-name "eawtstub.jar"
         ;; No tests
         #:tests? #f))
      (home-page "")
      (synopsis "")
      (description "")
      (license license:asl2.0))))


(define-public java-automaton
  (package
    (name "java-automaton")
    (version "1.12.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/cs-au-dk/dk.brics.automaton")
                     (commit "328cf493ec2537af9d2bbce0eb4b4ef118b66547")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1vjcz8m8wlqx6j1cymww7mfh9ckxfwcfm63dvg3bg394l8qzbxg3"))))
    (build-system ant-build-system)
    (arguments
     `(#:tests? #f; no tests
       #:phases
       (modify-phases %standard-phases
         (replace 'install (install-jars ".")))))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))


;; Newer versions are not free software anymore
;; latest free versions are 1.8.1 and 1.8.0. We require something older for
;; intellij though.
(define-public java-jgoodies-common
  (package
    (name "java-jgoodies-common")
    (version "1.2.1")
    (source (origin
              (method url-fetch)
              (uri "http://www.jgoodies.com/download/libraries/common/jgoodies-common-1_2_1.zip")
              (sha256
               (base32
                "1yrdqg522ldijxp3yykz4320chhyf2a9bfapl71379kh3viyvqhg"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "jgoodies-common.jar"
       #:source-dir "src/core"
       #:tests? #f)); no tests
    (native-inputs
     `(("unzip" ,unzip)))
    (home-page "http://www.jgoodies.com")
    (synopsis "")
    (description "")
    (license license:bsd-3)))

(define-public java-jgoodies-forms
  (package
    (name "java-jgoodies-forms")
    (version "1.4.2")
    (source (origin
              (method url-fetch)
              (uri "http://www.jgoodies.com/download/libraries/forms/jgoodies-forms-1_4_2.zip")
              (sha256
               (base32
                "0qm9cpcw2zqbgz8376vds1jahlmj0x7j9aa2gc6yrp8z83qg4q6i"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "jgoodies-forms.jar"
       #:source-dir "src/core"
       #:tests? #f)); no tests
    (native-inputs
     `(("unzip" ,unzip)))
    (inputs
     `(("java-jgoodies-common" ,java-jgoodies-common)))
    (home-page "http://www.jgoodies.com")
    (synopsis "")
    (description "")
    (license license:bsd-3)))

(define-public java-spullara-cli-parser
  (package
    (name "java-spullara-cli-parser")
    (version "1.1.5")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/spullara/cli-parser")
                     (commit (string-append "cli-parser-" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1viysb4aws6nsbp4lil1fwwc69rr9s5z66g17iygmbq5lincz35y"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "spullara-cli-parser.jar"
       #:test-dir "src/test"
       #:source-dir "src/main/java"))
    (native-inputs
     `(("java-hamcrest-core" ,java-hamcrest-core)
       ("java-junit" ,java-junit)))
    (home-page "https://github.com/spullara/cli-parser")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public protobuf-2.5
  (package
    (inherit protobuf-2)
    (version "2.5.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/google/protobuf/releases/"
                                  "download/v" version "/protobuf-"
                                  version ".tar.gz"))
              (sha256
               (base32
                "13pmjg19i77jvidkn7drnpnprjxxk1mypx1jyzmdavwfagfa6nn5"))))))

;; TODO: Use git-fetch
(define-public java-protobuf-2.5
  (package
    (name "java-protobuf")
    (version (package-version protobuf-2.5))
    (source (package-source protobuf-2.5))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "protobuf.jar"
       #:source-dir "java/src/main/java"
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'build-protos
           (lambda _
             (invoke "protoc" "--java_out=java/src/main/java" "-Isrc"
                     "src/google/protobuf/descriptor.proto"))))))
    (inputs
     `(("java-guava" ,java-guava)))
    (native-inputs
     `(("java-junit" ,java-junit)
       ("protobuf" ,protobuf-2.5)))
    (home-page (package-home-page protobuf))
    (synopsis "")
    (description "")
    (license license:bsd-3)))

(define-public java-nanoxml
  (package
    (name "java-nanoxml")
    (version "2.2.3.dfsg-9")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://salsa.debian.org/java-team/libnanoxml2-java")
                     (commit "debian/2.2.3.dfsg-9")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "04pxpvmlqfqjjn5386j5si0q7q6834hff3yi8qrfz9zc2pnbm9lh"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "nanoxml.jar"
       #:source-dir "Sources/Java"
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'fix-enum
           (lambda _
             (substitute* (find-files "Sources/Java" "\\.java$")
               ((" enum ") " myenum ")
               ((" enum\\.") " myenum.")
               (("\\(enum\\.") "(myenum.")
               ))))))
    (home-page "http://nanoxml.sourceforge.net/orig")
    (synopsis "")
    (description "")
    (license license:bsd-3)))

(define-public java-jna
  (package
    (inherit java-native-access)
    (name "java-jna")
    (arguments
     (substitute-keyword-arguments (package-arguments java-native-access)
      ((#:phases phases)
       `(modify-phases ,phases
         (add-before 'install 'prepare-maven-install
           (lambda _
             (substitute* "pom-jna.xml"
               (("-SNAPSHOT") ""))
             (rename-file "build/jna.jar"
                          (string-append "jna-" ,(package-version java-native-access)
                                         ".jar"))))
         (replace 'install
           (install-from-pom "pom-jna.xml"))))))))

(define-public java-jna-platform
  (package
    (inherit java-native-access-platform)
    (name "java-jna-platform")
    (arguments
     (substitute-keyword-arguments (package-arguments java-native-access-platform)
      ((#:phases phases)
       `(modify-phases ,phases
         (replace 'fix-ant
           (lambda* (#:key inputs #:allow-other-keys)
             (substitute* "nbproject/project.properties"
               (("../../build/jna.jar")
                (car (find-files (assoc-ref inputs "java-jna")
                                 "\\.jar$")))
               (("../../lib/hamcrest-core-.*.jar")
                (car (find-files (assoc-ref inputs "java-hamcrest-core")
                                 "jar$")))
               (("../../lib/junit.jar")
                (car (find-files (assoc-ref inputs "java-junit")
                                 "jar$"))))))
         (add-before 'install 'prepare-maven-install
           (lambda _
             (substitute* "../../pom-jna-platform.xml"
               (("-SNAPSHOT") ""))))
         (replace 'install
           (install-from-pom "../../pom-jna-platform.xml"))))))
    (inputs
     `(("java-jna" ,java-jna)))))

(define-public apache-parent-pom-23
  ((@@ (gnu packages maven-parent-pom) make-apache-parent-pom)
    "23" "05c8i741f0m4311q264zvq0lc6srsyz2x95ga4d7qfd89swkzg9d"))

(define-public apache-commons-parent-pom-52
  (let* ((base ((@@ (gnu packages maven-parent-pom) make-apache-commons-parent-pom)
                 "52" "0fb6id9cs9944fjlirjc07bf234bwi96i642px09m9nrfj338n5d"
                 apache-parent-pom-23))
         (source (package-source base))
         (uri (origin-uri source)))
    (package
      (inherit base)
      (source (origin
                (inherit source)
                (uri (git-reference
                       (inherit uri)
                       (commit "rel/commons-parent-52"))))))))

(define-public java-javacomm
  (package
    (name "java-javacomm")
    (version "0.0.24")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/nyholku/purejavacomm")
                     (commit "9cb9f1fa63e59344ac3425e9a8ab973192bed2d3")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0g5bfnh15kfm78qg51rvbi75cpb1a9qzfxn5gzlk7r48cis2mag4"))))
    (build-system maven-build-system)
    (arguments
     `(#:exclude
       (("org.apache.maven.wagon" . ("wagon-ssh" "wagon-ftp"))
        ("org.apache.maven.plugins" . ("maven-deploy-plugin")))))
    (inputs
     `(("apache-commons-parent-pom" ,apache-commons-parent-pom-52)
       ("java-jna" ,java-jna)
       ("java-jna-platform" ,java-jna-platform)))
    (home-page "https://github.com/nyholku/purejavacomm")
    (synopsis "")
    (description "")
    (license license:bsd-3)))

(define-public java-pty4j
  (package
    (name "java-pty4j")
    (version "0.9.4")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/JetBrains/pty4j")
                     (commit version)))
              (sha256
               (base32
                "1vnaax2mzbk4df80cw979qgmkm5b476l6khs49sm7k3qvj29ihvl"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "pty4j.jar"
       #:tests? #f))
    (inputs
     `(("intellij-sdk-133-annotations" ,intellij-sdk-133-annotations)
       ("java-guava" ,java-guava)
       ("java-javacomm" ,java-javacomm)
       ("java-log4j-api" ,java-log4j-api)
       ("java-log4j-1.2-api" ,java-log4j-1.2-api)
       ("java-native-access" ,java-native-access)
       ("java-native-access-platform" ,java-native-access-platform)))
    (home-page "https://github.com/JetBrains/pty4j")
    (synopsis "")
    (description "")
    (license license:bsd-3)))

(define-public boringssl
  (package
    (name "boringssl")
    ;; Latest version at the time of packaging
    (version "32e59d2d3264e4e104b355ef73663b8b79ac4093")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://boringssl.googlesource.com/boringssl")
                     (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0hk90f5svx5zs86syydg3wy4b75m9469byrzj71xsyw6lh6zajjd"))))
    (build-system cmake-build-system)
    (arguments
     `(#:tests? #f; require ninja?
       #:configure-flags (list "-DBUILD_SHARED_LIBS=1")
       #:validate-runpath? #f; FIXME: this is buggy :/
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'change-home
           (lambda _
             (setenv "HOME" "/tmp")
             #t))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (lib (string-append out "/lib"))
                    (include (string-append out "/include")))
               (install-file "ssl/libssl.so" lib)
               (install-file "crypto/libcrypto.so" lib)
               (copy-recursively "../source/include" include))
             #t)))))
    (native-inputs
     `(("go" ,go)
       ("perl" ,perl)
       ("pkg-config" ,pkg-config)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:expat)))

(define-public java-conscrypt
  (package
    (name "java-conscrypt")
    (version "1.4.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/google/conscrypt.git")
                     (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1wfcc7gspvxxhm4vwb0yfidpsjj6kcdqnfwj2qsr4zfppn01gv7f"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "conscrypt.jar"
       #:source-dir "openjdk/src/main/java:common/src/main/java"
       #:test-dir "openjdk/src/test"
       #:tests? #f; conscrypt-testing require libcore from android
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'build-library
           (lambda* (#:key inputs #:allow-other-keys)
             (mkdir-p "build/classes/META-INF/native")
             (let ((jni-include (string-append "-I" (assoc-ref inputs "jdk")
                                               "/include/linux")))
               (for-each
                 (lambda (cpp-file)
                   (invoke "g++" "-c" cpp-file "-o" (string-append cpp-file ".o")
                           "-std=c++11" "-fPIC" "-O2" "-Icommon/src/jni/main/include"
                           "-Icommon/src/jni/unbundled/include" jni-include))
                 (find-files "common/src/jni/main/cpp" ".*.cc$")))
             (apply invoke "gcc" "-o"
                    "build/classes/META-INF/native/libconscrypt.so"
                    "-shared"
                    (find-files "common/src/jni/main/cpp" ".*.o$"))
             #t))
         (add-before 'build 'generate-constants
           (lambda _
             (invoke "g++" "-std=c++11" "-O2" "-o" "gen_constants"
                     "constants/src/gen/cpp/generate_constants.cc")
             (with-output-to-file "common/src/main/java/org/conscrypt/NativeConstants.java"
               (lambda _
                 (invoke "./gen_constants")))
             #t))
         ;(add-before 'build 'build-testing
         ;  (lambda _
         ;    (mkdir-p "build/testing-classes")
         ;    (apply invoke "javac" "-d" "build/testing-classes"
         ;           "-cp" (getenv "CLASSPATH")
         ;           (find-files "testing/src/main/java" ".*.java$"))
         ;    #t))
         (add-before 'check 'set-classpath
           (lambda _
             (setenv "CLASSPATH"
                     (string-append (getenv "CLASSPATH") ":build/testing-classes"))
             #t)))))
    (inputs
     `(("boringssl" ,boringssl)))
    ;(native-inputs
    ; `(("java-bouncycastle" ,java-bouncycastle)
    ;   ("java-mockito-1" ,java-mockito-1)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public java-eclipse-jetty-alpn-api
  (package
    (name "java-eclipse-jetty-alpn-api")
    (version "1.1.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/eclipse/jetty.alpn.api")
                     (commit "0a2671867f1ad7067bf4070e1b4209f8796d605d")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "11f3iq9sdy8si3f3cb9dhd3d9xc0l7cwrsra21jarw01cc9priij"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "jetty-alpn.jar"
       #:source-dir "src/main/java"
       #:tests? #f))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public java-eclipse-jetty-npn-api
  (package
    (name "java-eclipse-jetty-npn-api")
    (version "1.1.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/eclipse/jetty.project")
                     (commit "cc6196af50edf256c6fa3ead21d726073b08a087")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0sn4w6fwbhhzdaizffv04329nd2a7y702a828vvzr6vl8ipxkcv6"))
              (modules '((guix build utils)))
              (snippet
                `(begin
                   (for-each delete-file (find-files "." ".*.jar"))
                   #t))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "jetty-npn.jar"
       #:source-dir "jetty-npn/src/main/java"
       #:tests? #f))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public java-jctools-core
  (package
    (name "java-jctools-core")
    (version "2.1.2")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/JCTools/JCTools/archive/v"
                                  version ".tar.gz"))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "16vqznf6ikzvii72r13h02vcbqfp5kl0fcw2cfhp52fzk3pmpsi2"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "jctools-core.jar"
       #:source-dir "jctools-core/src/main/java"
       #:test-dir "jctools-core/src/test"
       ;; Require com.google.common.collect.testing (guava?)
       #:tests? #f))
    (native-inputs
     `(("java-guava" ,java-guava)
       ("java-hamcrest-core" ,java-hamcrest-core)
       ("java-junit" ,java-junit)))
    (home-page "https://jctools.github.io/JCTools")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public java-log4j-api-for-netty
  (package
    (inherit java-log4j-api)
    (version "2.6.2")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://apache/logging/log4j/" version
                                  "/apache-log4j-" version "-src.tar.gz"))
              (sha256
               (base32
                "1ri58x5l451ngz3p8shn4r7kb69vg0pkr9jb817952s9jbskwws9"))))))

(define-public java-log4j-core-for-netty
  (package
    (inherit java-log4j-api-for-netty)
    (name "java-log4j-core")
    (inputs
     `(("java-osgi-core" ,java-osgi-core)
       ("java-hamcrest-core" ,java-hamcrest-core)
       ("java-log4j-api" ,java-log4j-api-for-netty)
       ("java-mail" ,java-mail)
       ("java-jboss-jms-api-spec" ,java-jboss-jms-api-spec)
       ("java-lmax-disruptor" ,java-lmax-disruptor)
       ("java-kafka" ,java-kafka-clients)
       ("java-datanucleus-javax-persistence" ,java-datanucleus-javax-persistence)
       ("java-fasterxml-jackson-annotations" ,java-fasterxml-jackson-annotations)
       ("java-fasterxml-jackson-core" ,java-fasterxml-jackson-core)
       ("java-fasterxml-jackson-databind" ,java-fasterxml-jackson-databind)
       ("java-fasterxml-jackson-dataformat-xml" ,java-fasterxml-jackson-dataformat-xml)
       ("java-fasterxml-jackson-dataformat-yaml" ,java-fasterxml-jackson-dataformat-yaml)
       ("java-commons-compress" ,java-commons-compress)
       ("java-commons-csv" ,java-commons-csv)
       ("java-stax2-api" ,java-stax2-api)
       ("java-jeromq" ,java-jeromq)))
    (native-inputs
     `(("java-hamcrest-all" ,java-hamcrest-all)
       ("java-commons-io" ,java-commons-io)
       ("java-commons-lang3" ,java-commons-lang3)
       ("java-junit" ,java-junit)
       ("java-slf4j-api" ,java-slf4j-api)))
    (arguments
     `(#:tests? #f ; tests require more dependencies
       #:test-dir "src/test"
       #:source-dir "src/main/java"
       #:jar-name "log4j-core.jar"
       #:make-flags
       (list (string-append "-Ddist.dir=" (assoc-ref %outputs "out")
                            "/share/java"))
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'enter-dir
           (lambda _ (chdir "log4j-core") #t)))))
    (synopsis "Core component of the Log4j framework")
    (description "This package provides the core component of the Log4j
logging framework for Java.")))

(define-public java-netty-tcnative-boringssl
  (package
    (name "java-netty-tcnative-boringssl")
    (version "2.0.20")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/netty/netty-tcnative.git")
                     (commit (string-append "netty-tcnative-parent-"
                                            version ".Final"))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1kdgnk5133bw821iwhli8vj5kf8sb8vxav5wpdsv6vhrgrrm8nrl"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "netty-tcnative-boringssl.jar"
       #:source-dir "openssl-dynamic/src/main/java"
       #:test-dir "openssl-dynamic/src/test"
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'build-native
           (lambda* (#:key inputs system #:allow-other-keys)
             (let ((native-dir (string-append
                                 "build/classes/META-INF/native/linux"
                                 (if (or (string-prefix? "x86_64" system)
                                         (string-prefix? "aarch64" system))
                                   "64" "32")))
                   (apr-include (string-append "-I" (assoc-ref inputs "apr")
                                               "/include/apr-1"))
                   (jni-include (string-append "-I" (assoc-ref inputs "jdk")
                                               "/include/linux")))
               (mkdir-p native-dir)
               (for-each
                 (lambda (source)
                   (invoke "gcc" "-c" source "-o" (string-append source ".o")
                           "-fPIC" "-O2" jni-include apr-include))
                 (find-files "openssl-dynamic/src/main/c" ".*.c$"))
               (apply invoke "gcc" "-o"
                      (string-append native-dir "/libnetty_tcnative.so")
                      "-shared" "-lssl" "-lapr-1" "-lcrypto"
                      (find-files "openssl-dynamic/src/main/c" ".*.o$")))
               #t))
           (add-before 'check 'copy-lib
             (lambda _
               (mkdir-p "build/test-classes")
               (copy-recursively "build/classes/META-INF"
                                 "build/test-classes/META-INF")
               #t)))))
    (inputs
     `(("apr" ,apr)
       ("boringssl" ,boringssl)))
    (native-inputs
     `(("java-hamcrest-core" ,java-hamcrest-core)
       ("java-junit" ,java-junit)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public java-jboss-marshalling
  (package
    (name "java-jboss-marshalling")
    (version "2.0.6")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/jboss-remoting/jboss-marshalling")
                     (commit (string-append version ".Final"))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1afcfk0wgcggyzc4c47kfmxskbpkqvi65vr0g3gl6rikqm010knc"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "jboss-marshalling.jar"
       #:source-dir "api/src/main/java"
       #:test-dir "api/src/test"))
    (inputs
     `(("java-jboss-modules" ,java-jboss-modules)))
    (native-inputs
     `(("java-testng" ,java-testng)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:lgpl2.1+)))

(define-public java-jzlib
  (package
    (name "java-jzlib")
    (version "1.1.3")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/ymnk/jzlib/archive/"
                                  version ".tar.gz"))
              (sha256
               (base32
                "1i0fplk22dlyaz3id0d8hb2wlsl36w9ggbvsq67sx77y0mi6bnl3"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "jzlib.jar"
       #:source-dir "src/main/java"
       #:tests? #f))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:bsd-3)))

(define-public java-lzma
  (package
    (name "java-lzma")
    (version "1.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/jponge/lzma-java.git")
                     (commit (string-append "lzma-java-" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1cmgd95avjq1fsr1bx91cjcvz6ifwgq1iwyzvwvnr6s1w0r8m6hx"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "lzma.jar"
       #:source-dir "src/main/java"
       #:phases
       (modify-phases %standard-phases
         (add-before 'check 'copy-resources
           (lambda _
             (mkdir-p "target/test-classes")
             (copy-recursively "src/test/resources" "target/test-classes")
             #t)))))
    (native-inputs
     `(("java-commons-io" ,java-commons-io)
       ("java-junit" ,java-junit)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public java-ning-compress
  (package
    (name "java-ning-compress")
    (version "1.0.4")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/ning/compress.git")
                     (commit (string-append "compress-lzf-" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1scyws9rs268zvi3p6jvq9yc061zrn49ppp7v91y52si8b1xpbg8"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "ning-compress.jar"
       #:source-dir "src/main/java"
       #:tests? #f; require testng, not junit
       #:phases
       (modify-phases %standard-phases
         (add-before 'check 'fix-tests
           (lambda _
             (substitute* "src/test/lzf/TestLZF.java"
               (("package lzf;") "package lzf;

import com.ning.compress.lzf.*;
import org.junit.Assert.*;"))
             #t)))))
    (native-inputs
     `(("java-junit" ,java-junit)
       ("java-testng" ,java-testng)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public java-protobuf
  (package
    (name "java-protobuf")
    (version (package-version protobuf))
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/google/protobuf/releases/"
                                  "download/v" version "/protobuf-java-"
                                  version ".tar.gz"))
              (sha256
               (base32
                "0fd455ml4bd8p08md6c5ynlxk95s6p4k7kfhciqvilia2fwdpajl"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "protobuf.jar"
       #:source-dir "java/core/src/main/java"
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'build-protos
           (lambda _
             (for-each
               (lambda (proto)
                 (invoke "protoc" "--java_out=java/core/src/main/java" "-Isrc"
                         (string-append "src/google/protobuf/" proto ".proto")))
                 '("any" "api" "descriptor" "duration" "empty" "field_mask"
                   "source_context" "struct" "timestamp" "type" "wrappers"))
             (invoke "protoc" "--java_out=java/core/src/main/java" "-Isrc"
                     "src/google/protobuf/compiler/plugin.proto")
             #t)))))
    (inputs
     `(("java-guava" ,java-guava)))
    (native-inputs
     `(("java-junit" ,java-junit)
       ("protobuf" ,protobuf)))
    (home-page (package-home-page protobuf))
    (synopsis "")
    (description "")
    (license license:bsd-3)))

(define-public java-protobuf-nano
  (package
    (inherit java-protobuf)
    (name "java-protobuf-nano")
    (version "3.5.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/protocolbuffers/protobuf")
                     (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "14gq6rnv03zvcb5hym240z4yqiphrmd5y4zx9a77n37rwvfgx5qy"))))
    (arguments
     `(#:jar-name "protobuf.jar"
       #:source-dir "javanano/src/main/java"
       #:tests? #f))))

(define-public java-jboss-modules
  (package
    (name "java-jboss-modules")
    (version "1.8.6")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/jboss-modules/"
                                  "jboss-modules/archive/" version ".Final.tar.gz"))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "0cm8z4kc1lq4jg3866dzhjaaz5kglsn9bymp4ra7jimyij84xxv2"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "jboss-modules.jar"
       #:source-dir "src/main/java"
       #:tests? #f; require jboss.shrinkwrap
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'copy-resources
           (lambda _
             (copy-recursively "src/main/resources" "build/classes")
             #t)))))
    (native-inputs
     `(("java-junit" ,java-junit)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public java-netty-common
  (package
    (name "java-netty-common")
    (version "4.1.31")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/netty/netty/archive/netty-"
                                  version ".Final.tar.gz"))
              (sha256
               (base32
                "0dchrbwrhsxycs8kgwqcp81xybyx6p13k86d5pjsqni1hrc4sn2i"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "netty-common.jar"
       #:source-dir "common/src/main/java"
       #:test-dir "common/src/test"
       ;; Weird issue with log4j
       #:test-exclude (list "**/Abstract*.*"
                            "**/Log4JLoggerFactoryTest.*")
       #:tests? #f))
    (inputs
     `(("java-commons-logging-minimal" ,java-commons-logging-minimal)
       ("java-jctools-core" ,java-jctools-core)
       ("java-log4j-api" ,java-log4j-api-for-netty)
       ("java-log4j-core" ,java-log4j-core-for-netty)
       ("java-log4j-1.2-api" ,java-log4j-1.2-api)
       ("java-slf4j-api" ,java-slf4j-api)))
    (native-inputs
     `(("java-hamcrest-all" ,java-hamcrest-all)
       ("java-junit" ,java-junit)
       ;("java-mockito" ,java-mockito)
       ("java-slf4j-simple" ,java-slf4j-simple)))
    (home-page "https://netty.io/")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public java-netty-buffer
  (package
    (inherit java-netty-common)
    (name "java-netty-buffer")
    (arguments
     `(#:jar-name "netty-buffer.jar"
       #:source-dir "buffer/src/main/java"
       #:test-dir "buffer/src/test"
       #:test-exclude (list "**/Abstract*.*"
                            "**/ByteBufAllocatorTest.*")
       #:tests? #f))
    (inputs
     `(("java-netty-common" ,java-netty-common)
       ,@(package-inputs java-netty-common)))))

(define-public java-netty-resolver
  (package
    (inherit java-netty-common)
    (name "java-netty-resolver")
    (arguments
     `(#:jar-name "netty-resolver.jar"
       #:source-dir "resolver/src/main/java"
       #:test-dir "resolver/src/test"
       #:tests? #f))
    (inputs
     `(("java-netty-common" ,java-netty-common)
       ,@(package-inputs java-netty-common)))))

(define-public java-netty-transport
  (package
    (inherit java-netty-common)
    (name "java-netty-transport")
    (arguments
     `(#:jar-name "netty-transport.jar"
       #:source-dir "transport/src/main/java"
       #:test-dir "transport/src/test"
       ;; reference to assertEquals is ambiguous
       #:tests? #f))
    (inputs
     `(("java-netty-buffer" ,java-netty-buffer)
       ("java-netty-common" ,java-netty-common)
       ("java-netty-resolver" ,java-netty-resolver)
       ,@(package-inputs java-netty-common)))
    (native-inputs
     `(("java-logback-classic" ,java-logback-classic)
       ("java-logback-core" ,java-logback-core)
       ,@(package-native-inputs java-netty-common)))))

(define-public java-netty-codec
  (package
    (inherit java-netty-common)
    (name "java-netty-codec")
    (arguments
     `(#:jar-name "netty-codec.jar"
       #:source-dir "codec/src/main/java"
       #:test-dir "codec/src/test"
       #:test-exclude
       (list "**/Abstract*.java" "**/Base64Test.java" "**/ZlibTest.java"
             "**/marshalling/*.java")
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'check 'fix-tests
           (lambda _
             (with-directory-excursion "codec/src/test/java/io/netty/handler"
               (substitute* "codec/ByteToMessageCodecTest.java"
                 (("Equals\\(1, ") "Equals((int) 1, (int)")))
             #t)))))
    (inputs
     `(("java-jboss-marshalling" ,java-jboss-marshalling)
       ("java-jzlib" ,java-jzlib)
       ("java-lz4" ,java-lz4)
       ("java-lzma" ,java-lzma)
       ("java-netty-buffer" ,java-netty-buffer)
       ("java-netty-common" ,java-netty-common)
       ("java-netty-transport" ,java-netty-transport)
       ("java-ning-compress" ,java-ning-compress)
       ("java-protobuf" ,java-protobuf)
       ("java-protobuf-nano" ,java-protobuf-nano)
       ,@(package-inputs java-netty-common)))
    (native-inputs
     `(("java-commons-compress" ,java-commons-compress)
       ("java-netty-resolver" ,java-netty-resolver)
       ,@(package-native-inputs java-netty-common)))))

(define-public java-netty-handler
  (package
    (inherit java-netty-common)
    (name "java-netty-handler")
    (arguments
     `(#:jar-name "netty-handler.jar"
       #:source-dir "handler/src/main/java"
       #:test-dir "handler/src/test"
       ;; Reference to assertEquals is ambiguous
       #:tests? #f))
    (inputs
     `(("java-bouncycastle" ,java-bouncycastle)
       ("java-conscrypt" ,java-conscrypt)
       ("java-eclipse-jetty-alpn-api" ,java-eclipse-jetty-alpn-api)
       ("java-eclipse-jetty-npn-api" ,java-eclipse-jetty-npn-api)
       ("java-netty-buffer" ,java-netty-buffer)
       ("java-netty-codec" ,java-netty-codec)
       ("java-netty-common" ,java-netty-common)
       ("java-netty-tcnative-boringssl" ,java-netty-tcnative-boringssl)
       ("java-netty-transport" ,java-netty-transport)
       ,@(package-inputs java-netty-common)))
    (native-inputs
     `(("java-logback-classic" ,java-logback-classic)
       ("java-logback-core" ,java-logback-core)
       ,@(package-native-inputs java-netty-common)))))

;; TODO: animal-sniffer-enforcer-rule and animal-sniffer-maven-plugin depend
;; on maven.
(define-public java-animal-sniffer
  (package
    (name "java-animal-sniffer")
    (version "1.15")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/mojohaus/animal-sniffer/"
                                  "archive/animal-sniffer-parent-"
                                  version ".tar.gz"))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32
                "11k7fhhx6xcpz6iwsxpvr5ivbv1db6xmrrnhl1nzhxl8ja1rsmdc"))))
    (build-system ant-build-system)
    (propagated-inputs
     `(("java-asm" ,java-asm)))
    (arguments
     `(#:tests? #f
       #:jar-name (string-append ,name "-" ,version ".jar")
       #:source-dir "animal-sniffer/src/main/java"))
    (home-page "http://www.mojohaus.org/animal-sniffer")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public java-animal-sniffer-annotations
  (package
    (inherit java-animal-sniffer)
    (name "java-animal-sniffer-annotations")
    (version (package-version java-animal-sniffer))
    (propagated-inputs '())
    (arguments
     `(#:tests? #f
       #:jar-name (string-append ,name "-" ,version ".jar")
       #:source-dir "animal-sniffer-annotations/src/main/java"))))

(define-public java-okio
  (package
    (name "java-okio")
    (version "1.16.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/square/okio/archive/okio-parent-"
                                  version ".tar.gz"))
              (sha256
               (base32
                "05xxrsv2klfvcnp5r4ri28d7hh502wcwlvsm9lnx4kpqlpkq0yzp"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "okio.jar"
       #:source-dir "okio/src/main/java"
       #:test-dir "okio/src/test"))
    (inputs
     `(("java-jsr305" ,java-jsr305)
       ("java-animal-sniffer-annotations" ,java-animal-sniffer-annotations)))
    (native-inputs
     `(("java-hamcrest-core" ,java-hamcrest-core)
       ("java-junit" ,java-junit)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public java-okhttp
  (package
    (name "java-okhttp")
    (version "3.12.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/square/okhttp/archive/parent-"
                                  version ".tar.gz"))
              (sha256
               (base32
                "1gj3rlz2sy6hbdy7yp18f2rb19z0mrp0wai6fls1slsds4fay2gr"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "okhttp.jar"
       #:source-dir "okhttp/src/main/java"
       #:test-dir "okhttp/src/test"
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'remove-platforms
	   (lambda _
	     (delete-file "okhttp/src/main/java/okhttp3/internal/platform/AndroidPlatform.java")
	     (delete-file "okhttp/src/main/java/okhttp3/internal/platform/ConscryptPlatform.java")
	     (substitute* "okhttp/src/main/java/okhttp3/internal/platform/Platform.java"
	       (("AndroidPlatform.buildIfSupported.*") "null;\n")
	       (("ConscryptPlatform.buildIfSupported.*") "null;\n"))
	     #t))
	 (add-after 'unpack 'fill-templates
	   (lambda _
	     (with-directory-excursion "okhttp/src/main/java-templates"
	       (for-each
	         (lambda (file)
		   (let ((installed-name (string-append "../java/" file)))
	             (copy-file file installed-name)
		     (substitute* installed-name
		       (("\\$\\{project.version\\}") ,version))))
	         (find-files "." ".*.java")))
	     #t)))))
    (inputs
     `(("java-animal-sniffer-annotations" ,java-animal-sniffer-annotations)
       ("java-jsr305" ,java-jsr305)
       ("java-okio" ,java-okio)))
    (native-inputs
     `(("java-junit" ,java-junit)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

(define-public java-javaparser-3.9
  (package
    (name "java-javaparser")
    (version "3.9.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/javaparser/javaparser.git")
                     (commit "b7907a0dc39ff10388943dfffba01bec4bb116dc")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "172y8wbwqwwvqlhk65df0zilyicq7nagnsa9f4gav8s85p9ijxr8"))
              (modules '((guix build utils)))
              (snippet
               '(begin
                  (for-each delete-file
                            (find-files "." "\\.jar$"))
                  #t))))
    (build-system ant-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'build 'fill-template
           (lambda _
             (with-directory-excursion "javaparser-core/src/main"
               (copy-file "java-templates/com/github/javaparser/JavaParserBuild.java"
                          "java/com/github/javaparser/JavaParserBuild.java")
               (substitute* "java/com/github/javaparser/JavaParserBuild.java"
                 (("\\$\\{project.version\\}") ,version)
                 (("\\$\\{project.name\\}") "javaparser")
                 (("\\$\\{project.build.finalName\\}") "javaparser")
                 (("\\$\\{maven.version\\}") "fake")
                 (("\\$\\{maven.build.version\\}") "fake")
                 (("\\$\\{build.timestamp\\}") "0")
                 (("\\$\\{java.vendor\\}") "Guix")
                 (("\\$\\{java.vendor.url\\}") "https://gnu.org/software/guix")
                 (("\\$\\{java.version\\}") "1.8")
                 (("\\$\\{os.arch\\}") "any")
                 (("\\$\\{os.name\\}") "GuixSD")
                 (("\\$\\{os.version\\}") "not available")))
             #t))
         (add-before 'build 'generate-javacc
           (lambda _
             (with-directory-excursion "javaparser-core/src/main/java"
               (invoke "java" "javacc" "../javacc/java.jj"))
             #t))
         (add-before 'build 'copy-javacc-support
           (lambda _
             (with-directory-excursion "javaparser-core/src/main"
               (copy-recursively "javacc-support" "java"))
             #t))
         (add-before 'build 'fix-ambiguousity
           (lambda _
             (substitute* (find-files "javaparser-symbol-solver-core/src/main/java" ".*.java")
               (("TypeParameter node")
                "com.github.javaparser.ast.type.TypeParameter node"))
             (substitute* (find-files "javaparser-core-metamodel-generator/src/main/java" ".*.java")
               (("\\(TypeParameter.class")
                "(com.github.javaparser.ast.type.TypeParameter.class"))
             (substitute* (find-files "javaparser-core/src/main/java" ".*.java")
               (("final TypeParameter ")
                "final com.github.javaparser.ast.type.TypeParameter ")
               (("\\(TypeParameter.class")
                "(com.github.javaparser.ast.type.TypeParameter.class")
               (("new TypeParameter\\(")
                "new com.github.javaparser.ast.type.TypeParameter(")
               (("\\(TypeParameter\\)")
                "(com.github.javaparser.ast.type.TypeParameter)")
               (("TypeParameter r")
                "com.github.javaparser.ast.type.TypeParameter r")
               (("TypeParameter tp")
                "com.github.javaparser.ast.type.TypeParameter tp")
               (("\\{TypeParameter")
                "{com.github.javaparser.ast.type.TypeParameter")
               (("public TypeParameter ")
                "public com.github.javaparser.ast.type.TypeParameter ")
               (("visit\\(TypeParameter ")
                "visit(com.github.javaparser.ast.type.TypeParameter ")
               (("<TypeParameter>")
                "<com.github.javaparser.ast.type.TypeParameter>"))
             #t))
         (replace 'build
           (lambda _
             (define (build name)
               (format #t "Building ~a~%" name)
               (delete-file-recursively "build/classes")
               (mkdir-p "build/classes")
               (apply invoke "javac"
                      "-cp" (string-append (getenv "CLASSPATH") ":"
                                           (string-join (find-files "build/jar" ".") ":"))
                      "-d" "build/classes"
                      (find-files (string-append name "/src/main/java")
                                  ".*.java"))
               (invoke "jar" "-cf" (string-append "build/jar/" name ".jar")
                       "-C" "build/classes" "."))
             (mkdir-p "build/classes")
             (mkdir-p "build/test-classes")
             (mkdir-p "build/jar")
             (build "javaparser-core")
             (build "javaparser-core-serialization")
             (build "javaparser-core-generators")
             (build "javaparser-core-metamodel-generator")
             (build "javaparser-symbol-solver-model")
             (build "javaparser-symbol-solver-logic")
             (build "javaparser-symbol-solver-core")
             #t))
         (replace 'check
           (lambda _
             (define (test name)
               (format #t "Testing ~a~%" name)
               (delete-file-recursively "build/test-classes")
               (mkdir-p "build/test-classes")
               (apply invoke "javac" "-d" "build/test-classes"
                      "-cp" (string-append (getenv "CLASSPATH") ":"
                                           (string-join (find-files "build/jar" ".") ":"))
                      (find-files (string-append name "/src/test/java")
                                  ".*.java"))
               (invoke "java" "-cp" (string-append (getenv "CLASSPATH") ":"
                                                   (string-join (find-files "build/jar" ".") ":")
                                                   ":build/test-classes")
                       "org.junit.runner.JUnitCore"
                       (map
                         (lambda (file)
                           (string-join
                             (string-split
                               (substring file 14 (- (string-length file) 5)) #\/)
                             "."))
                         (find-files (string-append name "/src/test/java")
                                     ".*Test.java"))))
             ;; We need junit5 that recursively depend on this package,
             ;; as well as jbehave.
             ;(test "javaparser-core-testing")
             ;(test "javaparser-symbol-solver-testing")
             #t))
         (replace 'install
           (install-jars "build/jar")))))
    (inputs
     `(("java-guava" ,java-guava)
       ("java-jboss-javassist" ,java-jboss-javassist)
       ("java-jsonp-api" ,java-jsonp-api)))
    (native-inputs
     `(("javacc" ,javacc)
       ("java-hamcrest-core" ,java-hamcrest-core)
       ("java-junit" ,java-junit)
       ("java-okhttp" ,java-okhttp)))
    (home-page "http://javaparser.org/")
    (synopsis "Parser for Java")
    (description
     "This project contains a set of libraries implementing a Java 1.0 - Java
11 Parser with advanced analysis functionalities.")
    (license license:lgpl2.0+)))

(define-public java-proxy-vole
  (package
    (name "java-proxy-vole")
    (version "20131209")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/brsanthu/proxy-vole")
                     ;; The commit on which README is updated
                     (commit "e0ec99be4a9b59aab21631c3712a3d32d8f82123")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "04lwb056pm5mc872ng317lpqa77da6np8fyjaq2wh9r16vgr33pi"))
              (modules '((guix build utils)))
              (snippet
               `(begin
                  (delete-file-recursively "proxy_vole/lib")
                  (for-each delete-file (find-files "." "\\.jar$"))))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "proxy-vole.jar"
       #:source-dir "proxy_vole/src"
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (replace 'install (install-jars "build")))))
    (inputs
     (list rhino))
    (home-page "https://github.com/brsanthu/proxy-vole")
    (synopsis "")
    (description "")
    (license license:bsd-3)))

(define-public java-imgscalr
  (package
    (name "java-imgscalr")
    (version "4.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/rkalla/imgscalr")
                     (commit (string-append version "-release"))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1vbfx2wa9lavagmg2pgy5jq4m7msp1dkc4pwr7vv5a746fx24pg9"))))
    (build-system ant-build-system)
    (arguments
     `(#:tests? #f; no test target
       #:phases
       (modify-phases %standard-phases
         (replace 'install
           (install-jars ".")))))
    (home-page "https://github.com/rkalla/imgscalr")
    (synopsis "")
    (description "")
    (license license:asl2.0)))

;; XXX: Missing the JNI library
(define-public java-native-platform
  (package
    (name "java-native-platform")
    (version "0.22-milestone-24")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/gradle/native-platform")
                     (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "11k0286irly4jbx7qxjpm9hw15r7zp6smcdhq602zn1s305j8z65"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "native-platform.jar"
       #:source-dir "native-platform/src/main/java"
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'generate-version
           (lambda _
             (with-output-to-file "native-platform/src/main/java/net/rubygrapefruit/platform/internal/jni/NativeVersion.java"
               (lambda _
                 (format #t "package net.rubygrapefruit.platform.internal.jni;
public interface NativeVersion {
  String VERSION = \"~a\";
}" ,version))))))))
    (inputs (list java-jsr305))
    (home-page "https://github.com/rkalla/imgscalr")
    (synopsis "")
    (description "")
    (license license:asl2.0)))
