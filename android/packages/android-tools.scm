;;; GNU Guix --- Functional package management for GNU
;;; Automatically imported by (android import repo).
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
;;; Copyright © 2021,2022 Julien Lepiller <julien@lepiller.eu>

(define-module (android packages android-tools)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (android build-system soong)
  #:use-module (android packages android-headers)
  #:use-module (android packages android-sources)
  #:use-module (android packages bison)
  #:use-module (android packages clang)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages vim)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix utils))

(define-public aapt
  (package
    (name "aapt")
    (version %latest-android-13)
    (source
      android-platform-frameworks-base-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_binary_host"
        #:module
        "aapt"
        #:blueprint
        "tools/aapt/Android.bp"
        #:base-package
        "//frameworks/base/tools/aapt"))
    (inputs
      `(("libandroidfw" ,libandroidfw)
        ("libpng" ,libpng)
        ("libutils" ,libutils)
        ("liblog" ,liblog)
        ("libcutils" ,libcutils)
        ("libexpat" ,libexpat)
        ("libziparchive" ,libziparchive)
        ("libbase" ,libbase)
        ("libz" ,libz)
        ("libaapt" ,libaapt)
        ("libbuildversion" ,libbuildversion)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public aapt2
  (package
    (name "aapt2")
    (version %latest-android-13)
    (source
      android-platform-frameworks-base-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_binary_host"
        #:module
        "aapt2"
        #:blueprint
        "tools/aapt2/Android.bp"
        #:base-package
        "//frameworks/base/tools/aapt2"))
    (inputs
      `(("jni-headers" ,jni-headers)
        ("libandroidfw" ,libandroidfw)
        ("libutils" ,libutils)
        ("liblog" ,liblog)
        ("libcutils" ,libcutils)
        ("libexpat" ,libexpat)
        ("libziparchive" ,libziparchive)
        ("libpng" ,libpng)
        ("libbase" ,libbase)
        ("libprotobuf-cpp-full" ,libprotobuf-cpp-full)
        ("libz" ,libz)
        ("libbuildversion" ,libbuildversion)
        ("libidmap2-policies" ,libidmap2-policies)
        ("libaapt2" ,libaapt2)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public adb
  (package
    (name "adb")
    (version %latest-android-13)
    (source
      android-platform-packages-modules-adb-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_binary_host"
        #:module
        "adb"
        #:blueprint
        "Android.bp"
        #:base-package
        "//packages/modules/adb"))
    (inputs
      `(("libadb-crypto" ,libadb-crypto)
        ("libadb-host" ,libadb-host)
        ("libadb-pairing-auth" ,libadb-pairing-auth)
        ("libadb-pairing-connection"
         ,libadb-pairing-connection)
        ("libadb-protos" ,libadb-protos)
        ("libadb-sysdeps" ,libadb-sysdeps)
        ("libadb-tls-connection" ,libadb-tls-connection)
        ("libandroidfw" ,libandroidfw)
        ("libapp-processes-protos-full"
         ,libapp-processes-protos-full)
        ("libbase" ,libbase)
        ("libbrotli" ,libbrotli)
        ("libcrypto" ,libcrypto)
        ("libcrypto-utils" ,libcrypto-utils)
        ("libcutils" ,libcutils)
        ("libdiagnose-usb" ,libdiagnose-usb)
        ("libfastdeploy-host" ,libfastdeploy-host)
        ("liblog" ,liblog)
        ("liblz4" ,liblz4)
        ("libmdnssd" ,libmdnssd)
        ("libopenscreen-discovery"
         ,libopenscreen-discovery)
        ("libopenscreen-platform-impl"
         ,libopenscreen-platform-impl)
        ("libprotobuf-cpp-full" ,libprotobuf-cpp-full)
        ("libssl" ,libssl)
        ("libusb" ,libusb)
        ("libutils" ,libutils)
        ("libz" ,libz)
        ("libziparchive" ,libziparchive)
        ("libzstd" ,libzstd)
        ("bin2c-fastdeployagent" ,bin2c-fastdeployagent)
        ("bin2c-fastdeployagentscript"
         ,bin2c-fastdeployagentscript)
        ("libbuildversion" ,libbuildversion)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public aidl
  (package
    (name "aidl")
    (version %latest-android-13)
    (source
      android-platform-system-tools-aidl-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_binary_host"
        #:module
        "aidl"
        #:blueprint
        "Android.bp"
        #:base-package
        "//system/tools/aidl"))
    (inputs
      `(("libgtest-prod-headers" ,libgtest-prod-headers)
        ("libbase" ,libbase)
        ("libcutils" ,libcutils)
        ("libgtest" ,libgtest)
        ("libaidl-common" ,libaidl-common)
        ("liblog" ,liblog)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public apksig
  (package
    (name "apksig")
    (version %latest-android-13)
    (source android-platform-tools-apksig-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "java_library_host"
        #:module
        "apksig"
        #:blueprint
        "Android.bp"
        #:base-package
        "//tools/apksig"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public apksigner
  (package
    (name "apksigner")
    (version %latest-android-13)
    (source android-platform-tools-apksig-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "java_binary_host"
        #:module
        "apksigner"
        #:blueprint
        "Android.bp"
        #:base-package
        "//tools/apksig"))
    (inputs
      `(("apksig" ,apksig)
        ("conscrypt-unbundled" ,conscrypt-unbundled)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public aprotoc
  (package
    (name "aprotoc")
    (version %latest-android-13)
    (source
      android-platform-external-protobuf-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_binary_host"
        #:module
        "aprotoc"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/protobuf"))
    (inputs
      `(("libprotoc" ,libprotoc) ("libz" ,libz)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public avb-headers
  (package
    (name "avb-headers")
    (version %latest-android-13)
    (source android-platform-external-avb-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_headers"
        #:module
        "avb_headers"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/avb"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public bcm-object
  (package
    (name "bcm-object")
    (version %latest-android-13)
    (source
      android-platform-external-boringssl-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_object"
        #:module
        "bcm_object"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/boringssl"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public bin2c-fastdeployagent
  (package
    (name "bin2c-fastdeployagent")
    (version %latest-android-13)
    (source
      android-platform-packages-modules-adb-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "java_genrule"
        #:module
        "bin2c_fastdeployagent"
        #:blueprint
        "Android.bp"
        #:base-package
        "//packages/modules/adb"))
    (inputs `(("deployagent" ,deployagent)))
    (native-inputs `(("xxd" ,xxd)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public bin2c-fastdeployagentscript
  (package
    (name "bin2c-fastdeployagentscript")
    (version %latest-android-13)
    (source
      android-platform-packages-modules-adb-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "genrule"
        #:module
        "bin2c_fastdeployagentscript"
        #:blueprint
        "Android.bp"
        #:base-package
        "//packages/modules/adb"))
    (native-inputs `(("xxd" ,xxd)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public bootimg-headers
  (package
    (name "bootimg-headers")
    (version %latest-android-13)
    (source
      android-platform-system-tools-mkbootimg-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_headers"
        #:module
        "bootimg_headers"
        #:blueprint
        "Android.bp"
        #:base-package
        "//system/tools/mkbootimg"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public conscrypt-generate-constants
  (package
    (name "conscrypt-generate-constants")
    (version %latest-android-13)
    (source
      android-platform-external-conscrypt-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_binary_host"
        #:module
        "conscrypt_generate_constants"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/conscrypt"))
    (inputs
      `(("libcrypto" ,libcrypto) ("libssl" ,libssl)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public conscrypt-unbundled
  (package
    (name "conscrypt-unbundled")
    (version %latest-android-13)
    (source
      android-platform-external-conscrypt-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "java_library_host"
        #:module
        "conscrypt-unbundled"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/conscrypt"))
    (inputs
      `(("conscrypt-unbundled-generated-constants"
         ,conscrypt-unbundled-generated-constants)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public conscrypt-unbundled-generated-constants
  (package
    (name "conscrypt-unbundled-generated-constants")
    (version %latest-android-13)
    (source
      android-platform-external-conscrypt-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "genrule"
        #:module
        "conscrypt-unbundled_generated_constants"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/conscrypt"))
    (inputs
      `(("conscrypt-generate-constants"
         ,conscrypt-generate-constants)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public deployagent
  (package
    (name "deployagent")
    (version %latest-android-13)
    (source
      android-platform-packages-modules-adb-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "java_binary"
        #:module
        "deployagent"
        #:blueprint
        "fastdeploy/Android.bp"
        #:base-package
        "//packages/modules/adb/fastdeploy"))
    (inputs `(("deployagent-lib" ,deployagent-lib)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public deployagent-lib
  (package
    (name "deployagent-lib")
    (version %latest-android-13)
    (source
      android-platform-packages-modules-adb-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "java_library"
        #:module
        "deployagent_lib"
        #:blueprint
        "fastdeploy/Android.bp"
        #:base-package
        "//packages/modules/adb/fastdeploy"))
    (inputs
      `(("aprotoc" ,aprotoc)
        ("libprotobuf-java-lite" ,libprotobuf-java-lite)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public dexdump
  (package
    (name "dexdump")
    (version %latest-android-13)
    (source android-platform-art-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "art_cc_binary"
        #:module
        "dexdump"
        #:blueprint
        "dexdump/Android.bp"
        #:base-package
        "//art/dexdump"))
    (inputs
      `(("libdexfile" ,libdexfile)
        ("libartbase" ,libartbase)
        ("libbase" ,libbase)
        ("libartpalette" ,libartpalette)
        ("liblog" ,liblog)
        ("libz" ,libz)
        ("libziparchive" ,libziparchive)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public dmtracedump
  (package
    (name "dmtracedump")
    (version %latest-android-13)
    (source android-platform-art-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "art_cc_binary"
        #:module
        "dmtracedump"
        #:blueprint
        "tools/dmtracedump/Android.bp"
        #:base-package
        "//art/tools/dmtracedump"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public dx
  (package
    (name "dx")
    (version %latest-android-13)
    (source android-platform-dalvik-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "java_binary_host"
        #:module
        "dx"
        #:blueprint
        "dx/Android.bp"
        #:base-package
        "//dalvik/dx"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public etc1tool
  (package
    (name "etc1tool")
    (version %latest-android-13)
    (source android-platform-development-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_binary_host"
        #:module
        "etc1tool"
        #:blueprint
        "tools/etc1tool/Android.bp"
        #:base-package
        "//development/tools/etc1tool"))
    (inputs
      `(("libexpat" ,libexpat)
        ("libpng" ,libpng)
        ("libETC1" ,libETC1)
        ("libz" ,libz)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public fastboot
  (package
    (name "fastboot")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_binary_host"
        #:module
        "fastboot"
        #:blueprint
        "fastboot/Android.bp"
        #:base-package
        "//system/core/fastboot"))
    (inputs
      `(("avb-headers" ,avb-headers)
        ("bootimg-headers" ,bootimg-headers)
        ("libext4-utils" ,libext4-utils)
        ("libziparchive" ,libziparchive)
        ("libsparse" ,libsparse)
        ("libutils" ,libutils)
        ("liblog" ,liblog)
        ("libz" ,libz)
        ("libdiagnose-usb" ,libdiagnose-usb)
        ("libbase" ,libbase)
        ("libcutils" ,libcutils)
        ("libgtest-host" ,libgtest-host)
        ("liblp" ,liblp)
        ("libcrypto" ,libcrypto)
        ("libfastboot" ,libfastboot)
        ("libbuildversion" ,libbuildversion)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public fmtlib
  (package
    (name "fmtlib")
    (version %latest-android-13)
    (source
      android-platform-external-fmtlib-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_static"
        #:module
        "fmtlib"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/fmtlib"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public fmtlib-headers
  (package
    (name "fmtlib-headers")
    (version %latest-android-13)
    (source
      android-platform-external-fmtlib-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_headers"
        #:module
        "fmtlib_headers"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/fmtlib"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public hprof-conv
  (package
    (name "hprof-conv")
    (version %latest-android-13)
    (source android-platform-dalvik-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_binary_host"
        #:module
        "hprof-conv"
        #:blueprint
        "tools/hprof-conv/Android.bp"
        #:base-package
        "//dalvik/tools/hprof-conv"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public img2simg
  (package
    (name "img2simg")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_binary"
        #:module
        "img2simg"
        #:blueprint
        "libsparse/Android.bp"
        #:base-package
        "//system/core/libsparse"))
    (inputs
      `(("libsparse" ,libsparse)
        ("libz" ,libz)
        ("libbase" ,libbase)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public jni-headers
  (package
    (name "jni-headers")
    (version %latest-android-13)
    (source
      android-platform-libnativehelper-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_headers"
        #:module
        "jni_headers"
        #:blueprint
        "Android.bp"
        #:base-package
        "//libnativehelper"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libETC1
  (package
    (name "libETC1")
    (version %latest-android-13)
    (source
      android-platform-frameworks-native-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libETC1"
        #:blueprint
        "opengl/libs/Android.bp"
        #:base-package
        "//frameworks/native/opengl/libs"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libaapt
  (package
    (name "libaapt")
    (version %latest-android-13)
    (source
      android-platform-frameworks-base-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_host_static"
        #:module
        "libaapt"
        #:blueprint
        "tools/aapt/Android.bp"
        #:base-package
        "//frameworks/base/tools/aapt"))
    (inputs
      `(("libandroidfw" ,libandroidfw)
        ("libpng" ,libpng)
        ("libutils" ,libutils)
        ("liblog" ,liblog)
        ("libcutils" ,libcutils)
        ("libexpat" ,libexpat)
        ("libziparchive" ,libziparchive)
        ("libbase" ,libbase)
        ("libz" ,libz)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libaapt2
  (package
    (name "libaapt2")
    (version %latest-android-13)
    (source
      android-platform-frameworks-base-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_host_static"
        #:module
        "libaapt2"
        #:blueprint
        "tools/aapt2/Android.bp"
        #:base-package
        "//frameworks/base/tools/aapt2"))
    (inputs
      `(("jni-headers" ,jni-headers)
        ("libprotobuf-cpp-full" ,libprotobuf-cpp-full)
        ("libandroidfw" ,libandroidfw)
        ("libutils" ,libutils)
        ("liblog" ,liblog)
        ("libcutils" ,libcutils)
        ("libexpat" ,libexpat)
        ("libziparchive" ,libziparchive)
        ("libpng" ,libpng)
        ("libbase" ,libbase)
        ("libz" ,libz)
        ("libbuildversion" ,libbuildversion)
        ("libidmap2-policies" ,libidmap2-policies)
        ("aprotoc" ,aprotoc)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libadb-crypto
  (package
    (name "libadb-crypto")
    (version %latest-android-13)
    (source
      android-platform-packages-modules-adb-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libadb_crypto"
        #:blueprint
        "crypto/Android.bp"
        #:base-package
        "//packages/modules/adb/crypto"))
    (inputs
      `(("libadb-protos" ,libadb-protos)
        ("libadb-sysdeps" ,libadb-sysdeps)
        ("libbase" ,libbase)
        ("liblog" ,liblog)
        ("libcrypto" ,libcrypto)
        ("libcrypto-utils" ,libcrypto-utils)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libadb-host
  (package
    (name "libadb-host")
    (version %latest-android-13)
    (source
      android-platform-packages-modules-adb-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_host_static"
        #:module
        "libadb_host"
        #:blueprint
        "Android.bp"
        #:base-package
        "//packages/modules/adb"))
    (inputs
      `(("libadb-crypto" ,libadb-crypto)
        ("libadb-pairing-connection"
         ,libadb-pairing-connection)
        ("libadb-protos" ,libadb-protos)
        ("libadb-tls-connection" ,libadb-tls-connection)
        ("libbase" ,libbase)
        ("libcrypto" ,libcrypto)
        ("libcrypto-utils" ,libcrypto-utils)
        ("libcutils" ,libcutils)
        ("libdiagnose-usb" ,libdiagnose-usb)
        ("liblog" ,liblog)
        ("libmdnssd" ,libmdnssd)
        ("libopenscreen-discovery"
         ,libopenscreen-discovery)
        ("libopenscreen-platform-impl"
         ,libopenscreen-platform-impl)
        ("libprotobuf-cpp-lite" ,libprotobuf-cpp-lite)
        ("libusb" ,libusb)
        ("libutils" ,libutils)
        ("platform-tools-version"
         ,platform-tools-version)
        ("libbuildversion" ,libbuildversion)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libadb-pairing-auth
  (package
    (name "libadb-pairing-auth")
    (version %latest-android-13)
    (source
      android-platform-packages-modules-adb-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libadb_pairing_auth"
        #:blueprint
        "pairing_auth/Android.bp"
        #:base-package
        "//packages/modules/adb/pairing_auth"))
    (inputs
      `(("libcrypto" ,libcrypto)
        ("liblog" ,liblog)
        ("libbase" ,libbase)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libadb-pairing-connection
  (package
    (name "libadb-pairing-connection")
    (version %latest-android-13)
    (source
      android-platform-packages-modules-adb-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libadb_pairing_connection"
        #:blueprint
        "pairing_connection/Android.bp"
        #:base-package
        "//packages/modules/adb/pairing_connection"))
    (inputs
      `(("libcrypto" ,libcrypto)
        ("liblog" ,liblog)
        ("libadb-pairing-auth" ,libadb-pairing-auth)
        ("libbase" ,libbase)
        ("libssl" ,libssl)
        ("libadb-protos" ,libadb-protos)
        ("libadb-tls-connection" ,libadb-tls-connection)
        ("libprotobuf-cpp-lite" ,libprotobuf-cpp-lite)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libadb-protos
  (package
    (name "libadb-protos")
    (version %latest-android-13)
    (source
      android-platform-packages-modules-adb-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libadb_protos"
        #:blueprint
        "proto/Android.bp"
        #:base-package
        "//packages/modules/adb/proto"))
    (inputs
      `(("aprotoc" ,aprotoc)
        ("libprotobuf-cpp-lite" ,libprotobuf-cpp-lite)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libadb-sysdeps
  (package
    (name "libadb-sysdeps")
    (version %latest-android-13)
    (source
      android-platform-packages-modules-adb-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libadb_sysdeps"
        #:blueprint
        "Android.bp"
        #:base-package
        "//packages/modules/adb"))
    (inputs
      `(("libbase" ,libbase) ("liblog" ,liblog)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libadb-tls-connection
  (package
    (name "libadb-tls-connection")
    (version %latest-android-13)
    (source
      android-platform-packages-modules-adb-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libadb_tls_connection"
        #:blueprint
        "tls/Android.bp"
        #:base-package
        "//packages/modules/adb/tls"))
    (inputs
      `(("libbase" ,libbase)
        ("libcrypto" ,libcrypto)
        ("liblog" ,liblog)
        ("libssl" ,libssl)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libaidl-common
  (package
    (name "libaidl-common")
    (version %latest-android-13)
    (source
      android-platform-system-tools-aidl-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_static"
        #:module
        "libaidl-common"
        #:blueprint
        "Android.bp"
        #:base-package
        "//system/tools/aidl"))
    (inputs
      `(("libgtest-prod-headers" ,libgtest-prod-headers)
        ("libbase" ,libbase)
        ("libcutils" ,libcutils)
        ("libgtest" ,libgtest)))
    (native-inputs
     `(("bison" ,bison-3.5)
       ("flex" ,flex)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libandroidfw
  (package
    (name "libandroidfw")
    (version %latest-android-13)
    (source
      android-platform-frameworks-base-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libandroidfw"
        #:blueprint
        "libs/androidfw/Android.bp"
        #:base-package
        "//frameworks/base/libs/androidfw"))
    (inputs
      `(("libz" ,libz)
        ("libbase" ,libbase)
        ("libcutils" ,libcutils)
        ("liblog" ,liblog)
        ("libutils" ,libutils)
        ("libziparchive" ,libziparchive)
        ("libincfs-utils" ,libincfs-utils)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libapp-processes-protos-full
  (package
    (name "libapp-processes-protos-full")
    (version %latest-android-13)
    (source
      android-platform-packages-modules-adb-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_host_static"
        #:module
        "libapp_processes_protos_full"
        #:blueprint
        "proto/Android.bp"
        #:base-package
        "//packages/modules/adb/proto"))
    (inputs
      `(("aprotoc" ,aprotoc)
        ("libprotobuf-cpp-full" ,libprotobuf-cpp-full)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libartbase
  (package
    (name "libartbase")
    (version %latest-android-13)
    (source android-platform-art-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "art_cc_library"
        #:module
        "libartbase"
        #:blueprint
        "libartbase/Android.bp"
        #:base-package
        "//art/libartbase"))
    (inputs
      `(("libziparchive" ,libziparchive)
        ("libz" ,libz)
        ("liblog" ,liblog)
        ("libartpalette" ,libartpalette)
        ("libbase" ,libbase)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libartpalette
  (package
    (name "libartpalette")
    (version %latest-android-13)
    (source android-platform-art-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "art_cc_library"
        #:module
        "libartpalette"
        #:blueprint
        "libartpalette/Android.bp"
        #:base-package
        "//art/libartpalette"))
    (inputs
      `(("libbase-headers" ,libbase-headers)
        ("jni-headers" ,jni-headers)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libbacktrace-headers
  (package
    (name "libbacktrace-headers")
    (version %latest-android-13)
    (source
      android-platform-system-unwinding-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_headers"
        #:module
        "libbacktrace_headers"
        #:blueprint
        "libbacktrace/Android.bp"
        #:base-package
        "//system/unwinding/libbacktrace"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libbase
  (package
    (name "libbase")
    (version %latest-android-13)
    (source
      android-platform-system-libbase-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libbase"
        #:blueprint
        "Android.bp"
        #:base-package
        "//system/libbase"))
    (inputs
      `(("libbase-headers" ,libbase-headers)
        ("liblog" ,liblog)
        ("fmtlib" ,fmtlib)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libbase-headers
  (package
    (name "libbase-headers")
    (version %latest-android-13)
    (source
      android-platform-system-libbase-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_headers"
        #:module
        "libbase_headers"
        #:blueprint
        "Android.bp"
        #:base-package
        "//system/libbase"))
    (inputs `(("fmtlib-headers" ,fmtlib-headers)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libbrotli
  (package
    (name "libbrotli")
    (version %latest-android-13)
    (source
      android-platform-external-brotli-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_static"
        #:module
        "libbrotli"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/brotli"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libbuildversion
  (package
    (name "libbuildversion")
    (version %latest-android-13)
    (source android-platform-build-soong-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_static"
        #:module
        "libbuildversion"
        #:blueprint
        "cc/libbuildversion/Android.bp"
        #:base-package
        "//build/soong/cc/libbuildversion"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libcrypto
  (package
    (name "libcrypto")
    (version %latest-android-13)
    (source
      android-platform-external-boringssl-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libcrypto"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/boringssl"))
    (inputs `(("bcm-object" ,bcm-object)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libcrypto-utils
  (package
    (name "libcrypto-utils")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libcrypto_utils"
        #:blueprint
        "libcrypto_utils/Android.bp"
        #:base-package
        "//system/core/libcrypto_utils"))
    (inputs `(("libcrypto" ,libcrypto)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libcutils
  (package
    (name "libcutils")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libcutils"
        #:blueprint
        "libcutils/Android.bp"
        #:base-package
        "//system/core/libcutils"))
    (inputs
      `(("libbase-headers" ,libbase-headers)
        ("libcutils-headers" ,libcutils-headers)
        ("libprocessgroup-headers"
         ,libprocessgroup-headers)
        ("liblog" ,liblog)
        ("libbase" ,libbase)
        ("libcutils-sockets" ,libcutils-sockets)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libcutils-headers
  (package
    (name "libcutils-headers")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_headers"
        #:module
        "libcutils_headers"
        #:blueprint
        "libcutils/Android.bp"
        #:base-package
        "//system/core/libcutils"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libcutils-sockets
  (package
    (name "libcutils-sockets")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libcutils_sockets"
        #:blueprint
        "libcutils/Android.bp"
        #:base-package
        "//system/core/libcutils"))
    (inputs `(("liblog" ,liblog)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libdexfile
  (package
    (name "libdexfile")
    (version %latest-android-13)
    (source android-platform-art-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "art_cc_library"
        #:module
        "libdexfile"
        #:blueprint
        "libdexfile/Android.bp"
        #:base-package
        "//art/libdexfile"))
    (inputs
      `(("jni-headers" ,jni-headers)
        ("libdexfile-external-headers"
         ,libdexfile-external-headers)
        ("libziparchive" ,libziparchive)
        ("libz" ,libz)
        ("libartpalette" ,libartpalette)
        ("liblog" ,liblog)
        ("libbase" ,libbase)
        ("libartbase" ,libartbase)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libdexfile-external-headers
  (package
    (name "libdexfile-external-headers")
    (version %latest-android-13)
    (source android-platform-art-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_headers"
        #:module
        "libdexfile_external_headers"
        #:blueprint
        "libdexfile/Android.bp"
        #:base-package
        "//art/libdexfile"))
    (inputs `(("libbase-headers" ,libbase-headers)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libdiagnose-usb
  (package
    (name "libdiagnose-usb")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_static"
        #:module
        "libdiagnose_usb"
        #:blueprint
        "diagnose_usb/Android.bp"
        #:base-package
        "//system/core/diagnose_usb"))
    (inputs `(("libbase" ,libbase)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libexpat
  (package
    (name "libexpat")
    (version %latest-android-13)
    (source
      android-platform-external-expat-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libexpat"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/expat"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libext4-utils
  (package
    (name "libext4-utils")
    (version %latest-android-13)
    (source android-platform-system-extras-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libext4_utils"
        #:blueprint
        "ext4_utils/Android.bp"
        #:base-package
        "//system/extras/ext4_utils"))
    (inputs `(("libbase" ,libbase) ("libz" ,libz)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libfastboot
  (package
    (name "libfastboot")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_host_static"
        #:module
        "libfastboot"
        #:blueprint
        "fastboot/Android.bp"
        #:base-package
        "//system/core/fastboot"))
    (inputs
      `(("avb-headers" ,avb-headers)
        ("bootimg-headers" ,bootimg-headers)
        ("libstorage-literals-headers"
         ,libstorage-literals-headers)
        ("libext4-utils" ,libext4-utils)
        ("libziparchive" ,libziparchive)
        ("libsparse" ,libsparse)
        ("libutils" ,libutils)
        ("liblog" ,liblog)
        ("libz" ,libz)
        ("libdiagnose-usb" ,libdiagnose-usb)
        ("libbase" ,libbase)
        ("libcutils" ,libcutils)
        ("libgtest-host" ,libgtest-host)
        ("liblp" ,liblp)
        ("libcrypto" ,libcrypto)
        ("libbuildversion" ,libbuildversion)
        ("platform-tools-version"
         ,platform-tools-version)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libfastdeploy-host
  (package
    (name "libfastdeploy-host")
    (version %latest-android-13)
    (source
      android-platform-packages-modules-adb-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_host_static"
        #:module
        "libfastdeploy_host"
        #:blueprint
        "Android.bp"
        #:base-package
        "//packages/modules/adb"))
    (inputs
      `(("libprotobuf-cpp-lite" ,libprotobuf-cpp-lite)
        ("libadb-host" ,libadb-host)
        ("libandroidfw" ,libandroidfw)
        ("libbase" ,libbase)
        ("libcrypto" ,libcrypto)
        ("libcrypto-utils" ,libcrypto-utils)
        ("libcutils" ,libcutils)
        ("libdiagnose-usb" ,libdiagnose-usb)
        ("liblog" ,liblog)
        ("libmdnssd" ,libmdnssd)
        ("libusb" ,libusb)
        ("libutils" ,libutils)
        ("libz" ,libz)
        ("libziparchive" ,libziparchive)
        ("libbuildversion" ,libbuildversion)
        ("aprotoc" ,aprotoc)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libgtest
  (package
    (name "libgtest")
    (version %latest-android-13)
    (source
      android-platform-external-googletest-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_static"
        #:module
        "libgtest"
        #:blueprint
        "googletest/Android.bp"
        #:base-package
        "//external/googletest/googletest"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libgtest-host
  (package
    (name "libgtest-host")
    (version %latest-android-13)
    (source
      android-platform-external-googletest-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_host_static"
        #:module
        "libgtest_host"
        #:blueprint
        "googletest/Android.bp"
        #:base-package
        "//external/googletest/googletest"))
    (inputs `(("libgtest" ,libgtest)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libgtest-prod-headers
  (package
    (name "libgtest-prod-headers")
    (version %latest-android-13)
    (source
      android-platform-external-googletest-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_headers"
        #:module
        "libgtest_prod_headers"
        #:blueprint
        "googletest/Android.bp"
        #:base-package
        "//external/googletest/googletest"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libidmap2-policies
  (package
    (name "libidmap2-policies")
    (version %latest-android-13)
    (source
      android-platform-frameworks-base-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libidmap2_policies"
        #:blueprint
        "cmds/idmap2/Android.bp"
        #:base-package
        "//frameworks/base/cmds/idmap2"))
    (inputs `(("libandroidfw" ,libandroidfw)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libincfs-utils
  (package
    (name "libincfs-utils")
    (version %latest-android-13)
    (source
      android-platform-system-incremental-delivery-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_static"
        #:module
        "libincfs-utils"
        #:blueprint
        "incfs/Android.bp"
        #:base-package
        "//system/incremental/delivery/incfs"))
    (inputs
      `(("libbase" ,libbase) ("libutils" ,libutils)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public liblog
  (package
    (name "liblog")
    (version %latest-android-13)
    (source
      android-platform-system-logging-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "liblog"
        #:blueprint
        "liblog/Android.bp"
        #:base-package
        "//system/logging/liblog"))
    (inputs
      `(("libbase-headers" ,libbase-headers)
        ("libcutils-headers" ,libcutils-headers)
        ("liblog-headers" ,liblog-headers)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public liblog-headers
  (package
    (name "liblog-headers")
    (version %latest-android-13)
    (source
      android-platform-system-logging-13-source)
    (build-system soong-build-system)
    (arguments
      `(#:module-type
        "cc_library_headers"
        #:module
        "liblog_headers"
        #:blueprint
        "liblog/Android.bp"
        #:base-package
        "//system/logging/liblog"
        #:libcxx #f
        #:libcxxabi #f
        #:cross-libc? #f
        #:clang ,(clang-for-target clang-14 #f)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public liblp
  (package
    (name "liblp")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "liblp"
        #:blueprint
        "fs_mgr/liblp/Android.bp"
        #:base-package
        "//system/core/fs_mgr/liblp"))
    (inputs
      `(("libcrypto" ,libcrypto)
        ("libbase" ,libbase)
        ("liblog" ,liblog)
        ("libcrypto-utils" ,libcrypto-utils)
        ("libsparse" ,libsparse)
        ("libext4-utils" ,libext4-utils)
        ("libz" ,libz)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public liblz4
  (package
    (name "liblz4")
    (version %latest-android-13)
    (source android-platform-external-lz4-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "liblz4"
        #:blueprint
        "lib/Android.bp"
        #:base-package
        "//external/lz4/lib"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libmdnssd
  (package
    (name "libmdnssd")
    (version %latest-android-13)
    (source
      android-platform-external-mdnsresponder-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libmdnssd"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/mdnsresponder"))
    (inputs
      `(("liblog" ,liblog) ("libcutils" ,libcutils)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libopenscreen-absl
  (package
    (name "libopenscreen-absl")
    (version %latest-android-13)
    (source
      android-platform-external-openscreen-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_host_static"
        #:module
        "libopenscreen_absl"
        #:blueprint
        "third_party/abseil/Android.bp"
        #:base-package
        "//external/openscreen/third_party/abseil"))
    (inputs
      `(("libopenscreen-absl-headers"
         ,libopenscreen-absl-headers)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libopenscreen-absl-headers
  (package
    (name "libopenscreen-absl-headers")
    (version %latest-android-13)
    (source
      android-platform-external-openscreen-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_headers"
        #:module
        "libopenscreen_absl_headers"
        #:blueprint
        "third_party/abseil/Android.bp"
        #:base-package
        "//external/openscreen/third_party/abseil"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libopenscreen-discovery
  (package
    (name "libopenscreen-discovery")
    (version %latest-android-13)
    (source
      android-platform-external-openscreen-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_host_static"
        #:module
        "libopenscreen-discovery"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/openscreen"))
    (inputs
      `(("libopenscreen-absl-headers"
         ,libopenscreen-absl-headers)
        ("libopenscreen-absl" ,libopenscreen-absl)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libopenscreen-platform-impl
  (package
    (name "libopenscreen-platform-impl")
    (version %latest-android-13)
    (source
      android-platform-external-openscreen-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_host_static"
        #:module
        "libopenscreen-platform-impl"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/openscreen"))
    (inputs
      `(("libopenscreen-absl-headers"
         ,libopenscreen-absl-headers)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libpng
  (package
    (name "libpng")
    (version %latest-android-13)
    (source
      android-platform-external-libpng-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libpng"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/libpng"))
    (inputs `(("libz" ,libz)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libprocessgroup-headers
  (package
    (name "libprocessgroup-headers")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_headers"
        #:module
        "libprocessgroup_headers"
        #:blueprint
        "libprocessgroup/Android.bp"
        #:base-package
        "//sytem/core/libprocessgroup"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libprotobuf-cpp-full
  (package
    (name "libprotobuf-cpp-full")
    (version %latest-android-13)
    (source
      android-platform-external-protobuf-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libprotobuf-cpp-full"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/protobuf"))
    (inputs `(("libz" ,libz)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libprotobuf-cpp-lite
  (package
    (name "libprotobuf-cpp-lite")
    (version %latest-android-13)
    (source
      android-platform-external-protobuf-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libprotobuf-cpp-lite"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/protobuf"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libprotobuf-java-lite
  (package
    (name "libprotobuf-java-lite")
    (version %latest-android-13)
    (source
      android-platform-external-protobuf-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "java_library_static"
        #:module
        "libprotobuf-java-lite"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/protobuf"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libprotoc
  (package
    (name "libprotoc")
    (version %latest-android-13)
    (source
      android-platform-external-protobuf-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libprotoc"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/protobuf"))
    (inputs `(("libz" ,libz)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libsparse
  (package
    (name "libsparse")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libsparse"
        #:blueprint
        "libsparse/Android.bp"
        #:base-package
        "//system/core/libsparse"))
    (inputs `(("libz" ,libz) ("libbase" ,libbase)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libsplit-select
  (package
    (name "libsplit-select")
    (version %latest-android-13)
    (source
      android-platform-frameworks-base-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_host_static"
        #:module
        "libsplit-select"
        #:blueprint
        "tools/split-select/Android.bp"
        #:base-package
        "//frameworks/base/tools/split-select"))
    (inputs
      `(("android-frameworks-base-tools-include" ,android-frameworks-base-tools-include)
        ("libaapt" ,libaapt)
        ("libandroidfw" ,libandroidfw)
        ("libpng" ,libpng)
        ("libutils" ,libutils)
        ("liblog" ,liblog)
        ("libcutils" ,libcutils)
        ("libexpat" ,libexpat)
        ("libziparchive" ,libziparchive)
        ("libbase" ,libbase)
        ("libz" ,libz)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libssl
  (package
    (name "libssl")
    (version %latest-android-13)
    (source
      android-platform-external-boringssl-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libssl"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/boringssl"))
    (inputs `(("libcrypto" ,libcrypto)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libstorage-literals-headers
  (package
    (name "libstorage-literals-headers")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_headers"
        #:module
        "libstorage_literals_headers"
        #:blueprint
        "fs_mgr/libstorage_literals/Android.bp"
        #:base-package
        "//system/core/fs_mgr/libstorage_literals"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libsystem-headers
  (package
    (name "libsystem-headers")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_headers"
        #:module
        "libsystem_headers"
        #:blueprint
        "libsystem/Android.bp"
        #:base-package
        "//system/core/libsystem"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libusb
  (package
    (name "libusb")
    (version %latest-android-13)
    (source
      android-platform-external-libusb-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libusb"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/libusb"))
    (inputs `(("liblog" ,liblog)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libutils
  (package
    (name "libutils")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libutils"
        #:blueprint
        "libutils/Android.bp"
        #:base-package
        "//system/core/libutils"))
    (inputs
      `(("libbase-headers" ,libbase-headers)
        ("libutils-headers" ,libutils-headers)
        ("libcutils" ,libcutils)
        ("liblog" ,liblog)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libutils-headers
  (package
    (name "libutils-headers")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_headers"
        #:module
        "libutils_headers"
        #:blueprint
        "libutils/Android.bp"
        #:base-package
        "//system/core/libutils"))
    (inputs
      `(("libbacktrace-headers" ,libbacktrace-headers)
        ("libbase-headers" ,libbase-headers)
        ("libcutils-headers" ,libcutils-headers)
        ("liblog-headers" ,liblog-headers)
        ("libprocessgroup-headers"
         ,libprocessgroup-headers)
        ("libsystem-headers" ,libsystem-headers)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libz
  (package
    (name "libz")
    (version %latest-android-13)
    (source android-platform-external-zlib-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libz"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/zlib"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libzipalign
  (package
    (name "libzipalign")
    (version %latest-android-13)
    (source android-platform-build-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library_host_static"
        #:module
        "libzipalign"
        #:blueprint
        "tools/zipalign/Android.bp"
        #:base-package
        "//build/tools/zipalign"))
    (inputs
      `(("libutils" ,libutils)
        ("libcutils" ,libcutils)
        ("liblog" ,liblog)
        ("libziparchive" ,libziparchive)
        ("libz" ,libz)
        ("libbase" ,libbase)
        ("libzopfli" ,libzopfli)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libziparchive
  (package
    (name "libziparchive")
    (version %latest-android-13)
    (source
      android-platform-system-libziparchive-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libziparchive"
        #:blueprint
        "Android.bp"
        #:base-package
        "//system/libziparchive"))
    (inputs
      `(("libgtest-prod-headers" ,libgtest-prod-headers)
        ("liblog" ,liblog)
        ("libbase" ,libbase)
        ("libz" ,libz)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libzopfli
  (package
    (name "libzopfli")
    (version %latest-android-13)
    (source
      android-platform-external-zopfli-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libzopfli"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/zopfli"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public libzstd
  (package
    (name "libzstd")
    (version %latest-android-13)
    (source android-platform-external-zstd-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_library"
        #:module
        "libzstd"
        #:blueprint
        "Android.bp"
        #:base-package
        "//external/zstd"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public platform-tools-version
  (package
    (name "platform-tools-version")
    (version %latest-android-13)
    (source android-platform-development-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "genrule"
        #:module
        "platform_tools_version"
        #:blueprint
        "sdk/Android.bp"
        #:base-package
        "//development/sdk"))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public simg2img
  (package
    (name "simg2img")
    (version %latest-android-13)
    (source android-platform-system-core-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_binary"
        #:module
        "simg2img"
        #:blueprint
        "libsparse/Android.bp"
        #:base-package
        "//system/core/libsparse"))
    (inputs
      `(("libsparse" ,libsparse)
        ("libz" ,libz)
        ("libbase" ,libbase)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public split-select
  (package
    (name "split-select")
    (version %latest-android-13)
    (source
      android-platform-frameworks-base-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_binary_host"
        #:module
        "split-select"
        #:blueprint
        "tools/split-select/Android.bp"
        #:base-package
        "//frameworks/base/tools/split-select"))
    (inputs
      `(("android-frameworks-base-tools-include" ,android-frameworks-base-tools-include)
        ("libaapt" ,libaapt)
        ("libandroidfw" ,libandroidfw)
        ("libpng" ,libpng)
        ("libutils" ,libutils)
        ("liblog" ,liblog)
        ("libcutils" ,libcutils)
        ("libexpat" ,libexpat)
        ("libziparchive" ,libziparchive)
        ("libbase" ,libbase)
        ("libz" ,libz)
        ("libsplit-select" ,libsplit-select)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public zipalign
  (package
    (name "zipalign")
    (version %latest-android-13)
    (source android-platform-build-13-source)
    (build-system soong-build-system)
    (arguments
      '(#:module-type
        "cc_binary_host"
        #:module
        "zipalign"
        #:blueprint
        "tools/zipalign/Android.bp"
        #:base-package
        "//build/tools/zipalign"))
    (inputs `(("libzipalign" ,libzipalign)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))
