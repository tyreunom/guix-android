;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android packages bison)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages bison)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils))

(define-public bison-3.5
  (package
    (inherit bison)
    (version "3.5.4")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://gnu/bison/bison-"
                                  version".tar.xz"))
              (sha256
               (base32
                "033jngb5qs9avkw5913626vb59bz8mi54g4k0lna73wph6cfj5sc"))))))
