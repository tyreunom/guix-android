;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android packages scala)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system ant)
  #:use-module (guix build-system copy)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages java)
  #:use-module (gnu packages protobuf)
  #:use-module (gnu packages web))

;; This package downloads the so-called official version of scala, a pre-built
;; binary by the scala developers.
;;
;; TODO: We should bootstrap scala properly.
;; For now, this binary version is not exported, as we can use it to rebuild
;; scala from source.
(define %binary-scala
  (package
    (name "scala")
    (version "2.13.6")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://downloads.lightbend.com/scala/"
                                  version  "/scala-" version ".tgz"))
              (sha256
               (base32
                "0hzd6pljc8z5fwins5a05rwpx2w7wmlb6gb8973c676i7i895ps9"))))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       '(("." ""))
       #:phases
       (modify-phases %standard-phases
         (add-before 'install 'set-java-home
           (lambda* (#:key inputs #:allow-other-keys)
             (substitute* (find-files "bin" ".")
               (("^#!.*" shebang)
                (string-append shebang "\nJAVA_HOME="
                               (assoc-ref inputs "openjdk"))))))
         (add-before 'set-java-home 'remove-unneeded
           (lambda _
             (for-each delete-file (find-files "bin" "bat$")))))))
    (inputs
     `(("openjdk" ,openjdk14)))
    (home-page "https://scala-lang.org/")
    (synopsis "Scala programming language")
    (description "Scala combines object-oriented and functional programming in
one concise, high-level language.  Scala's static types help avoid bugs in
complex applications, and its JVM and JavaScript runtimes let you build
high-performance systems with easy access to huge ecosystems of libraries.")
    (license license:bsd-3)))

(define scala-asm
  (package
    (inherit java-asm)
    (version "9.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/scala/scala-asm")
                     (commit "s-9.1")))
              (file-name (git-file-name "scala-asm" version))
              (sha256
               (base32
                "1wsrlb6kb0fwxjdqanxqgmq4qcyq9gqn129w3l4bj7gvlspll33l"))))
    (arguments
     `(#:jar-name "java-asm.jar"
       #:source-dir "src/main/java"
       ;; no tests
       #:tests? #f))))

(define-public java-jline3-terminal
  (package
    (name "java-jline3-terminal")
    (version "3.19.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/jline/jline3")
                     (commit (string-append "jline-parent-" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "16cfbkbj925c92xnwq5sbg7v57yh840g0mh95iyzkkajxirz9qn9"))
              (snippet
               ;; calls maven, and conflicts with ant
               `(delete-file "build"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "jline3-terminal.jar"
       #:source-dir "terminal/src/main/java"
       #:test-dir "terminal/src/test"
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'copy-resources
           (lambda _
             (copy-recursively "terminal/src/main/resources/" "build/classes")
             #t)))))
    (native-inputs
     `(("java-easymock" ,java-easymock)
       ("java-junit" ,java-junit)))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:bsd-3)))

(define-public java-jline3-reader
  (package
    (inherit java-jline3-terminal)
    (name "java-jline3-reader")
    (arguments
     `(#:jar-name "jline3-reader.jar"
       #:source-dir "reader/src/main/java"
       #:test-dir "reader/src/test"))
    (inputs
     `(("java-jline3-terminal" ,java-jline3-terminal)))))

(define-public scala
  (package
    (inherit %binary-scala)
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/scala/scala")
                     (commit (string-append "v" (package-version %binary-scala)))))
              (file-name (git-file-name "scala" (package-version %binary-scala)))
              (sha256
               (base32
                "1gl156n6nd4xnq3cb6f1bbfbb9s4cp6bd9xczl99plpx6jwnpmhl"))))
    (build-system ant-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (replace 'build
           (lambda* (#:key inputs #:allow-other-keys)
             (define* (build-project directory jar-name name #:optional (jar? #t))
               (let* ((build-dir (string-append "build/class/" name))
                      (java-files (find-files directory ".*.java$"))
                      (scala-files (find-files directory ".*.scala$")))
                 (mkdir-p build-dir)
                 (format #t "Building project ~a...~%" name)
                 (with-output-to-file (string-append build-dir "/" name ".properties")
                   (lambda _
                     (format #t "version.number=~a~%" ,(package-version this-package))
                     (format #t "maven.version.number=~a~%" ,(package-version this-package))))
                 (unless (eq? scala-files '())
                   (apply invoke "scalac" "-classpath"
                          (string-append
                            ;; Add any top-level directory in build that may contain
                            ;; .class files, but don't actually add build/ iteself or
                            ;; any individual class file.
                            (string-join
                              (filter (lambda (s) (eq? (string-count s #\/) 2))
                                      (find-files "build/class" "." #:directories? #t))
                              ":"))
                          "-d" build-dir "-nobootcp"
                          (append scala-files java-files)))
                 (unless (eq? java-files '())
                   (apply invoke "javac" "-classpath"
                          (string-append
                            (getenv "CLASSPATH") ":"
                            (string-join
                              (filter (lambda (s) (eq? (string-count s #\/) 2))
                                      (find-files "build/class" "." #:directories? #t))
                              ":"))
                          "-g" "-source" "1.8" "-target" "1.8"
                          "-d" build-dir java-files))
                 (mkdir-p "build/jar")
                 (when jar?
                   (invoke "jar" "cf" (string-append "build/jar/" jar-name)
                           "-C" build-dir "."))))

             (let ((scala-asm (assoc-ref inputs "scala-asm")))
               (setenv "CLASSPATH" (string-join (find-files scala-asm ".*.jar$") ":")))
             (setenv "JAVA_OPTS" "-Xmx1G")
             (build-project "src/library" "scala-library.jar" "library")
             (build-project "src/reflect" "scala-reflect.jar" "reflect")
             (build-project "src/compiler" "scala-compiler.jar" "compiler" #f)
             (build-project "src/interactive" "scala-compiler-interactive.jar" "interactive" #f)
             (build-project "src/scaladoc" "scala-compiler-doc.jar" "scaladoc" #f)
             (build-project "src/repl" "scala-repl.jar" "repl" #f)
             (build-project "src/repl-frontend" "scala-repl-frontend.jar" "replFrontend" #f)
             (build-project "src/scalap" "scalap.jar" "scalap")

             ;; create scala-compiler.jar as a union of some of those above
             (mkdir-p "build/class/scala-compiler")
             (with-directory-excursion "build/class/scala-compiler"
               (let ((scala-asm (assoc-ref inputs "scala-asm")))
                 (invoke "jar" "xf" (car (find-files scala-asm ".*.jar$"))))
               (copy-recursively "../compiler" ".")
               (copy-recursively "../interactive" ".")
               (copy-recursively "../scaladoc" ".")
               (copy-recursively "../repl" ".")
               (copy-recursively "../replFrontend" "."))
             (invoke "jar" "cf" "build/jar/scala-compiler.jar"
                     "-C" "build/class/scala-compiler" ".")))
         (replace 'install
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (lib (string-append out "/lib"))
                    (jna (assoc-ref inputs "java-native-access"))
                    (jline-terminal (assoc-ref inputs "java-jline3-terminal"))
                    (jline-reader (assoc-ref inputs "java-jline3-reader")))
               (mkdir-p lib)
               (for-each
                 (lambda (jar)
                   (copy-file jar (string-append lib "/" (basename jar))))
                 (find-files "build/jar" ".*.jar$"))
               (symlink (car (find-files jna "linux-.*.jar$"))
                        (string-append lib "/jna-lib.jar"))
               (symlink (car (find-files jna "jna.jar$"))
                        (string-append lib "/jna.jar"))
               (symlink (car (find-files jline-reader ".*.jar$"))
                        (string-append lib "/jline-reader.jar"))
               (symlink (car (find-files jline-terminal ".*.jar$"))
                        (string-append lib "/jline-terminal.jar")))))
         (add-after 'install 'install-bin
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin")))
               (define (install-script script class)
                 (let ((script (string-append bin "/" script)))
                   (copy-file "src/compiler/templates/tool-unix.tmpl" script)
                   ;; See project/ScalaTool and build.sbt
                   (substitute* script
                     (("@@") "@")
                     (("@class@") class)
                     (("@properties@") "-Dscala.home=\"$SCALA_HOME\"")
                     (("@javaflags@") "-Xmx256M -Xms32M")
                     (("@toolflags@") "")
                     (("@classpath@") ""))
                   (chmod script #o755)))
               (mkdir-p bin)
               (install-script "scala" "scala.tools.nsc.MainGenericRunner")
               (install-script "scalac" "scala.tools.nsc.Main")
               (install-script "fsc" "scala.tools.nsc.fsc.CompileClient")
               (install-script "scaladoc" "scala.tools.nsc.ScalaDoc")
               (install-script "scalap" "scala.tools.scalap.Main")))))))
    (inputs
     `(("scala" ,%binary-scala)
       ("scala-asm" ,scala-asm)
       ("java-jline3-terminal" ,java-jline3-terminal)
       ("java-jline3-reader" ,java-jline3-reader)
       ("java-native-access" ,java-native-access)))))
