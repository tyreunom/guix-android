;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android packages android-sdk)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages java)
  #:use-module (gnu packages java-xml)
  #:use-module (gnu packages xml)
  #:use-module (guix build-system ant)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix utils))

;; This is the version of the SDK used to build Replicant 6.0.
(define replicant-version "6.0.1.81")
(define replicant-commit "android-6.0.1_r81")

;; Google copied different external projects to their own repos, and gradle
;; knows them as the internal name.

;; This package is known to gradle as :external:fat32lib, but it is actually
;; not an android-specific project.  The original version is at
;; https://github.com/waldheinz/fat32-lib
(define-public android-java-external-fat32lib
  (package
    (name "android-java-external-fat32lib")
    (version replicant-version)
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://android.googlesource.com/platform/external/fat32lib")
                     ;; current master
                     (commit "3b20147d70c788a5406d76a0048689f33793a6c2")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0m3zaybz6f7s20349hq16580v0xqahld22jicpivchbxami13jbq"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "fat32lib.jar"
       #:source-dir "src/main/java"
       ;; no tests
       #:tests? #f))
    (home-page "https://android.googlesource.com/platform/external/fat32lib")
    (synopsis "Fat file system manipulation from Java")
    (description "This library allows to manipulate FAT file systems using the
Java programming language.  Because of its age and simplicity, FAT can be
called the least common denominator in file systems, being used in digital
cameras, cell phones, etc. and being supported by almost every operating system
in existence.  This project aims at making FAT file systems accessible for
Java programs without using the operating system to interpret the on-disk
structures.  Instead, it provides a pure Java implementation of the FAT
specification.")
    (license license:lgpl2.1+)))

(define android-platform-tools-base-origin
  (origin
    (method git-fetch)
    (uri (git-reference
           (url "https://android.googlesource.com/platform/tools/base")
           (commit replicant-commit)))
    (file-name (git-file-name "android-java-tools-base" replicant-version))
    (sha256
     (base32
      "0axqk4j7km2m4kbb5vd6gmwj9sxdl8zkx392llfb2p8pgzwaf706"))))

(define-public android-java-tools-base-annotations
  (package
    (name "android-java-tools-base-annotations")
    (version replicant-version)
    (source android-platform-tools-base-origin)
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "android-tools-base-annotations.jar"
       #:source-dir "annotations/src/main/java"
       ;; No tests
       #:tests? #f))
    (home-page "https://android.googlesource.com/platform/tools/base")
    (synopsis "Annotations used in Android application development")
    (description "This package contains Java annotations useful for Android
application development.")
    (license license:asl2.0)))

(define-public jobb
  (package
    (name "jobb")
    (version replicant-version)
    (source android-platform-tools-base-origin)
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "jobb.jar"
       #:source-dir "jobb/src/main/java"
       #:tests? #f ;no tests
       #:main-class "com.android.jobb.Main"
       #:phases
       (modify-phases %standard-phases
         (add-after 'install 'install-bin
           (lambda* (#:key outputs inputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin"))
                    (jobb (string-append bin "/jobb"))
                    (fat32lib (assoc-ref inputs "android-java-external-fat32lib")))
               (mkdir-p bin)
               (with-output-to-file jobb
                 (lambda _
                   (format #t "#!~a~%" (which "sh"))
                   (format #t "exec java -classpath ~a:~a com.android.jobb.Main \"$@\""
                           (car (find-files out "\\.jar$"))
                           (car (find-files fat32lib "\\.jar$")))))
               (chmod jobb #o755))
             #t)))))
    (home-page "https://android.googlesource.com/platform/tools/base")
    (inputs
     `(("android-java-external-fat32lib" ,android-java-external-fat32lib)))
    (synopsis "Create OBB files for use on Android")
    (description "Jobb is a tool to create Opaque Binary Blob (OBB) files
for Android.")
    ;; TODO: Unclear licensing
    (license (list license:isc
                   license:asl2.0
                   license:bsd-2
                   ;; Twofish is uncopyrighted and license-free, and was created and analyzed by:                   
                   ;; Bruce Schneier - John Kelsey - Doug Whiting                                     
                   ;; David Wagner - Chris Hall - Niels Ferguson
                   ;; At the same time, the source code says "all rights reserved"
                   ;; and lists different authors.
                   ;; Is this really free software?
                   ;; From http://www.cryptix.org/: All projects are governed
                   ;; by the Cryptix General License. Note: the copyright on the
                   ;; software has been returned to the individual developers
                   ;; when the Cryptix Foundation ltd. was dissolved.
                   (license:non-copyleft "file://jobb/NOTICE")))))

(define-public android-java-tools-base-common
  (package
    (name "android-java-tools-base-common")
    (version replicant-version)
    (source android-platform-tools-base-origin)
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "android-tools-base-common.jar"
       #:source-dir "common/src/main/java"
       #:test-dir "common/src/test"))
    (inputs
     `(("android-java-tools-base-annotations" ,android-java-tools-base-annotations)
       ("java-guava" ,java-guava)))
    (native-inputs
     `(("java-junit" ,java-junit)))
    (home-page "https://android.googlesource.com/platform/tools/base")
    (synopsis "Common code for the Android SDK")
    (description "This package contains resource configuration enums, as well as
interfaces and common code used by the Android SDK.")
    (license license:asl2.0)))

(define-public android-java-tools-base-layoutlib-api
  (package
    (name "android-java-tools-base-layoutlib-api")
    (version replicant-version)
    (source android-platform-tools-base-origin)
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "android-tools-base-layoutlib-api.jar"
       #:source-dir "layoutlib-api/src/main/java"
       #:test-dir "layoutlib-api/src/test"))
    (inputs
     `(("android-java-tools-base-annotations" ,android-java-tools-base-annotations)
       ("android-java-tools-base-common" ,android-java-tools-base-common)
       ("java-jetbrains-annotations" ,java-jetbrains-annotations)
       ("java-kxml2" ,java-kxml2)
       ("java-xpp3" ,java-xpp3)))
    (native-inputs
     `(("java-junit" ,java-junit)))
    (home-page "https://android.googlesource.com/platform/tools/base")
    (synopsis "")
    (description "This package contains code describing the API used to load
and interact with layoutlib in Android.  It is to be packaged with clients
accessing layoutlib.")
    (license license:asl2.0)))

(define-public android-java-tools-base-dvlib
  (package
    (name "android-java-tools-base-dvlib")
    (version replicant-version)
    (source android-platform-tools-base-origin)
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "android-tools-base-dvlib.jar"
       #:source-dir "device_validator/dvlib/src/main/java"
       #:test-dir "device_validator/dvlib/src/test"
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'copy-resources
           (lambda _
             (copy-recursively "device_validator/dvlib/src/main/resources"
                               "build/classes")
             #t)))))
    (inputs
     `(("android-java-tools-base-annotations" ,android-java-tools-base-annotations)
       ("android-java-tools-base-common" ,android-java-tools-base-common)))
    (native-inputs
     `(("java-junit" ,java-junit)))
    (home-page "https://android.googlesource.com/platform/tools/base")
    (synopsis "Android XML files manipulation")
    (description "This package contains a library to manage the Android device
database XML files.")
    (license license:asl2.0)))

(define-public android-java-tools-base-sdklib
  (package
    (name "android-java-tools-base-sdklib")
    (version replicant-version)
    (source android-platform-tools-base-origin)
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "android-tools-base-sdklib.jar"
       #:source-dir "sdklib/src/main/java"
       #:test-dir "sdklib/src/test"
       #:test-exclude (list ;"**/DeviceSchemaTest.java"
                            "**/DeviceManagerTest.java"
                            ;; Unexpected warning message because of more recent dependencies
                            "**/DebugKeyProviderTest.java"
                            ;; Cannot find a validator
                            "**/AddonsListFetcherTest.java"
                            "**/SdkStatsTest.java"
                            "**/SdkAddonSourceTest.java"
                            "**/SdkRepoSourceTest.java"
                            "**/SdkSysImgSourceTest.java"
                            "**/Validate*Test.java"
       )
       #:phases
       (modify-phases %standard-phases
         (add-before 'check 'copy-test-sources
           (lambda _
             (copy-recursively "device_validator/dvlib/src/test/java"
                               "sdklib/src/test/java")
             (copy-recursively "device_validator/dvlib/src/test/resources"
                               "build/test-classes")
             (copy-recursively "sdklib/src/test/java/com/android/sdklib/testdata"
                               "build/test-classes/com/android/sdklib/testdata")
             #t)))))
    (inputs
     `(("android-java-tools-base-annotations" ,android-java-tools-base-annotations)
       ("android-java-tools-base-common" ,android-java-tools-base-common)
       ("android-java-tools-base-layoutlib-api" ,android-java-tools-base-layoutlib-api)
       ("android-java-tools-base-dvlib" ,android-java-tools-base-dvlib)
       ("java-commons-compress" ,java-commons-compress)
       ("java-guava" ,java-guava)
       ("java-httpcomponents-httpclient" ,java-httpcomponents-httpclient)
       ("java-httpcomponents-httpcore" ,java-httpcomponents-httpcore)
       ("java-httpcomponents-httpmime" ,java-httpcomponents-httpmime)))
    (native-inputs
     `(("java-junit" ,java-junit)))
    (home-page "https://android.googlesource.com/platform/tools/base")
    (synopsis "Android SDK download library")
    (description "This package contains a library to parse and download the
Android SDK.")
    (license license:asl2.0)))
