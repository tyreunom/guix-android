;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android packages android-build)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages golang-build)
  #:use-module (gnu packages ninja)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system go)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix utils))

(define-public blueprint
  (package
    (name "blueprint")
    (version "git")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://android.googlesource.com/platform/build/blueprint")
                     (commit "ba9a3b9ea887b49c97cb7821369293e4a519c692")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1gzn6sqzxjrqis676im6fnqnhpjmfsx58xwjcb6wjlfmsclc6sdg"))
              (patches
                (search-patches "blueprint-fix-microfactory-rpath.patch"))))
    (build-system go-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (replace 'build
           (lambda _
             (with-directory-excursion "src"
               (mkdir "build")
               (with-directory-excursion "build"
                 (invoke "bash" "../bootstrap.bash")
                 (invoke "bash" "./blueprint.bash")))
             #t))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (mkdir-p (assoc-ref outputs "out"))
             (copy-recursively "src" (string-append (assoc-ref outputs "out") "/src"))
             #t)))))
    (native-inputs
     `(("ninja" ,ninja)))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define-public ninja-for-android
  ;; NOTE: always use master
  (let ((commit "ffd6a3e01d9cf37cc48c966af361eabacedf500b")
        (revision "0"))
    (package
      (inherit ninja)
      (version (git-version "1.6.0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                       (url "https://android.googlesource.com/platform/external/ninja")
                       (commit commit)))
                (file-name (git-file-name "ninja" version))
                (sha256
                 (base32
                  "1b7s3z61nsf6zxk5270i841yz5p8kmndzwjc8dlrznqsqvj29bc5")))))))
      ;(arguments
      ;  (substitute-keyword-arguments (package-arguments ninja)
      ;    ((#:phases phases)
      ;     `(modify-phases ,phases
      ;        (delete 'check)))))))

(define-public golang-protobuf-for-android
  (origin
          (method git-fetch)
          (uri (git-reference
                 (url "https://android.googlesource.com/platform/external/golang-protobuf")
                 (commit "77559201bef4b7e709f77e7ee2a66bdcc903add6")))
          (file-name (git-file-name "golang-protobuf" "git"))
          (sha256
           (base32
            "0ln8q13m3bbh33j1vw269v6h6y5wp37x414mqhkc57kjfl4s3c6z"))))

(define-public kati
  (package
    (name "kati")
    (version "git")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://android.googlesource.com/platform/build/kati")
                     (commit "android-security-10.0.0_r48")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "15jlrm8sg36pplnzsya3bq5506krznfaskhdirlswj5qfzwfykbm"))))
    (build-system gnu-build-system)
    (arguments
     `(#:test-target "test"
       ; TODO: ignoring for now, needs some more dependencies.
       ; Upstreams says to run:
       ;   make test
       ;   go test --ckati
       ;   go test --ckati --ninja
       ;   go test --ckati --ninja --all
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (replace 'build
           (lambda _
             (invoke "make" "ckati")
             #t))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin")))
               (mkdir-p bin)
               (install-file "ckati" bin))
             #t)))))
    (home-page "https://github.com/google/kati")
    (synopsis "GNU Make clone")
    (description "kati is an experimental GNU make clone.  The main goal of
this tool is to speed-up incremental build of Android.  Currently, kati does
not offer a faster build by itself.  It instead converts your Makefile to a
ninja file.")
    (license license:asl2.0)))

(define-public soong
  (package
    (name "soong")
    (version "git")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://android.googlesource.com/platform/build/soong")
                     (commit "4964a5e0004ab80e3cbc7bd030ce5ddc33e5d36e")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0id8w1wwv2n65d8008im0b1xbzrlpspja4306fc4xcdrg75dc98c"))))
    (build-system go-build-system)
    (arguments
     `(#:import-path "build/soong"
       #:phases
       (modify-phases %standard-phases
         (replace 'build
           (lambda* (#:key inputs #:allow-other-keys)
             ;; These copies are required because of the finder.
             ;; build/soong/ui/build/finder.go is configured to look from
             ;; the build root (here "src"): it will look for Android.bp
             ;; files, as well as build/blueprint/Blueprints.
             ;; However, it does not follow symlinks, and it doesn't seem
             ;; possible to give it a list of external directories.
             ;;
             ;; TODO: patch the finder to follow symlinks or find our
             ;; own Android.bp in inputs.
             ;; Other possibility: generate an Android.bp.list ourselves.
             ;;
             ;; Yes, this is vendoring.  For Go programs, this is expected,
             ;; though ugly.
             ;; XXX: What happens when using this soong, and what about other
             ;; languages? Are they also bundled because of the way soong works?
             ;; If we have built a dependency, do we still need to provide its
             ;; sources, or does Android.bp suffice?
             (mkdir-p "src/external")
             (copy-recursively (assoc-ref inputs "golang-protobuf")
                               "src/external/golang-protobuf")
             (copy-recursively
               (string-append (assoc-ref inputs "blueprint") "/src")
               "src/build/blueprint")
             ;; We need this or the process cannot find anything
             (symlink "build/soong/root.bp" "src/Android.bp")
             (with-directory-excursion "src/build/soong"
               (substitute* (find-files "." ".*.bash$")
                 (("/bin/pwd") (which "pwd")))
               (substitute* "scripts/microfactory.bash"
                 (("GOROOT=.*") (string-append "GOROOT=" (assoc-ref inputs "go") "\n"))
                 (("\\$\\{TOP\\}/build/blueprint")
                  (string-append (assoc-ref inputs "blueprint") "/src"))
                 ;; ensure libgcc is in Rpath
                 (("EXTRA_ARGS=\"")
                  (string-append "EXTRA_ARGS=\"-r " (assoc-ref inputs "gcc:lib") "/lib ")))
               ;; TODO: this script builds soong_ui (from go sources) using
               ;; microfactory (part of blueprint).  However, the resulti binary
               ;; cannot run, because it is missing libgcc_s.so and has no RPATH.
               (format #t "~a~%" (getenv "LIBRARY_PATH"))
               (setenv "LANG" "en_US.UTF-8")
               ;; this part tries to find proper locales, but "locale -a" doesn't
               ;; work in the build environment
               (substitute* "ui/build/config.go"
                 (("ret.configureLocale\\(ctx\\)") ""))
               ;; bypass ram check (it wants 16GB minimum, but try anyway,
               ;; after all we can always swap :p)
               ;; Apparently there's a better way to do it with environment
               ;; variables? TODO: ask GNUtoo
               (substitute* "ui/build/build.go"
                 (("ram <= 16") "ram <= 0"))
               (substitute* "ui/build/path.go"
                 (("cfg.TrimPath,")
                  (string-append "cfg.Rpath = \"" (assoc-ref inputs "gcc:lib")
                                 "/lib\"\ncfg.TrimPath,"))
                 ;; TEMPORARY_DISABLE_PATH_RESTRICTIONS is obsolete, but we
                 ;; don't want to rely on the path interposer.
                 (("ctx.Fatalln\\(\"TEMPORARY_DISABLE_PATH_RESTRICTIONS.*")
                  "return\n"))
               (substitute* "ui/build/soong.go"
                 (("./build/blueprint")
                  (string-append (assoc-ref inputs "blueprint") "/src"))
                 (("build/blueprint")
                  (string-append (assoc-ref inputs "blueprint") "/src")))
               ;; set the proper path to ninja
               (substitute* '("scripts/transitive-deps.sh" "scripts/reverse-deps.sh")
                 (("ninja_bin=.*")
                  (string-append "ninja_bin=" (assoc-ref inputs "ninja") "/bin/ninja")))
               ;; ensure we find binaries from the path, not from prebuilts
               (substitute* "ui/build/config.go"
                 (("return.*prebuilts/build-tools.*") "return name"))
               ;; actually don't use the path interposer. It is used for
               ;; reproducible builds. It disallows execution of host binaries
               ;; and uses instead its own prebuilt versions. However, (1) our
               ;; binaries are reproducible and (2) it fails because it doesn't
               ;; find binaries in expected places.
               (setenv "TEMPORARY_DISABLE_PATH_RESTRICTIONS" "true")
               (invoke "bash" "./soong_ui.bash" "--make-mode" "--skip-make"))
             #t)))))
    (native-inputs
     `(("blueprint" ,blueprint)
       ("ninja" ,ninja-for-android)
       ("gcc:lib" ,gcc "lib")))
    (inputs
     `(("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
       ("golang-protobuf" ,golang-protobuf-for-android))) 
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))
