;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020, 2021 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android import repo)
  #:use-module (android build blueprint)
  #:use-module (android build soong)
  #:use-module (android build soong makefile)
  #:use-module (android iri)
  #:use-module (gcrypt hash)
  #:use-module (guix base32)
  #:use-module (guix build utils)
  #:use-module (guix colors)
  #:use-module (guix git)
  #:use-module (guix import utils)
  #:use-module ((guix serialization) #:select (write-file))
  #:use-module (guix ui)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module (ice-9 receive)
  #:use-module (ice-9 textual-ports)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-39)
  #:use-module (sxml simple)
  #:export (%default-manifest-url

            get-manifest
            parse-manifest
            fetch-manifest-repositories

            get-all-bp-files
            get-bp-maps
            import-source
            import-package
            import-recursively))

;; To use from a repl:
;; $ guix repl -L .
;; (guile)> ,use (android import repo)
;; (guile)> (define manifest (get-manifest))
;; (guile)> (define root (fetch-manifest-repositories manifest))
;;
;; To import, from a repl:
;; (guile)> (define bp-files (get-all-bp-files root))
;; (guile)> (define bp-maps (get-bp-maps bp-files))
;; (guile)> (import-package manifest root bp-maps "foo")
;;
;; For importing all dependencies to a file:
;; (guile)> (import-recursively manifest root bp-maps '("adb" "fastboot")
;;                              "sources.scm" "packages.scm")

(define %default-manifest-url "https://android.googlesource.com/platform/manifest")

;; An android-manifest represents the configuration of a collection of repositories
;; used to build android and related tools.
(define-record-type android-manifest
  (make-android-manifest file remotes revision remote sync projects)
  android-manifest?
  (file     android-manifest-file)
  (remotes  android-manifest-remotes)
  (revision android-manifest-revision)
  (remote   android-manifest-remote)
  (sync     android-manifest-sync)
  (projects android-manifest-projects))

(define* (update-android-manifest
           manifest #:key
           (file     (android-manifest-file manifest))
           (remotes  (android-manifest-remotes manifest))
           (revision (android-manifest-revision manifest))
           (remote   (android-manifest-remote manifest))
           (sync     (android-manifest-sync manifest))
           (projects (android-manifest-projects manifest)))
  "Return an updated version of manifest where fields specified with a keyword
have a new (passed) value, and other fields retain their original value from
manifest."
  (make-android-manifest file remotes revision remote sync projects))

;; A remote is a location where some or all android repositories are located.
;; Forks may have different remotes.
(define-record-type remote
  (make-remote name base review)
  remote?
  (name   remote-name)
  (base   remote-base)
  (review remote-review))

;; A project is a source code repository.
(define-record-type project
  (make-project name path groups clone-depth copies links)
  project?
  (name        project-name)
  (path        project-path)
  (groups      project-groups)
  (clone-depth project-clone-depth)
  (copies      project-copies)
  (links       project-links))

;; Parsers used to convert the manifest file (xml) to our internal representation
;; (an android-manifest object).
(define (parse-copies commands)
  (match commands
    (((? string? _) commands ...) (parse-copies commands))
    (() '())
    ((('copyfile ('@ ('src src) ('dest dest))) commands ...)
     (cons `(,src . ,dest) (parse-copies commands)))
    ((('linkfile _ ...) commands ...) (parse-copies commands))))

(define (parse-links commands)
  (match commands
    (((? string? _) commands ...) (parse-links commands))
    (() '())
    ((('linkfile ('@ ('src src) ('dest dest))) commands ...)
     (cons `(,src . ,dest) (parse-links commands)))
    ((('copyfile _ ...) commands ...) (parse-links commands))))

(define (parse-project project)
  (match project
    ((('@ args ...) commands ...)
     (let ((groups (assoc-ref args 'groups)))
       (make-project (car (assoc-ref args 'name))
                     (car (assoc-ref args 'path))
                     (if groups (string-split (car groups) #\,) '())
                     (string->number
                       (car (or (assoc-ref args 'clone-depth) '("0"))))
                     (parse-copies commands)
                     (parse-links commands))))))

(define (parse-remote remote base)
  (match remote
    ((('@ args ...))
     (let ((name (assoc-ref args 'name))
           (fetch (assoc-ref args 'fetch))
           (review (assoc-ref args 'review)))
       (make-remote (car name)
                    (resolve-iri base (car fetch))
                    (car review))))))

(define (parse-manifest-aux manifest file url)
  (define (parse-manifest-aux content)
    (let loop ((content content)
               (manifest (make-android-manifest file '() #f #f #f '())))
      (match content
        (() manifest)
        (((? string? _) content ...)
         (loop content manifest))
        ((('superproject _ ...) content ...)
         (loop content manifest))
        ((('contactinfo _ ...) content ...)
         (loop content manifest))
        ((('repo-hooks _ ...) content ...)
         (loop content manifest))
        ((('remote remote ...) content ...)
         (loop content (update-android-manifest
                         manifest
                         #:remotes
                         (cons
                           (parse-remote remote url)
                           (android-manifest-remotes manifest)))))
        ((('default ('@ args ...)) content ...)
         (let ((revision (assoc-ref args 'revision))
               (remote (assoc-ref args 'remote))
               (sync (assoc-ref args 'sync-j)))
           (loop content
                 (update-android-manifest
                   manifest
                   #:revision (if revision
                                  (car revision)
                                  (android-manifest-revision manifest))
                   #:remote (if remote
                                (car remote)
                                (android-manifest-remote manifest))
                   #:sync (if sync
                              (car sync)
                              (android-manifest-sync manifest))))))
        ((('project project ...) content ...)
         (loop content
               (update-android-manifest
                 manifest
                 #:projects
                 (cons
                   (parse-project project)
                   (android-manifest-projects manifest))))))))

  (match manifest
    (('*TOP* ('*PI* _ ...) ('manifest content ...))
     (parse-manifest-aux content))))

(define (parse-manifest file url)
  "Take a filename as @var{file} that corresponds to the name of the file containing
the manifest in xml form, and @var{url}, the url of the repository from which
the file was downloaded.  Return a parsed manifest as an @code{android-manifest}
object."
  (call-with-input-file file
    (lambda (port)
      (let ((manifest (xml->sxml port)))
        (parse-manifest-aux manifest file url)))))

(define* (get-manifest #:key (url %default-manifest-url) (version %latest-android-13)
                       (cache-directory (%repository-cache-directory)))
  "Download a manifest from a repository (@var{url}) at a given branch (@var{version}).
Return an @code{android-manifest} object."
  (receive (location commit _)
    (update-cached-checkout url #:ref `(branch . ,version))
    (parameterize ((%repository-cache-directory cache-directory))
      (parse-manifest
        (string-append location "/default.xml")
        url))))

(define (get-default-remote remotes name)
  (let loop ((remotes remotes))
    (match remotes
      (() (throw 'cannot-find name))
      ((remote remotes ...)
       (if (equal? (remote-name remote) name)
           remote
           (loop remotes))))))

(define* (fetch-manifest-repositories
           manifest #:key (cache-directory (%repository-cache-directory)))
  "Synchronous fetch of the various repositories specified in @var{manifest},
an @code{android-manifest} object.  The repositories are cloned in a common
directory under %repository-cache-directory.  This repository depends on the
contents of the manifest file, so you can end up with multiple checkouts
if you fetch multiple versions.

Each project is downloaded in a subdirectory that is specified by the manifest,
files are copied and symlinked as specified.  This function is supposed to
mimick the behavior of the repo tool."

  (define (get-base-cache-dir)
    (call-with-input-file (android-manifest-file manifest)
      (lambda (port)
        (string-append (%repository-cache-directory) "/"
                       (bytevector->base32-string
                         (sha256 (string->utf8 (get-string-all port))))))))

  (parameterize ((%repository-cache-directory cache-directory))
    (let* ((manifest (update-android-manifest
                       manifest #:projects (reverse (android-manifest-projects manifest))))
           (repo (get-default-remote (android-manifest-remotes manifest)
                                     (android-manifest-remote manifest)))
           (repository-base (remote-base repo))
           (revision (android-manifest-revision manifest))
           (base-cache-dir (get-base-cache-dir)))
      (let loop ((projects (android-manifest-projects manifest)))
        (unless (null? projects)
          (let* ((project (car projects))
                 (clone-depth (project-clone-depth project))
                 (name (project-name project))
                 (cache-directory (string-append base-cache-dir "/" (project-path project))))
            (if (string-contains name "prebuilts")
                (format #t (colorize-string "Skipping ~a. ~a remaining.~%" (color YELLOW))
                        name (- (length projects) 1))
                (begin
                  (format #t (colorize-string "Updating ~a. ~a remaining.~%" (color GREEN))
                          name (- (length projects) 1))
                  (format #t (colorize-string "" (color CLEAR)))
                  (mkdir-p cache-directory)
                  (with-directory-excursion cache-directory
                    (unless (file-exists? ".git")
                      (invoke/quiet "git" "init"))
                    (catch #t
                      (lambda _
                        (invoke/quiet "git" "remote" "add" "origin"
                                      (resolve-iri repository-base name)))
                      (const #t))
                    (catch #t
                      (lambda _
                        (invoke/quiet "git" "checkout" "FETCH_HEAD"))
                      (lambda _
                        (if (> clone-depth 0)
                            (invoke "git" "fetch" "--depth" (number->string clone-depth)
                                    "origin" revision)
                            (invoke "git" "fetch" "--depth" "1" "origin" revision))
                        (invoke/quiet "git" "checkout" "FETCH_HEAD"))))
                  ;; XXX: this would mimick the behavior of the repo tool, but
                  ;; it also messes up recursive hash by adding additional files
                  ;; in some repos.
                  #;(with-directory-excursion base-cache-dir
                    (for-each
                      (match-lambda
                        ((src . dest)
                         (mkdir-p (dirname dest))
                         (when (file-exists? dest)
                           (delete-file dest))
                         (copy-file (string-append cache-directory "/" src)
                                    dest)))
                      (project-copies project))
                    (for-each
                      (match-lambda
                        ((src . dest)
                         (mkdir-p (dirname dest))
                         (when (file-exists? dest)
                           (delete-file dest))
                         (symlink (string-append cache-directory "/" src)
                                  dest)))
                      (project-links project))))))
          (loop (cdr projects))))

      (display "Finished updating source repositories.")
      (newline)
      base-cache-dir)))

(define (import-source-definition name url branch hash)
  `(define-public ,(string->symbol name)
     (origin
       (method git-fetch)
       (uri (git-reference
              (url ,url)
              (commit ,branch)))
       (file-name (git-file-name ,name ,branch))
       (sha256 (base32 ,hash)))))

(define (import-module name revision source module-type module-name blueprint inputs)
  (define branch
    (let ((branch (if (string-prefix? "refs/tags/" revision)
                      (substring revision 10)
                      revision)))
      (cond
        ((equal? branch %latest-android-13)
         '%latest-android-13)
        ((equal? branch %latest-android-12)
         '%latest-android-12)
        ((equal? branch %latest-android-11)
         '%latest-android-11)
        ((equal? branch %latest-android-10)
         '%latest-android-10)
        (else branch))))

  `(define-public ,(string->symbol name)
     (package
       (name ,name)
       (version ,branch)
       (source ,(string->symbol source))
       (build-system soong-build-system)
       (arguments
         ,(list 'quote
            (list #:module-type module-type
                  #:module module-name
                  #:blueprint blueprint)))
       ,@(maybe-inputs (map android-name->guix-name inputs))
       (home-page "")
       (synopsis "")
       (description "")
       (license #f))))

(define (git-hash repo)
  (let-values (((port get-hash) (open-sha256-port)))
    (write-file repo port #:select?
                (lambda (file stats) (not (string=? (basename file) ".git"))))
    (force-output port)
    (get-hash)))

(define (get-all-bp-files root)
  (find-files root "^Android\\.bp$"))

(define-record-type bp-map
  (make-bp-map vars modules)
  bp-map?
  (vars bp-map-vars)
  (modules bp-map-modules))

(define (get-bp-maps bp-files)
  (let-values (((vars modules) (get-lookup-tables bp-files)))
    (make-bp-map vars modules)))

(define (import-source manifest root source)
  (let* ((project (car source))
         (module-dir (project-path project))
         (repo (get-default-remote (android-manifest-remotes manifest)
                                   (android-manifest-remote manifest)))
         (repository-base (remote-base repo))
         (revision (android-manifest-revision manifest))
         (branch (if (string-prefix? "refs/tags/" revision)
                     (substring revision 10)
                     revision))
         (source-name (string-append
                        "android-"
                        (string-join
                          (string-split
                            (string-join (string-split (project-name project) #\/) "-")
                            #\_)
                          "-")
                        (cond
                          ((equal? branch %latest-android-13) "-13")
                          ((equal? branch %latest-android-12) "-12")
                          ((equal? branch %latest-android-11) "-11")
                          ((equal? branch %latest-android-10) "-10")
                          (else ""))
                        "-source"))
         (branch (cond
                   ((equal? branch %latest-android-13)
                    '%latest-android-13)
                   ((equal? branch %latest-android-12)
                    '%latest-android-12)
                   ((equal? branch %latest-android-11)
                    '%latest-android-11)
                   ((equal? branch %latest-android-10)
                    '%latest-android-10)
                   (else branch)))
         (hash (bytevector->nix-base32-string
                 (git-hash (string-append root "/" module-dir)))))
    (import-source-definition
      source-name
      (resolve-iri repository-base (project-name project))
      branch
      hash)))

(define (import-package manifest root bp-map module-name)
  "Import a package called @var{module-name}, by looking for it in the
@var{bp-files}, with help from @var{manifest} to find the original source
repository."
   (let* ((vars (bp-map-vars bp-map))
          (modules (bp-map-modules bp-map))
          (module (get-module-with-map vars modules #f module-name))
          (module-dir (string-drop
                        (dirname (blueprint-module-file module))
                        (+ (string-length root) 1)))
          (repo (get-default-remote (android-manifest-remotes manifest)
                                    (android-manifest-remote manifest)))
          (repository-base (remote-base repo))
          (projects (android-manifest-projects manifest))
          (project (find
                     (lambda (project)
                       (string-prefix? (project-path project) module-dir))
                     projects))
          (revision (android-manifest-revision manifest))
          (branch (if (string-prefix? "refs/tags/" revision)
                      (substring revision 10)
                      revision))
          (source-name (string-append
                         "android-"
                         (string-join
                           (string-split
                             (string-join (string-split (project-name project) #\/) "-")
                             #\_)
                           "-")
                         (cond
                           ((equal? branch %latest-android-13) "-13")
                           ((equal? branch %latest-android-12) "-12")
                           ((equal? branch %latest-android-11) "-11")
                           ((equal? branch %latest-android-10) "-10")
                           (else ""))
                         "-source"))
          (makefile
            (with-directory-excursion (dirname (blueprint-module-file module))
              (get-makefile dummy-resolve-dep module
                            (blueprint-module-type module) module-name
                            (%current-system) #f
                            #:dry-run? #t)))
          (build-dependencies
            (flatten (map makefile-rule-dependencies (makefile-build-rules makefile))))
          (install-dependencies
            (flatten (map (match-lambda ((type input dest deps) deps))
                                (makefile-install-rules makefile))))
          (dependencies (uniq (append build-dependencies install-dependencies)))
          (blueprint-path
            (string-join
              (drop (string-split module-dir #\/)
                    (length (string-split (project-path project) #\/)))
              "/"))
          (package (import-module
                     (android-name->guix-name module-name)
                     revision
                     source-name
                     (blueprint-module-type module)
                     module-name
                     (string-append
                       blueprint-path
                       (if (equal? blueprint-path "")
                           ""
                           "/")
                       "Android.bp")
                     dependencies)))
     (values
       package
       (cons project module-dir)
       dependencies)))

(define (file->module-name filename)
  (let* ((base (basename filename ".scm"))
         (dir (dirname filename)))
    (string-join
      (string-split (if (or (equal? dir "") (equal? dir "."))
                        base
                        (string-append dir "/" base)) #\/)
      " ")))

(define (sources-header filename)
  (string-append
    ";;; GNU Guix --- Functional package management for GNU
;;; Automatically imported by (android import repo).
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (" (file->module-name filename) ")
  #:use-module (android build blueprint)
  #:use-module (guix packages)
  #:use-module (guix git-download))
"))

(define (packages-header sources-filename packages-filename)
  (string-append
    ";;; GNU Guix --- Functional package management for GNU
;;; Automatically imported by (android import repo).
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (" (file->module-name packages-filename) ")
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (android build-system soong)
  #:use-module (" (file->module-name sources-filename) ")
  #:use-module (gnu packages vim)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix utils))
"))

(define (package-rewriting-port output)
  "Return a port that properly formats packages."
  (define (write-string str)
    (let loop ((chars (string->list str))
               (seen-keyword? #f))
      (match chars
        (()
         #t)
        ((#\newline rest ...)
         (if seen-keyword?
             ; skip newline so the argument is on the same line.
             (let loop2 ((rest rest))
               (match rest
                 (() #t)
                 ((#\  rest ...)
                  (loop2 rest))
                 ((chr rest ...)
                  (write-char #\  output)
                  (write-char chr output)
                  (loop rest #f))))
             (begin
               (write-char #\newline output)
               (loop rest #f))))
        ((#\  rest ...)
         (write-char #\  output)
         (loop rest #f)); reset keyword if we see a space, this is properly indented
        ((#\# #\: rest ...)
         (write-char #\# output)
         (write-char #\: output)
         (loop rest #t))
        ((chr rest ...)
         (write-char chr output)
         (loop rest seen-keyword?)))))

  (make-soft-port (vector (cut write-char <>)
                          write-string
                          (lambda _ #t)           ; flush
                          #f
                          (lambda _ #t)           ; close
                          #f)
                  "w"))

(define (import-recursively manifest root bp-map module-names source-file package-file)
  (define (package-name p)
    (match p
      (('define-public name _ ...) (symbol->string name))))

  (define (package-name<? p1 p2)
    (string<? (package-name p1) (package-name p2)))

  (define (source-name<? s1 s2)
    (string<? (project-name (car s1)) (project-name (car s2))))

  (define projects (android-manifest-projects manifest))
  (define (get-project path)
    (list
      (find (lambda (project)
              (equal? (project-name project) path))
            projects)))

  (define %header-source-paths
    (list "platform/frameworks/av"
          "platform/frameworks/base"
          "platform/frameworks/native"
          "platform/hardware/libhardware"
          "platform/hardware/libhardware_legacy"
          "platform/hardware/ril"
          "platform/libnativehelper"
          "platform/system/core"
          "platform/system/media"))

  (let loop ((packages '())
             (sources (map get-project %header-source-paths))
             (imported '())
             (dependencies (if (list? module-names)
                               module-names
                               (list module-names))))
    (format #t "Importing. Imported ~a packages, ~a left to consider (may contain duplicates).\r"
            (length imported) (length dependencies))
    (match dependencies
      (()
       (newline)
       (with-output-to-file source-file
         (lambda _
           (display (sources-header source-file))
           (for-each
             (lambda (source)
               (newline)
               (pretty-print (import-source manifest root source)))
             (uniq (sort sources source-name<?)
                   (lambda (s1 s2)
                     (equal? (project-name (car s1)) (project-name (car s2))))))))
       (with-output-to-file package-file
         (lambda _
           (display (packages-header source-file package-file))
           (for-each
             (lambda (p)
               (newline)
               (pretty-print p))
             (uniq (sort packages package-name<?)
                   (lambda (p1 p2)
                     (equal? (package-name p1) (package-name p2))))))))
      ((dep dependencies ...)
       (if (member dep imported)
           (loop packages sources imported dependencies)
           (catch #t
             (lambda _
               (let-values (((package source-name deps)
                             (import-package manifest root bp-map dep)))
                 (loop (cons package packages)
                       (cons source-name sources)
                       (cons dep imported)
                       (sort (append dependencies deps) string<?))))
             (lambda exception
               (report-error (G_ "failed to import ~a: ~a~%") dep exception))))))))
