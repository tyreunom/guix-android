;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android build soong-build-system)
  #:use-module (android build blueprint)
  #:use-module (android build soong)
  #:use-module (android build soong makefile)
  #:use-module ((guix build gnu-build-system) #:prefix gnu:)
  #:use-module (guix build utils)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:export (%standard-phases
            soong-build))

;; Commentary:
;;
;; Builder-side code of the standard soong build procedure.
;;
;; Code:

(define* (reset-include-path #:key inputs native-inputs target #:allow-other-keys)
  "When cross-compiling, ensure cross-libc headers are available"
  (let ((global-header-packages
         '("android-system-core-include"
           "android-system-logging-liblog-include"
           "android-system-media-audio-include"
           "android-hardware-libhardware-include"
           "android-hardware-libhardware-legacy-include" "android-hardware-ril-include"
           "android-frameworks-native-include" "android-frameworks-native-opengl-include"
           "android-frameworks-av-include" "android-libnativehelper-include")))
    (if target
        (begin
          (setenv "CPLUS_INCLUDE_PATH"
                  (string-append
                    (string-join
                      (map
                        (lambda (pkg)
                          (string-append (assoc-ref inputs pkg) "/include:"))
                        global-header-packages)
                      "")
                    (if (assoc-ref inputs "libcxx")
                        (string-append (pk (assoc-ref inputs "libcxx")) "/include/c++/v1:")
                        "")
                    (if (assoc-ref inputs "libcxxabi")
                        (string-append (pk (assoc-ref inputs "libcxxabi")) "/include:")
                        "")
                    (if (assoc-ref inputs "cross-libc")
                        (string-append (pk (assoc-ref inputs "cross-libc")) "/include:")
                        "")
                    (if (assoc-ref inputs "kernel-headers")
                        (string-append (assoc-ref inputs "kernel-headers") "/include")
                        "")))
          (setenv "C_INCLUDE_PATH"
                  (string-append
                    (string-join
                      (map
                        (lambda (pkg)
                          (string-append (assoc-ref inputs pkg) "/include:"))
                        global-header-packages)
                      "")
                    (if (assoc-ref inputs "cross-libc")
                        (string-append (pk (assoc-ref inputs "cross-libc")) "/include:")
                        "")
                    (if (assoc-ref inputs "kernel-headers")
                        (string-append (assoc-ref inputs "kernel-headers") "/include")
                        ""))))
        (begin
          (setenv "CPLUS_INCLUDE_PATH"
                  (string-append
                    (string-join
                      (map
                        (lambda (pkg)
                          (string-append (assoc-ref inputs pkg) "/include:"))
                        global-header-packages)
                      "")
                    (if (assoc-ref inputs "libcxx")
                        (string-append (assoc-ref inputs "libcxx") "/include/c++/v1:")
                        "")
                    (if (assoc-ref inputs "libcxxabi")
                        (string-append (assoc-ref inputs "libcxxabi") "/include:")
                        "")
                    (if (assoc-ref inputs "libc")
                        (string-append (assoc-ref inputs "libc") "/include:")
                        "")
                    (if (assoc-ref inputs "kernel-headers")
                        (string-append (assoc-ref inputs "kernel-headers") "/include")
                        "")))
          (setenv "C_INCLUDE_PATH"
                  (string-append
                    (string-join
                      (map
                        (lambda (pkg)
                          (string-append (assoc-ref inputs pkg) "/include:"))
                        global-header-packages)
                      "")
                    (if (assoc-ref inputs "libc")
                        (string-append (assoc-ref inputs "libc") "/include:")
                        "")
                    (if (assoc-ref inputs "kernel-headers")
                        (string-append (assoc-ref inputs "kernel-headers") "/include")
                        "")))))))

(define* (configure #:key inputs outputs blueprint module-type module base-package
                    system target #:allow-other-keys)
  "Create a Makefile that implements the build system for the given target."
  (define* (file->package root file #:optional (base "/"))
    (string-append
      base
      (substring
        (dirname file)
        (string-length root))))

  (let* ((module-name module)
         (dn (dirname blueprint))
         (module-package (string-append base-package (if (equal? dn ".")
                                                         ""
                                                         (string-append "/" dn))))
         (module (get-module
                   (append
                     (filter-map
                       (lambda (i)
                         (let ((bp (find
                                     (const #t)
                                     (find-files
                                       (string-append (cdr i) "/share/android")
                                       "^Android.bp$"))))
                           (and bp
                                (file-exists? bp)
                                (cons bp (file->package
                                           (string-append (cdr i) "/share/android")
                                           bp)))))
                       inputs)
                     (map
                       (lambda (file)
                         (cons file (file->package "." file base-package)))
                       (find-files "." "^Android\\.bp$")))
                   module-type module))
         (out (assoc-ref outputs "out"))
         (android (string-append out "/share/android" module-package)))
    (mkdir-p android)
    (generate-bp-file (string-append android "/Android.bp") module)
    (chdir (dirname (if (list? blueprint) (car blueprint) blueprint)))
    (let ((makefile (get-makefile (resolve-dep inputs) module module-type
                                  module-name system target)))
      (when (file-exists? "Makefile")
        (delete-file "Makefile"))
      (generate-soong-makefile
        (update-makefile makefile
          #:variables
          (cons (cons "PREFIX" out) (makefile-variables makefile)))
        #:name "Makefile"))))

(define %standard-phases
  ;; Everything is as with the GNU Build System except for the `configure'
  ;; , `build', `check' and `install' phases.
  (modify-phases gnu:%standard-phases
    (delete 'bootstrap)
    (add-before 'configure 'reset-include-path reset-include-path)
    (replace 'configure configure)
    (delete 'check)))

(define* (soong-build #:key inputs (phases %standard-phases)
                      #:allow-other-keys #:rest args)
  "Build the given package, applying all of PHASES in order."
  (apply gnu:gnu-build #:inputs inputs #:phases phases args))

;;; soong-build-system.scm ends here
