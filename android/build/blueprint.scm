;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020, 2021 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android build blueprint)
  #:use-module (ice-9 match)
  #:use-module (ice-9 peg)
  #:use-module (ice-9 textual-ports)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-11)
  #:export (%latest-android-10
            %latest-android-11
            %latest-android-12
            %latest-android-13

            parse-blueprint-file
            parse-tree->blueprint
            get-module
            get-module-with-map
            uniq
            define-module-record-type
            merge-alists
            merge-values
            generate-bp-file
            get-lookup-tables

            blueprint-module
            blueprint-module?
            make-blueprint-module
            blueprint-module-type
            blueprint-module-name
            blueprint-module-visibility
            blueprint-module-keys
            blueprint-module-defaults
            blueprint-module-file
            blueprint-module-package
            update-blueprint-module

            blueprint
            blueprint?
            make-blueprint
            blueprint-vars
            blueprint-modules

            android-name->guix-name))

(define (android-name->guix-name name)
  (string-join (string-split name #\_) "-"))

;; See https://source.android.com/setup/start/build-numbers#source-code-tags-and-builds
;; Updated once a month
(define %latest-android-10 "android-security-10.0.0_r71")
(define %latest-android-11 "android-security-11.0.0_r60")
(define %latest-android-12 "android-12.1.0_r27")
(define %latest-android-13 "android-13.0.0_r83")

(define-peg-pattern inline-comment none (and "//" (* COMMCHR) (? "\r") "\n"))
(define-peg-pattern multiline-comment all (and (ignore "/*") comment-part))
(define-peg-pattern comment-part body
                    (or (ignore (and (* "*") "/"))
                        (and (* "*") (+ comment-chr) comment-part)))
(define-peg-pattern comment-chr body (or "\t" "\n" (range #\ #\)) (range #\+ #\xffff)))
(define-peg-pattern comment none (or inline-comment multiline-comment))
(define-peg-pattern COMMCHR none
                    (or " " "!" "\\" "\"" "\t" (range #\# #\xffff)))
(define-peg-pattern COLON none ":")
(define-peg-pattern COMMA none ",")
(define-peg-pattern SP none (or " " "\n" "\r" "\t" comment))

(define-peg-pattern TRUE body "true")
(define-peg-pattern FALSE body "false")
(define-peg-pattern boolean all (or TRUE FALSE))
(define-peg-pattern integer all (+ (range #\0 #\9)))
(define-peg-pattern string-pat all (and (ignore "\"") (* string-chr) (ignore "\"")))
(define-peg-pattern string-chr body (or " " "!" (and (ignore "\\") "\"")
                                        (and (ignore "\\") "\\") (range #\# #\xffff)))
(define-peg-pattern string-or-var body (or string-pat var-name))
(define-peg-pattern string-all all (or (and string-or-var (* SP) (ignore "+") (* SP) string-all)
                                        string-or-var))
(define-peg-pattern simple-strings-list body
                    (and (ignore "[")
                         (* SP)
                         (? (and string-all (* (and (* SP) COMMA (* SP) string-all)) (? COMMA)))
                         (* SP)
                         (ignore "]")))
(define-peg-pattern strings-list-or-var all (or simple-strings-list var-name))
(define-peg-pattern complex-strings-list all
                    (and strings-list-or-var (* SP)
                         (+ (and (* SP) (ignore "+") (* SP) strings-list-or-var))))
(define-peg-pattern strings-list all (or complex-strings-list simple-strings-list))
(define-peg-pattern map-pat all (and (ignore "{")
                                     (* SP)
                                     (? (and keyval (* (and (* SP) COMMA (* SP) keyval)) (? COMMA)))
                                     (* SP)
                                     (ignore "}")))
(define-peg-pattern keyval all (and var-name (* SP) COLON (* SP) keyval-val))
(define-peg-pattern keyval-val all value-pat)
(define-peg-pattern value-pat body
                    (or boolean integer strings-list string-all map-pat var-name))
(define-peg-pattern module-pat all (and var-name (* SP) map-pat))
(define-peg-pattern assignment all (and var-name (* SP) (ignore "=")
                                        (* SP) value-pat))
(define-peg-pattern var-name body (+ (or "_" (range #\a #\z) (range #\A #\Z)
                                         (range #\0 #\9))))
(define-peg-pattern blueprint-pat body
                    (* (and (* SP) (or assignment module-pat) (* SP))))

(define (parse-blueprint-file file)
  (peg:tree (match-pattern blueprint-pat (call-with-input-file file get-string-all))))

(define-record-type blueprint-module
  (make-blueprint-module type name visibility keys defaults file package)
  blueprint-module?
  (type blueprint-module-type)
  (name blueprint-module-name)
  (visibility blueprint-module-visibility)
  (keys blueprint-module-keys)
  (defaults blueprint-module-defaults)
  (file blueprint-module-file)
  (package blueprint-module-package))

(define* (update-blueprint-module module #:key
                                  (type (blueprint-module-type module))
                                  (name (blueprint-module-name module))
                                  (visibility (blueprint-module-visibility module))
                                  (keys (blueprint-module-keys module))
                                  (defaults (blueprint-module-defaults module))
                                  (file (blueprint-module-file module))
                                  (package (blueprint-module-package module)))
  (make-blueprint-module type name visibility keys defaults file package))

(define-record-type blueprint
  (make-blueprint package vars modules)
  blueprint?
  (package blueprint-package)
  (vars    blueprint-vars)
  (modules blueprint-modules))

(define* (update-blueprint blueprint #:key
                           (package (blueprint-package blueprint))
                           (vars (blueprint-vars blueprint))
                           (modules (blueprint-modules blueprint)))
  (make-blueprint package vars modules))

(define (get-unique-module modules name)
  (match (filter (lambda (module) (equal? name (blueprint-module-name module)))
                 modules)
    ((module) module)
    (_ (throw 'no-unique-module modules name))))

(define (flatten l)
  (match l
    (() '())
    (((? list? l1) l ...)
     (append (flatten l1) (flatten l)))
    ((e1 l ...)
     (cons e1 (flatten l)))))

(define (flatten-map proc vals)
  (flatten (map proc vals)))

(define (alist-delete* lst keys)
  (match lst
    (() '())
    (((k . v) lst ...)
     (if (member k keys)
         (alist-delete* lst keys)
         (cons (cons k v) (alist-delete* lst keys))))))

(define (string->guix-symbol str)
  (string->symbol (string-join (string-split str #\_) "-")))

(define (guix-symbol->string sym)
  (string-join (string-split (symbol->string sym) #\-) "_"))

(define-record-type symbolic-string
  (make-symbolic-string content)
  symbolic-string?
  (content symbolic-string-content))

(define (parse-tree->value val)
  (define (parse-tree->val val)
    (match val
      ((? string? s)
       (symbol->keyword (string->symbol s)))
      (((? list? val) ...)
       (flatten-map parse-tree->val val))
      (('boolean "true") #t)
      (('boolean "false") #f)
      (('integer num) (string->number num))
      (('map-pat val ...)
       (parse-tree->keyvals val))
      ('map-pat '())
      (('string-pat val) val)
      ('string-pat "")
      (('strings-list-or-var c)
       (parse-tree->val c))
      (('strings-list-or-var c ...)
       (parse-tree->val c))
      ('strings-list-or-var "")
      (('strings-list val ...)
       (flatten-map parse-tree->val val))
      (('complex-strings-list val ...)
       (flatten-map parse-tree->val val))
      ('strings-list '())
      (('string-all val)
       (parse-tree->val val))
      (('string-all val ...)
       (make-symbolic-string (map parse-tree->val val)))))

  (parse-tree->val val))

(define (parse-tree->keyvals tree)
  (match tree
    (() '())
    ((('keyval key ('keyval-val val)) tree ...)
     (cons `(,(string->guix-symbol key) . ,(parse-tree->value val))
           (parse-tree->keyvals tree)))
    (((tree ...))
     (parse-tree->keyvals tree))))

(define (file->package root file)
  (string-append "/" (substring (dirname file) (string-length root))))

(define (parse-tree->blueprint tree file package-name)
  (let loop ((tree tree) (package #f) (vars '()) (modules '()))
    (match tree
      (() (make-blueprint package vars modules))
      (('module-pat type 'map-pat)
       (if (equal? type "package")
           (loop '() (make-blueprint-module type #f #f '() '() file package-name) vars modules)
           (loop '() package vars
                 (cons (make-blueprint-module type #f #f '() '() file package-name) modules))))
      ((('module-pat type 'map-pat) tree ...)
       (if (equal? type "package")
           (loop tree (make-blueprint-module type #f #f '() '() file package-name)
                 vars modules)
           (loop tree package vars
                 (cons (make-blueprint-module type #f #f '() '() file package-name) modules))))
      (('module-pat type ('map-pat keyvals ...))
       ;; hit the next rule
       (loop (list tree) package vars modules))
      ((('module-pat type ('map-pat keyvals ...)) tree ...)
       (let* ((keyvals (parse-tree->keyvals keyvals))
              (name (assoc-ref keyvals 'name))
              (visibility (assoc-ref keyvals 'visibility))
              (defaults (or (assoc-ref keyvals 'defaults) '()))
              (keyvals (alist-delete* keyvals '(name defaults))))
         (if (equal? type "package")
             (loop tree (make-blueprint-module type name visibility keyvals defaults file package-name)
                   vars modules)
             (loop tree package vars
                   (cons (make-blueprint-module
                           type name visibility keyvals defaults file package-name)
                         modules)))))
      (('assignment var val)
       (loop '() package (cons (cons var (parse-tree->value val)) vars)
             modules))
      ((('assignment var val) tree ...)
       (loop tree package (cons (cons var (parse-tree->value val)) vars)
             modules)))))

(define* (get-lookup-tables files
                            #:key
                            (exclude-types '("ndk_library" "llndk_library")))
  "Return lookup tables as two values.  The first value is a hash table from
package name to blueprint record, the second value is a hash table from module
name to package names that contain a module of that name."
  (define total (length files))

  (define bp-table (make-hash-table (length files)))
  (define module-table (make-hash-table (* (length files) 10)))

  (define (update-modules h k v)
    (hash-set! h k (cons v (or (hash-ref h k) '()))))
  (define (update-packages h k v)
    (let ((old (hash-ref h k)))
      (hash-set!
        h
        k
        (if old
            (make-blueprint
              (or (blueprint-package old) (blueprint-package v))
              (append (blueprint-vars old) (blueprint-vars v))
              (append (blueprint-modules old) (blueprint-modules v)))
            v))))

  (let loop ((files files))
    (match files
      (() (values bp-table module-table))
      (((file . package) files ...)
       (let* ((bp (parse-tree->blueprint (parse-blueprint-file file) file package))
              (files (append (map
                               (lambda (included-file)
                                 (let* ((file (string-append (dirname file) "/" included-file))
                                        (dn (dirname included-file)))
                                   (cons file
                                         (if (equal? dn ".")
                                             package
                                             (string-append package "/" dn)))))
                               (or (assoc-ref (blueprint-vars bp) "build") '()))
                             files))
              (modules
                (filter
                   (lambda (m)
                     (not (member (blueprint-module-type m) exclude-types)))
                   (blueprint-modules bp))))
         (for-each
           (lambda (m) (update-modules
                         module-table
                         (blueprint-module-name m)
                         (blueprint-module-package m)))
           modules)
         (update-packages bp-table package bp)
         (loop files)))
      ((file files ...)
       (throw 'first-is-wrong file)))))

(define* (find-module bp-table module-table type name
                      #:key (exclude-types '("ndk_library" "llndk_library")))
  "Return a list of modules that have a given @var{type} and @var{name}.
@var{bp-table} and @var{module-table} are hash tables returned by
@code{get-lookup-tables}."
  (filter (lambda (m)
            (and (or (and (not type)
                          (not (member (blueprint-module-type m) exclude-types)))
                     (equal? (blueprint-module-type m) type))
                 (equal? (blueprint-module-name m) name)))
          (append-map (lambda (p) (blueprint-modules (hash-ref bp-table p)))
                      (or (hash-ref module-table name) (throw 'not-found name)))))

(define* (uniq lst #:optional (equal? equal?))
  (define (member e lst)
    (let loop ((lst lst))
      (match lst
        (() #f)
        ((el lst ...) (if (equal? e el) #t (loop lst))))))

  (let loop ((lst lst) (ans '()))
    (match lst
      (() (reverse ans))
      ((e lst ...)
       (if (member e ans)
           (loop lst ans)
           (loop lst (cons e ans)))))))

(define (has-key? keyvals key)
  (match keyvals
    (((k . _) keyvals ...)
     (if (equal? k key)
         #t
         (has-key? keyvals key)))
    (() #f)))

(define (prepend-keyvals keyvals defaults)
  "Prepend @val{defaults} key-values to @var{keyvals} when possible. Augment
@var{keyvals} when key does not exist, prepend when possible or ignore when
not possible."
  (define (prepend-vals val previous)
    (match previous
      ('false val)
      (#f previous)
      (#t previous)
      (() val)
      ((? string? _) previous)
      ((? list? _)
       (if (string? (car previous))
           (uniq (append val previous))
           (prepend-keyvals previous val)))))

  (match defaults
    (() keyvals)
    (((key . val) defaults ...)
     (let* ((previous (assoc-ref keyvals key))
            (previous (if (has-key? keyvals key) previous 'false)))
       (prepend-keyvals
         (cons (cons key (prepend-vals val previous))
               keyvals)
         defaults)))))

(define (expand-module! package module type bp-table module-table)
  "Expand a module identified by its @var{package} name and @var{module} name.
Updates @var{bp-table}, a hash table of package name to blueprint record.
@var{module-table} is a hash table of module name to list of package names that
contain a module of that name.  Returns the expanded module.

Expansion is performed in three steps:

@enumerate
@item Variable exansion.
@item Defaults expansion.
@item Visibility expansion.
@end enumerate

Variables are declared in packages and are accessible in that package and all
subpackages.  Therefore, variable expansion runs variable lookup when it
encounters a variable reference, by looking for that variable in the current
blueprint record, or in the blueprint records of its parent packages.

Defaults can be declared in any package, so the @{module-table} is used to find
candidates.  Candidates are recursively expanded to determine their content and
visibility.  After this process, one of the candidates is selected based on
visibility.

Visibility is updated in the blueprint-module record based on the visibility or
defaults_visibility field depending on the type of module."

  (define* (find-parent bp #:optional (throw? #f))
    "Return the blueprint record associated with the first parent of the given
@var{bp}, a blueprint record.  Set @var{throw?} to #t if you want to throw an
exception, or to #f if you want to return #f when there is no parent."
    (let loop ((dir (dirname (blueprint-module-package (first (blueprint-modules bp))))))
      (if (equal? dir "/")
          (if throw? (throw 'no-parent) #f)
          (or (hash-ref bp-table dir) (loop (dirname dir))))))

  (define (get-variables bp)
    "Return the list of variables defined in @var{bp}, a blueprint record, and
its parents."
    (let ((vars (blueprint-vars bp))
          (parent (find-parent bp)))
      (if parent
          (append vars (get-variables parent))
          vars)))

  (define (expand-variables-val variables val)
    "Return @var{val}, expanded to replace all variable references with their
value."
    (match val
      ((? keyword? var) ; a variable reference
       (let ((val (assoc-ref variables (symbol->string (keyword->symbol var)))))
         (if val
             (expand-variables-val variables val)
             (throw 'no-such-variable var 'in variables))))
      (($ symbolic-string s) ; a symbolic string: a list of strings or values
       (let loop ((s s) (res ""))
         (match s
           (() res)
           ((v s ...)
            (loop s (string-append res (expand-variables-val variables v)))))))
      ((? list? l) ; a list of values
       (let loop ((l l) (res '()))
         (match l
           (() (reverse res))
           ;; a variable may expand to multiple values, so we need to treat them
           ;; specially to preserve order in the end result.
           (((? keyword? var) l ...)
            (let ((r (expand-variables-val variables var)))
              (loop l (append (reverse r) res))))
           ((v l ...)
            (loop l (cons (expand-variables-val variables v) res))))))
      ((k . v) ; a pair that is not a list
       (cons k (expand-variables-val variables v)))
      (_ val)))

  (define (expand-variables module-record)
    "First step of the process.  Returns the expanded @var{module-record}."
    (let* ((variables (get-variables (hash-ref bp-table (blueprint-module-package module-record))))
           (keyvals (blueprint-module-keys module-record))
           (keyvals (map (match-lambda ((k . v) (cons k (expand-variables-val variables v)))) keyvals)))
      (update-blueprint-module module-record #:keys keyvals)))

  (define (expand-defaults module-record)
    "Second step of the process.  Returns the expanded @var{module-record}."
    (let loop ((defaults (blueprint-module-defaults module-record))
               (keyvals (blueprint-module-keys module-record)))
      (match defaults
        (() (update-blueprint-module module-record #:defaults '() #:keys keyvals))
        ((default defaults ...)
         (let* ((candidates (map
                              (lambda (package)
                                (expand-module! package default #f bp-table module-table))
                              (hash-ref module-table default)))
                (candidates (filter (lambda (m) (is-visible? m package)) candidates)))
           (match candidates
             (() (throw 'no-visible-default-module-for default))
             ((c) (loop defaults (prepend-keyvals keyvals (blueprint-module-keys c))))
             (_ (throw 'too-many-candidates (map blueprint-module-package candidates)))))))))

  (define (package-visibility package)
    "Return the default visibility defined by this @var{package} or its parents."

    (let loop ((bp (hash-ref bp-table package)))
      (let ((module (first (blueprint-modules bp))))
        (or
          (assoc-ref (blueprint-module-keys module) 'default-visibility)
          (let ((parent (find-parent bp)))
            (if parent
                (loop (find-parent bp))
                '("//visibility:legacy_public")))))))

  (define (default-module? mod)
    "Returns whether a module is a default module, that is used to store default
values, and not used for building by itself."
    (string-suffix? "_defaults" (blueprint-module-type mod)))

  (define (expand-visibility module-record)
    "Last step of the process.  Returns the expanded @var{module-record}."
    (let ((visibility (append
                        (package-visibility (blueprint-module-package module-record))
                        (or (assoc-ref (blueprint-module-keys module-record)
                                       (if (default-module? module-record)
                                           'defaults-visibility
                                           'visibility))
                            '()))))
      (update-blueprint-module module-record
        #:visibility visibility)))

  (define (update-modules modules module)
    (match modules
      (() (list module))
      ((m modules ...)
       (if (equal? (blueprint-module-name m) (blueprint-module-name module))
         (cons module modules)
         (cons m (update-modules modules module))))))

  (let* ((bp (hash-ref bp-table package))
         (module (find
                   (lambda (m) (and
                                 (or (not type)
                                     (equal? (blueprint-module-type m) type))
                                 (equal? (blueprint-module-name m) module)))
                   (blueprint-modules bp)))
         (module (expand-variables module))
         (module (expand-defaults module))
         (module (expand-visibility module))
         (bp (update-blueprint bp #:modules (update-modules (blueprint-modules bp) module))))
    (hash-set! bp-table package bp)
    module))

(define (is-visible? module dependent-package)
  (if dependent-package
      (let ((visibility (blueprint-module-visibility module)))
        (let loop ((visible 'default) (visibilities visibility))
          (match visibilities
            ('() (if (equal? visible 'default)
                     #t
                     visible))
            (("//visibility:public" visibilities ...)
             (loop #t visibilities))
            (("//visibility:legacy_public" visibilities ...)
             (loop #t visibilities))
            (("//visibility:private" visibilities ...)
             (loop (equal? dependent-package (blueprint-module-package module)) visibilities))
            ((":__subpackages__" visibilities ...)
             (loop (or (equal? visible #t)
                       (string-prefix? (blueprint-module-package module) dependent-package))
                   visibilities))
            (("//visibility:override" visibilities ...)
             (loop 'visible visibilities))
            ((visibility visibilities ...)
             (match (string-split visibility #\:)
              ((or (pkg) (pkg "__pkg__"))
               (loop (or (equal? visible #t) (equal? dependent-package pkg)) visibilities))
              ((pkg "__subpackages__")
               (loop (or (equal? visible #t) (string-prefix? pkg dependent-package)) visibilities)))))))
      #t))

(define (get-module-with-map bp-table module-table module-type module-name dependent-package)
  "Return the module @var{module-name} of type @var{module-type} in the project.
@var{bp-table} and @var{module-table} are hash table produced by procedure
@code{get-lookup-table}.  To avoid confusion, only selects module visible from
@var{dependent-package} unless it is @code{#f}."
  (let ((candidates (find-module bp-table module-table module-type module-name)))
    (unless (and candidates (not (equal? candidates '())))
      (throw 'module-not-found module-name))
    (let* ((candidates (map
                         (lambda (m)
                           (expand-module!
                             (blueprint-module-package m)
                             module-name
                             (blueprint-module-type m)
                             bp-table
                             module-table))
                         candidates)))
      (match (filter (lambda (module) (is-visible? module dependent-package))
                     candidates)
        ('() (throw 'module-not-visible module-name 'in (map blueprint-module-package candidates) 'for dependent-package))
        ((module) module)
        (_
         (throw 'multiple-modules module-name 'in (map blueprint-module-package candidates)))))))

(define (get-module bp-files module-type module-name)
  "Return the module @var{module-name} of type @var{module-type} in
@var{bp-files}, a list of pairs containing an Android.bp file name and the
corresponding package name.  This procedure takes care of parsing the files and
returns a @code{blueprint-module} object."
  (let-values (((bp-table module-table) (get-lookup-tables bp-files)))
    (get-module-with-map bp-table module-table module-type module-name #f)))

(define (generate-bp-file file module)
  (define (serialize-value value indent)
    (match value
      (#t "true")
      (#f "false")
      ((? number? n) (number->string n))
      ((? string? s) (string-append "\"" (string-join (string-split s #\") "\\\"") "\""))
      (((? string? s) ...)
       (string-append "[\n"
                      (apply string-append
                             (map
                               (lambda (s)
                                 (string-append
                                   indent "  "
                                   (serialize-value s (string-append indent "  "))
                                   ",\n"))
                               s))
                      indent "]"))
      ((? list? alist)
       (string-append indent "{\n"
                      (apply string-append
                             (map
                               (match-lambda
                                 ((key . val)
                                  (string-append
                                    "  " indent (guix-symbol->string key)
                                    ": " (serialize-value
                                           val (string-append indent "  "))
                                    ",\n")))
                               alist))
                      indent "}"))))

  (define (serialize-module module)
    (match module
      (($ blueprint-module type name visibility keys defaults file package)
       (display (string-append type " "))
       (display (serialize-value (cons (cons 'name name) keys) "  "))
       (display "\n"))))

  (with-output-to-file file
    (lambda _
      (serialize-module module))))

(define* (syntax-append x . s)
  (define (->symbol s)
    (if (symbol? s) s (syntax->datum s)))
  (datum->syntax x (apply symbol-append (map ->symbol s))))

(define (merge-values v1 v2)
  (if (and (list? v1) (list? v2))
      (match v1
        ((((? symbol? _) . _) ...)
         (merge-alists v1 v2))
        (_ (uniq (append v1 v2))))
      (or v1 v2)))

(define (update-alist k v a)
  (match a
    (() `((,k . ,v)))
    (((key . val) a ...)
     (if (equal? key k)
         (cons (cons k (merge-values v val)) a)
         (cons (cons key val) (update-alist k v a))))))

(define (merge-alists a1 a2)
  (match a1
    (() a2)
    (((k . v) a1 ...)
     (merge-alists a1 (update-alist k v a2)))))

(define-syntax define-module-record-type
  (lambda (x)
    (define (specs->names specs)
      (map
        (lambda (s)
          (syntax-case s ()
            ((name _ ...) #'name)))
        specs))

    (define (specs->name-list specs)
      (map
        (lambda (s)
          (syntax-case s ()
            ((name _ ...) #`(quote name))))
        specs))

    (define (specs->field specs)
      (map
        (lambda (s)
          (syntax-case s ()
            ((name accessor _ ...)
             #`(name accessor))))
        specs))

    (define (specs->values keys specs)
      (map
        (lambda (s)
          (syntax-case s ()
            ((name _ (_ d))
             #`(or (assoc-ref keys (quote name)) d))))
        specs))

    (define (specs->updater alist config specs)
      (map
        (lambda (s)
          (syntax-case s ()
            ((name accessor _)
             #`(let ((new (assoc-ref alist (quote name)))
                     (old (accessor config)))
                 (merge-values new old)))))
        specs))

    (syntax-case x ()
      ((_ name make predicate updater field-spec ...)
       #`(begin
          (define-record-type name
            (make #,@(specs->names #'(field-spec ...)))
            predicate
            #,@(specs->field #'(field-spec ...)))

          (define (updater config alist)
            (make
              #,@(specs->updater #'alist #'config #'(field-spec ...))))

          (define (#,(syntax-append x 'blueprint-module-> #'name) module)
            (#,(syntax-append x 'keyvals-> #'name) (blueprint-module-keys module)))
          (define (#,(syntax-append x 'keyvals-> #'name) keys)
            (for-each
              (match-lambda
                ((key . value)
                 (unless (member key (list #,@(specs->name-list #'(field-spec ...))))
                   (throw 'unknown-key key))))
              keys)

              (make #,@(specs->values #'keys #'(field-spec ...)))))))))
