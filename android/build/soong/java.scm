;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android build soong java)
  #:use-module (android build blueprint)
  #:use-module (android build soong)
  #:use-module (android build soong makefile)
  #:use-module (android build soong proto)
  #:use-module (guix build utils)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (blueprint-module->java-config
            generate-java-makefile

            java-config
            make-java-config
            java-config?
            java-config-srcs
            java-config-exclude-srcs
            java-config-host-supported
            java-config-device-supported
            java-config-aidl
            java-config-apex-available
            java-config-arch
            java-config-common-srcs
            java-config-compile-dex
            java-config-compile-multilib
            java-config-debug-ramdisk
            java-config-device-specific
            java-config-dex-preopt
            java-config-dist
            java-config-dists
            java-config-dxflags
            java-config-enabled
            java-config-enforce-uses-libs
            java-config-errorprone
            java-config-exclude-java-resource-dirs
            java-config-exclude-java-resources
            java-config-exported-plugins
            java-config-hiddenapi-additional-annotations
            java-config-host-required
            java-config-hostdex
            java-config-include-srcs
            java-config-init-rc
            java-config-installable
            java-config-jacoco
            java-config-jarjar-rules
            java-config-java-resource-dirs
            java-config-java-resources
            java-config-java-version
            java-config-javac-shard-size
            java-config-javacflags
            java-config-jni-libs
            java-config-kotlincflags
            java-config-libs
            java-config-licenses
            java-config-lint
            java-config-manifest
            java-config-min-sdk-version
            java-config-multilib
            java-config-native-bridge-supported
            java-config-notice
            java-config-openjdk9
            java-config-optimize
            java-config-optional-uses-libs
            java-config-owner
            java-config-patch-module
            java-config-permitted-packages
            java-config-platform-apis
            java-config-plugins
            java-config-product-specific
            java-config-product-variables
            java-config-proprietary
            java-config-proto
            java-config-provides-uses-lib
            java-config-ramdisk
            java-config-recovery
            java-config-required
            java-config-sdk-member-name
            java-config-sdk-version
            java-config-services
            java-config-soc-specific
            java-config-static-kotlin-stdlib
            java-config-static-libs
            java-config-stem
            java-config-system-ext-specific
            java-config-system-modules
            java-config-target
            java-config-target.host
            java-config-target.hostdex
            java-config-target-required
            java-config-target-sdk-version
            java-config-uncompress-dex
            java-config-use-tools-jar
            java-config-uses-libs
            java-config-v4-signature
            java-config-vendor
            java-config-vendor-ramdisk
            java-config-vintf-fragments
            java-config-wrapper
            java-config-visibility))

(define-module-record-type java-config
  make-java-config java-config? extend-java-config

  (srcs java-config-srcs (default '()))
  (exclude-srcs java-config-exclude-srcs (default '()))
  (host-supported java-config-host-supported (default #f))
  (device-supported java-config-device-supported (default #f))
  (aidl java-config-aidl (default #f))
  (apex-available java-config-apex-available (default #f))
  (arch java-config-arch (default #f))
  (common-srcs java-config-common-srcs (default '()))
  (compile-dex java-config-compile-dex (default #f))
  (compile-multilib java-config-compile-multilib (default #f))
  (debug-ramdisk java-config-debug-ramdisk (default #f))
  (device-specific java-config-device-specific (default #f))
  (dex-preopt java-config-dex-preopt (default #f))
  (dist java-config-dist (default #f))
  (dists java-config-dists (default #f))
  (dxflags java-config-dxflags (default '()))
  (enabled java-config-enabled (default #t))
  (enforce-uses-libs java-config-enforce-uses-libs (default #f))
  (errorprone java-config-errorprone (default #f))
  (exclude-java-resource-dirs java-config-exclude-java-resource-dirs (default '()))
  (exclude-java-resources java-config-exclude-java-resources (default '()))
  (exported-plugins java-config-exported-plugins (default #f))
  (hiddenapi-additional-annotations java-config-hiddenapi-additional-annotations
                                    (default #f))
  (host-required java-config-host-required (default #f))
  (hostdex java-config-hostdex (default #f))
  (include-srcs java-config-include-srcs (default '()))
  (init-rc java-config-init-rc (default #f))
  (installable java-config-installable (default #f))
  (jacoco java-config-jacoco (default #f))
  (jarjar-rules java-config-jarjar-rules (default #f))
  (java-resource-dirs java-config-java-resource-dirs (default '()))
  (java-resources java-config-java-resources (default '()))
  (java-version java-config-java-version (default "1.9"))
  (javac-shard-size java-config-javac-shard-size (default #f))
  (javacflags java-config-javacflags (default '()))
  (jni-libs java-config-jni-libs (default '()))
  (kotlincflags java-config-kotlincflags (default '()))
  (libs java-config-libs (default '()))
  (licenses java-config-licenses (default #f))
  (lint java-config-lint (default #f))
  (manifest java-config-manifest (default #f))
  (min-sdk-version java-config-min-sdk-version (default #f))
  (multilib java-config-multilib (default #f))
  (native-bridge-supported java-config-native-bridge-supported (default #f))
  (notice java-config-notice (default #f))
  (openjdk9 java-config-openjdk9 (default #f))
  (optimize java-config-optimize (default #f))
  (optional-uses-libs java-config-optional-uses-libs (default #f))
  (owner java-config-owner (default #f))
  (patch-module java-config-patch-module (default #f))
  (permitted-packages java-config-permitted-packages (default #f))
  (platform-apis java-config-platform-apis (default #f))
  (plugins java-config-plugins (default #f))
  (product-specific java-config-product-specific (default #f))
  (product-variables java-config-product-variables (default '()))
  (proprietary java-config-proprietary (default #f))
  (proto java-config-proto (default '()))
  (provides-uses-lib java-config-provides-uses-lib (default #f))
  (ramdisk java-config-ramdisk (default #f))
  (recovery java-config-recovery (default #f))
  (required java-config-required (default #f))
  (sdk-member-name java-config-sdk-member-name (default #f))
  (sdk-version java-config-sdk-version (default #f))
  (services java-config-services (default #f))
  (soc-specific java-config-soc-specific (default #f))
  (static-kotlin-stdlib java-config-static-kotlin-stdlib (default #f))
  (static-libs java-config-static-libs (default '()))
  (stem java-config-stem (default #f))
  (system-ext-specific java-config-system-ext-specific (default #f))
  (system-modules java-config-system-modules (default #f))
  (target java-config-target (default #f))
  (target.host java-config-target.host (default #f))
  (target.hostdex java-config-target.hostdex (default #f))
  (target-required java-config-target-required (default #f))
  (target-sdk-version java-config-target-sdk-version (default #f))
  (uncompress-dex java-config-uncompress-dex (default #f))
  (use-tools-jar java-config-use-tools-jar (default #f))
  (uses-libs java-config-uses-libs (default #f))
  (v4-signature java-config-v4-signature (default #f))
  (vendor java-config-vendor (default #f))
  (vendor-ramdisk java-config-vendor-ramdisk (default #f))
  (vintf-fragments java-config-vintf-fragments (default #f))
  (wrapper java-config-wrapper (default #f))

  ;; XXX: Currently ignored, until it bites us.  Probably should have a
  ;; more global effect in blueprint.scm.
  (visibility java-config-visibility (default #f)))

(define (extend-java-with-proto java-conf proto-conf get-input)
  (let* ((lib (and (not (proto-config-plugin proto-conf))
                   (match (proto-config-type proto-conf)
                     ;; TODO: when building the SDK (ctx.useSdk), full and lite
                     ;; cases are different.
                     ("nano" "libprotobuf-java-nano")
                     ("micro" "libprotobuf-java-micro")
                     ((or "lite" #f) "libprotobuf-java-lite")
                     ("full" "libprotobuf-java-full"))))
         (srcs (expand-srcs (java-config-srcs java-conf)
                            (java-config-exclude-srcs java-conf)
                            get-input)))
    (if (null? (filter (lambda (file) (string-suffix? ".proto" file)) srcs))
        java-conf
        (extend-java-config java-conf `((static-libs . ,(list lib)))))))

;; Various flags set in soong/java/config/config.go.
(define javac-heap-flags
  '("-J-Xmx2048M"))
(define javac-vm-flags
  '("-J-XX:OnError=cat\\ hs_err_pid%p.log" "-J-XX:CICompilerCount=6"
    "-J-XX:+UseDynamicNumberOfGCThreads"))
(define java-vm-flags
  '("-XX:OnError=cat\\ hs_err_pid%p.log" "-XX:CICompilerCount=6"
    "-XX:+UseDynamicNumberOfGCThreads"))
(define common-jdk-flags
  '("-Xmaxerrs" "9999999"  "-encoding" "UTF-8"  "-sourcepath" "" "-g"
    ;; Turbine leaves out bridges which can cause javac to unnecessarily insert them into
    ;; subclasses (b/65645120).  Setting this flag causes our custom javac to assume that
    ;; the missing bridges will exist at runtime and not recreate them in subclasses.
    ;; If a different javac is used the flag will be ignored and extra bridges will be inserted.
    ;; The flag is implemented by https://android-review.googlesource.com/c/486427
    "-XDskipDuplicateBridges=true"

    ;; b/65004097: prevent using java.lang.invoke.StringConcatFactory when using -target 1.9
    "-XDstringConcat=inline"))

;; Individual build rules. See soong/java/builder.go.

(define (generate-rules proto-config config get-input)
  (let ((srcs (expand-srcs (java-config-srcs config)
                           (java-config-exclude-srcs config)
                           get-input)))
    (flatten
      (filter-map
        (lambda (file)
          (let ((suffix (match (reverse (string-split file #\.))
                        ((ext . rest) ext))))
            (match suffix
              ("java" #f)
              ("proto" (generate-java-proto-rule proto-config file)))))
        srcs))))

(define (all-sources srcs proto-conf)
  (flatten
    (map
      (lambda (src)
        (let ((suffix (match (reverse (string-split src #\.))
                      ((ext . rest) ext))))
          (match suffix
            ("java" src)
            ("proto" (proto-java-generated-source proto-conf src)))))
      srcs)))

(define (rsp-rule config get-input)
  (let* ((srcs (expand-srcs (java-config-srcs config)
                            (java-config-exclude-srcs config)
                            get-input))
         (java-srcs (filter (lambda (file) (string-suffix? ".java" file)) srcs)))
    (make-makefile-rule
      '("out.rsp")
      srcs
      (flatten
        (list
          "echo" java-srcs "`find gen/proto -name '*.java'`" ">out.rsp"))
      #f
      '())))

(define (javac-rule config get-input)
  (let* ((static-libs (java-config-static-libs config))
         (resource-dirs (java-config-java-resource-dirs config))
         (libs (java-config-libs config))
         (src-deps (filter-map
                     (lambda (src)
                       (and (string-prefix? ":" src) (substring src 1)))
                     (java-config-srcs config)))
         (classpath
           (map
             (lambda (lib)
               (string-append (get-input lib) "/share/java/" lib ".jar"))
             (append static-libs libs))))
    (make-makefile-rule
      '("classes")
      '("out.rsp")
      (append
        (list (list "mkdir" "-p" "build/classes"))
        (map
          (lambda (resource-dir)
            (list "cp" "-r" (string-append resource-dir "/*") "build/classes"))
          resource-dirs)
        (map
          (lambda (lib)
            (list "cd build/classes;"
                  "unzip" (string-append (get-input lib) "/share/java/"
                                         lib ".jar")))
          static-libs)
        (if (java-config-manifest config)
            `(("mkdir" "-p" "build/classes/META-INF")
              ("cp" ,(java-config-manifest config) "build/classes/META-INF/MANIFEST.MF"))
            '())
        (list
          (flatten
            (if (null? (java-config-srcs config))
                '()
                (list "javac" javac-heap-flags javac-vm-flags common-jdk-flags
                      (java-config-javacflags config)
                      (if (null? classpath)
                          '()
                          (list "-cp" (string-join classpath ":")))
                      "-source" (java-config-java-version config)
                      "-target" (java-config-java-version config)
                      "-d" "build/classes"
                      "@out.rsp")))))
      #f
      (append static-libs libs src-deps))))

(define (jar-rule config name)
  (let ((jar (string-append "build/jar/" name ".jar"))
        (manifest "build/classes/META-INF/MANIFEST.MF"))
    (make-makefile-rule
      (list (string-append "build/jar/" name ".jar"))
      '("classes")
      (list
        (list "for f in `find build/classes`; do touch -mt 198101010000 $$f; done")
        (flatten
          (list "if [ -f " manifest " ]"
                "; then" "cd build/classes;" "zip" "-0" "-r" "-X" "out.jar" manifest "*"
                "; else" "cd build/classes;" "zip" "-0" "-r" "-X" "out.jar" "*" "; fi"))
        (list "mv" "build/classes/out.jar" jar))
      #f
      '())))

(define (fix-jni-libs-rule get-input wrapper libs)
  (let ((libpath (string-join (map (lambda (lib)
                                     (string-append (get-input lib) "/lib"))
                                   libs)
                              ":")))
    (make-makefile-rule
      (list "wrapper")
      (list wrapper)
      (list "sed" "-i" (string-append "'s#java #java -Djava.library.path=\""
                                      libpath "\"#'")
            wrapper)
      #f
      libs)))

(define* (generate-java-makefile
           java-config
           #:key get-input module-type name system target
           #:allow-other-keys)
  (let* ((proto-conf (java-config-proto java-config))
         (proto-conf (keyvals->proto-config proto-conf))
         (proto-conf (extend-proto-with-defaults proto-conf))
         (proto-conf (extend-proto-with-java proto-conf get-input))
         (java-config (extend-java-with-proto java-config proto-conf get-input)))
    (let ((build-rules
            (append
              (list (rsp-rule java-config get-input))
              (generate-rules proto-conf java-config get-input)
              (if (and (java-config-wrapper java-config)
                       (java-config-jni-libs java-config))
                  (list (fix-jni-libs-rule get-input
                                           (java-config-wrapper java-config)
                                           (java-config-jni-libs java-config)))
                  '())
              (list (javac-rule java-config get-input))
              (list (jar-rule java-config name))))
          (install-rules
            (match module-type
              ((or "java_binary_host" "java_binary")
               `((file ,(string-append "build/jar/" name ".jar") "bin" ())
                 ,@(if (java-config-wrapper java-config)
                       `((executable ,(java-config-wrapper java-config) "bin" ()))
                       '())))
              (_ `((file ,(string-append "build/jar/" name ".jar") "share/java" ()))))))
      (make-makefile '() build-rules install-rules))))
