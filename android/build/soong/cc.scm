;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020, 2021 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android build soong cc)
  #:use-module (android build blueprint)
  #:use-module (android build soong)
  #:use-module (android build soong makefile)
  #:use-module (android build soong proto)
  #:use-module (guix build utils)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (blueprint-module->cc-config
            generate-cc-makefile

            cc-config
            make-cc-config
            cc-config?

            cc-config-srcs
            cc-config-exclude-srcs
            cc-config-objs
            cc-config-cflags
            cc-config-clang-cflags
            cc-config-conlyflags
            cc-config-cppflags
            cc-config-ldflags
            cc-config-asflags
            cc-config-lex
            cc-config-yacc
            cc-config-aidlflags
            cc-config-rtti
            cc-config-enabled

            cc-config-include-build-directory
            cc-config-include-dirs
            cc-config-local-include-dirs
            cc-config-export-include-dirs
            cc-config-export-system-include-dirs
            cc-config-generated-sources

            cc-config-header-libs
            cc-config-shared-libs
            cc-config-system-shared-libs
            cc-config-static-libs
            cc-config-whole-static-libs
            cc-config-generated-headers
            cc-config-group-static-libs

            cc-config-export-header-lib-headers
            cc-config-export-shared-lib-headers
            cc-config-export-static-lib-headers
            cc-config-export-generated-headers

            cc-config-static
            cc-config-shared
            cc-config-vndk
            cc-config-llndk
            cc-config-sanitize
            cc-config-afdo
            cc-config-lto
            cc-config-pack-relocations
            cc-config-nocrt
            cc-config-native-coverage
            cc-config-no-libcrt

            cc-config-bazel-module
            cc-config-version-script

            cc-config-use-version-lib
            cc-config-gnu-extensions
            cc-config-c-std
            cc-config-cpp-std

            cc-config-proto

            cc-config-target
            cc-config-arch
            cc-config-codegen
            cc-config-soong-config-variables
            cc-config-compile-multilib
            cc-config-multilib

            cc-config-host-supported

            cc-config-required
            cc-config-min-sdk-version
            cc-config-sdk-version
            cc-config-stl
            cc-config-stubs
            cc-config-logtags
            cc-config-unique-host-soname
            cc-config-static-ndk-lib
            cc-config-double-loadable

            cc-config-tidy
            cc-config-tidy-flags
            cc-config-tidy-checks
            cc-config-tidy-checks-as-errors
            cc-config-tidy-timeout-srcs

            cc-config-device-supported
            cc-config-recovery-available
            cc-config-vendor-available
            cc-config-vendor-ramdisk-available
            cc-config-ramdisk-available
            cc-config-apex-available
            cc-config-product-available
            cc-config-dist
            cc-config-pgo
            cc-config-visibility
            cc-config-defaults-visibility
            cc-config-product-variables

            cc-config-fuzz-config
            cc-config-header-abi-checker))

(define-module-record-type cc-config
  make-cc-config cc-config? extend-cc-config

  (srcs cc-config-srcs (default '()))
  (exclude-srcs cc-config-exclude-srcs (default '()))
  (objs cc-config-objs (default '()))

  (cflags cc-config-cflags (default '()))
  (clang-cflags cc-config-clang-cflags (default '()))
  (conlyflags cc-config-conlyflags (default '()))
  (cppflags cc-config-cppflags (default '()))
  (ldflags cc-config-ldflags (default '()))
  (asflags cc-config-asflags (default '()))
  (lex cc-config-lex (default '()))
  (yacc cc-config-yacc (default '()))
  (aidlflags cc-config-aidlflags (default '()))
  (rtti cc-config-rtti (default #f))
  (enabled cc-config-enabled (default #t))

  (include-build-directory cc-config-include-build-directory (default #t))
  (include-dirs cc-config-include-dirs (default '()))
  (local-include-dirs cc-config-local-include-dirs (default '()))
  (export-include-dirs cc-config-export-include-dirs (default '()))
  (export-system-include-dirs cc-config-export-system-include-dirs (default '()))
  (generated-sources cc-config-generated-sources (default '()))

  (header-libs cc-config-header-libs (default '()))
  (shared-libs cc-config-shared-libs (default '()))
  (system-shared-libs cc-config-system-shared-libs (default '()))
  (static-libs cc-config-static-libs (default '()))
  (whole-static-libs cc-config-whole-static-libs (default '()))
  (generated-headers cc-config-generated-headers (default '()))
  (group-static-libs cc-config-group-static-libs (default #f))

  (export-header-lib-headers cc-config-export-header-lib-headers (default '()))
  (export-shared-lib-headers cc-config-export-shared-lib-headers (default '()))
  (export-static-lib-headers cc-config-export-static-lib-headers (default '()))
  (export-generated-headers cc-config-export-generated-headers (default '()))

  (static cc-config-static (default '()))
  (shared cc-config-shared (default '()))
  (vndk cc-config-vndk (default '()))
  (llndk cc-config-llndk (default '()))
  (sanitize cc-config-sanitize (default '()))
  (afdo cc-config-afdo (default #f))
  (lto cc-config-lto (default '()))
  (pack-relocations cc-config-pack-relocations (default #f))
  (nocrt cc-config-nocrt (default #f))
  (native-coverage cc-config-native-coverage (default #f))
  (no-libcrt cc-config-no-libcrt (default #f))

  (bazel-module cc-config-bazel-module (default '()))
  (version-script cc-config-version-script (default #f))

  (use-version-lib cc-config-use-version-lib (default #f))
  (gnu-extensions cc-config-gnu-extensions (default #t))
  (c-std cc-config-c-std (default ""))
  (cpp-std cc-config-cpp-std (default ""))

  (proto cc-config-proto (default '()))

  (target cc-config-target (default '()))
  (arch cc-config-arch (default '()))
  (codegen cc-config-codegen (default '()))
  (soong-config-variables cc-config-soong-config-variables (default '()))
  (compile-multilib cc-config-compile-multilib (default #f))
  (multilib cc-config-multilib (default #f))

  (host-supported cc-config-host-supported (default #f))

  (required cc-config-required (default '()))
  (min-sdk-version cc-config-min-sdk-version (default #f))
  (sdk-version cc-config-sdk-version (default #f))
  (stl cc-config-stl (default #f))
  (stubs cc-config-stubs (default '()))
  (logtags cc-config-logtags (default '()))
  (unique-host-soname cc-config-unique-host-soname (default #f))
  (static-ndk-lib cc-config-static-ndk-lib (default #f))
  (double-loadable cc-config-double-loadable (default #f))

  (tidy cc-config-tidy (default #f))
  (tidy-flags cc-config-tidy-flags (default #f))
  (tidy-checks cc-config-tidy-checks (default '()))
  (tidy-checks-as-errors cc-config-tidy-checks-as-errors (default '()))
  (tidy-timeout-srcs cc-config-tidy-timeout-srcs (default '()))

  (device-supported cc-config-device-supported (default #f))
  (recovery-available cc-config-recovery-available (default #f))
  (vendor-available cc-config-vendor-available (default #f))
  (vendor-ramdisk-available cc-config-vendor-ramdisk-available (default #f))
  (ramdisk-available cc-config-ramdisk-available (default #f))
  (apex-available cc-config-apex-available (default #f))
  (product-available cc-config-product-available (default #f))
  (native-bridge-supported cc-config-native-bridge-supported (default #f))
  (dist cc-config-dist (default '()))
  (pgo cc-confg-pgo (default #f))
  ;; XXX: Currently ignored, until it bites us.  Probably should have a
  ;; more global effect in blueprint.scm.
  (visibility cc-config-visibility (default #f))
  (defaults-visibility cc-config-defaults-visibility (default #f))
  (product-variables cc-config-product-variables (default '()))
  (fuzz-config cc-config-fuzz-config (default '()))
  (header-abi-checker cc-config-header-abi-checker (default '())))

;; Global variables. See soong/cc/config/clang.go.

(define clang-unknown-cflags
  (list "-finline-functions"
        "-finline-limit=64"
        "-fno-canonical-system-headers"
	    "-Wno-clobbered"
	    "-fno-devirtualize"
	    "-fno-tree-sra"
	    "-fprefetch-loop-arrays"
	    "-funswitch-loops"
	    "-Werror=unused-but-set-parameter"
	    "-Werror=unused-but-set-variable"
	    "-Wmaybe-uninitialized"
	    "-Wno-error=clobbered"
	    "-Wno-error=maybe-uninitialized"
	    "-Wno-error=unused-but-set-parameter"
	    "-Wno-error=unused-but-set-variable"
	    "-Wno-extended-offsetof"
	    "-Wno-free-nonheap-object"
	    "-Wno-literal-suffix"
	    "-Wno-maybe-uninitialized"
	    "-Wno-old-style-declaration"
	    "-Wno-psabi"
	    "-Wno-unused-but-set-parameter"
	    "-Wno-unused-but-set-variable"
	    "-Wno-unused-local-typedefs"
	    "-Wunused-but-set-parameter"
	    "-Wunused-but-set-variable"
	    "-fdiagnostics-color"
	    ;; http://b/153759688
	    "-fuse-init-array"
	    ;; arm + arm64
	    "-fgcse-after-reload"
	    "-frerun-cse-after-loop"
	    "-frename-registers"
	    "-fno-strict-volatile-bitfields"
	    ;; arm + arm64
	    "-fno-align-jumps"
	    ;; arm
	    "-mthumb-interwork"
	    "-fno-builtin-sin"
	    "-fno-caller-saves"
	    "-fno-early-inlining"
	    "-fno-move-loop-invariants"
	    "-fno-partial-inlining"
	    "-fno-tree-copy-prop"
	    "-fno-tree-loop-optimize"
	    ;; x86 + x86_64
	    "-finline-limit=300"
	    "-fno-inline-functions-called-once"
	    "-mfpmath=sse"
	    "-mbionic"
	    ;; windows
	    "--enable-stdcall-fixup"

        ;; XXX: sometimes clang segfaults when building some C++ files.  The
        ;; reason is unknown, and seems to be related to the warning flags.
        ;; Prevent setting -Werror (it's not set by default), so we can remove
        ;; the -Wno-* flags that cause this issue
        ;; XXX: not part of soong, of course.
        "-Werror"
        ))

(define clang-extra-cflags
  (list "-D__compiler_offsetof=__builtin_offsetof"
        ;; Emit address-significance table which allows linker to perform safe ICF. Clang does
        ;; not emit the table by default on Android since NDK still uses GNU binutils.
        "-faddrsig"
        ;; Help catch common 32/64-bit errors.
        ;"-Werror=int-conversion"
        ;; Enable the new pass manager.
        "-fexperimental-new-pass-manager"
        ;; Disable overly aggressive warning for macros defined with a leading underscore
        ;; This happens in AndroidConfig.h, which is included nearly everywhere.
        ;; TODO: can we remove this now?
        "-Wno-reserved-id-macro"
        ;; Workaround for ccache with clang.
        ;; See http://petereisentraut.blogspot.com/2011/05/ccache-and-clang.html.
        "-Wno-unused-command-line-argument"
        ;; Force clang to always output color diagnostics. Ninja will strip the ANSI
        ;; color codes if it is not running in a terminal.
        "-fcolor-diagnostics"
        ;; Warnings from clang-7.0
        "-Wno-sign-compare"
        ;; Warnings from clang-8.0
        "-Wno-defaulted-function-deleted"
        ;; Disable -Winconsistent-missing-override until we can clean up the existing
        ;; codebase for it.
        "-Wno-inconsistent-missing-override"
        ;; Warnings from clang-10
        ;; Nested and array designated initialization is nice to have.
        ;; XXX: This results in a warning that can be treated as an error,
        ;; disabled for now.
        "-Wno-c99-designator"

        ;; Warnings from clang-12
        "-Wno-gnu-folding-constant"

        ;; Calls to the APIs that are newer than the min sdk version of the caller should be
        ;; guarded with __builtin_available.
        ;"-Wunguarded-availability"
        ;; This macro allows the bionic versioning.h to indirectly determine whether the
        ;; option -Wunguarded-availability is on or not.
        "-D__ANDROID_UNAVAILABLE_SYMBOLS_ARE_WEAK__"

        ;; Do not warn, so it's not treated as an error.
        ;; XXX: Additional flag not present in soong.
        "-Wno-microsoft-default-arg-redefinition"))

(define clang-extra-cppflags
  (list
    ;; -Wimplicit-fallthrough is not enabled by -Wall.
    "-Wimplicit-fallthrough"
    ;; Enable clang's thread-safety annotations in libcxx.
    "-D_LIBCPP_ENABLE_THREAD_SAFETY_ANNOTATIONS"
    ;; libc++'s math.h has an #include_next outside of system_headers.
    "-Wno-gnu-include-next"))

(define clang-extra-target-cflags
  (list "-nostdlibinc"))

(define clang-extra-no-override-cflags
  (list "-Werror=address-of-temporary"

        ;; Bug: http://b/29823425 Disable -Wnull-dereference until the
        ;; new cases detected by this warning in Clang r271374 are
        ;; fixed.
        ;;"-Werror=null-dereference"
        "-Werror=return-type"
        ;; http://b/72331526 Disable -Wtautological-* until the instances detected by these
        ;; new warnings are fixed.
        "-Wno-tautological-constant-compare"
        "-Wno-tautological-type-limit-compare"
        ;; http://b/145210666
        "-Wno-reorder-init-list"
        ;; http://b/145211066
        "-Wno-implicit-int-float-conversion"
        ;; New warnings to be fixed after clang-r377782.
        "-Wno-int-in-bool-context" ;; http://b/148287349
        "-Wno-sizeof-array-div" ;; http://b/148815709
        "-Wno-tautological-overlap-compare" ;; http://b/148815696
        ;; New warnings to be fixed after clang-r383902.
        "-Wno-deprecated-copy" ;; http://b/153746672
        "-Wno-range-loop-construct" ;; http://b/153747076
        "-Wno-misleading-indentation" ;; http://b/153746954
        "-Wno-zero-as-null-pointer-constant" ;; http://b/68236239
        "-Wno-deprecated-anon-enum-enum-conversion" ;; http://b/153746485
        "-Wno-deprecated-enum-enum-conversion" ;; http://b/153746563
        "-Wno-string-compare" ;; http://b/153764102
        "-Wno-enum-enum-conversion" ;; http://b/154138986
        "-Wno-enum-float-conversion" ;; http://b/154255917
        "-Wno-pessimizing-move" ;; http://b/154270751
        ))

(define clang-extra-external-cflags
  (list "-Wno-enum-compare"
        "-Wno-enum-compare-switch"
        ;; http://b/72331524 Allow null pointer arithmetic until the instances detected by
        ;; this new warning are fixed.
        "-Wno-null-pointer-arithmetic"
        ;; Bug: http://b/29823425 Disable -Wnull-dereference until the
        ;; new instances detected by this warning are fixed.
        "-Wno-null-dereference"
        ;; http://b/145211477
        "-Wno-pointer-compare"
        ;; http://b/145211022
        "-Wno-xor-used-as-pow"
        ;; http://b/145211022
        "-Wno-final-dtor-non-final-class"
        ;; http://b/165945989
        "-Wno-psabi"))

;; See soong/cc/config/global.go.

(define common-global-cflags
  (list "-DANDROID"
		"-fmessage-length=0"
		"-W"
		"-Wall"
		"-Wno-unused"
		"-Winit-self"
		"-Wpointer-arith"
		"-Wunreachable-code-loop-increment"

		;; Make paths in deps files relative
		"-no-canonical-prefixes"
		"-fno-canonical-system-headers"

		"-DNDEBUG"
		"-UDEBUG"

		"-fno-exceptions"
		"-Wno-multichar"

		"-O2"
		"-g"

		"-fno-strict-aliasing"

		"-Werror=date-time"
		"-Werror=pragma-pack"
		"-Werror=pragma-pack-suspicious-include"
        "-Werror=string-plus-int"
		"-Werror=unreachable-code-loop-increment"))

(define common-global-conlyflags '())

(define device-global-cflags
  (list "-fdiagnostics-color"

		"-ffunction-sections"
		"-fdata-sections"
		"-fno-short-enums"
		"-funwind-tables"
		"-fstack-protector-strong"
		"-Wa,--noexecstack"
		"-D_FORTIFY_SOURCE=2"

		"-Wstrict-aliasing=2"

		"-Werror=return-type"
		"-Werror=non-virtual-dtor"
		"-Werror=address"
		"-Werror=sequence-point"
		"-Werror=format-security"))

(define device-global-cppflags
  (list "-fvisibility-inlines-hidden"))

(define device-global-ldflags
  (list "-Wl,-z,noexecstack"
		"-Wl,-z,relro"
		"-Wl,-z,now"
		"-Wl,--build-id=md5"
		"-Wl,--warn-shared-textrel"
		"-Wl,--fatal-warnings"
		"-Wl,--no-undefined-version"
		"-Wl,--exclude-libs,libgcc.a"
		"-Wl,--exclude-libs,libgcc_stripped.a"
		"-Wl,--exclude-libs,libunwind_llvm.a"))

(define device-global-lldflags (list "-fuse-ld=lld"))

(define host-global-cflags '())

(define host-global-cppflags '())

(define host-global-ldflags '())

(define host-global-lldflags (list "-fuse-ld=lld"))

(define common-global-cppflags (list "-Wsign-promo"))

(define no-override-global-cflags
  (list "-Werror=int-to-pointer-cast"
		"-Werror=pointer-to-int-cast"
		"-Werror=fortify-source"))

;; TODO: customize depending on target
;; See soong/cc/config/x86_linux_host.go.
(define linux-cflags
  (list "-fdiagnostics-color"
        "-Wa,--noexecstack"
        "-fPIC"
        "-U_FORTIFY_SOURCE"
        "-D_FORTIFY_SOURCE=2"
        "-fstack-protector"
        ;; Workaround differences in inttypes.h between host and target.
        ;; See bug 12708004.
        "-D__STDC_FORMAT_MACROS"
        "-D__STDC_CONSTANT_MACROS"))

(define (extend-cc-with-proto cc-conf proto-conf)
  (let* ((lib (and (not (proto-config-plugin proto-conf))
                   (match (proto-config-type proto-conf)
                     ;; TODO: when building the SDK (ctx.useSdk), full and lite
                     ;; cases are different.
                     ("full" "libprotobuf-cpp-full")
                     ("lite" "libprotobuf-cpp-lite")
                     ("nanopb-c" "libprotobuf-c-nano")
                     ("nanopb-c-enable_malloc" "libprotobuf-c-nano-enable_malloc")
                     ("nanopb-c-16bit" "libprotobuf-c-nano-16bit")
                     ("nanopb-c-enable_malloc-16bit" "libprotobuf-c-nano-enable_malloc-16bit")
                     ("nanopb-c-32bit" "libprotobuf-c-nano-32bit")
                     ("nanopb-c-enable_malloc-32bit" "libprotobuf-c-nano-enable_malloc-32bit")
                     (#f #f))))
         (lib-key (if (and lib (member (proto-config-type proto-conf) '("full" "lite")))
                      'shared-libs
                      'static-libs))
         (header-key (if (and lib (member (proto-config-type proto-conf) '("full" "lite")))
                         'export-shared-lib-headers
                         'export-static-lib-headers)))
    (if lib
        (extend-cc-config cc-conf `((,lib-key . ,(list lib))
                                    (,header-key . ,(list lib))))
        cc-conf)))

;; Dependency management
(define (get-lib-dependencies config get-input)
  (let* ((dynamic-libs (cc-config-shared-libs config))
         (static-libs (sort-libs (cc-config-static-libs config) get-input))
         (whole-static-libs (cc-config-whole-static-libs config))
         (libbuildversion
           (if (cc-config-use-version-lib config)
               (list "libbuildversion")
               '())))
    (append dynamic-libs static-libs whole-static-libs libbuildversion)))

;; Individual build rules. See soong/cc/builder.go.

(define (generate-cc-object-rule cc-cmd cflags in out deps)
  (make-makefile-rule
    (list out) (list in)
    (flatten
      (list cc-cmd "-c"
            (filter
              (lambda (f)
                (not (member f clang-unknown-cflags)))
              cflags)
            "-MD" "-MF" (string-append out ".d") "-o" out in))
    #f
    deps))

(define (generate-lex-rule lexproperties in out)
  (make-makefile-rule
    (list out) (list in)
    (flatten
      (list "flex" (or (assoc-ref lexproperties 'flags) '())
            (string-append "-o" out) in))
    #t
    '()))

(define (generate-yacc-rule yaccproperties get-input in header out)
  (make-makefile-rule
    (list out header) (list in)
    (flatten
      (list (string-append "BISON_PKGDATADIR="
                           (get-input "bison") "/share/bison")
            "bison" "-d" (or (assoc-ref yaccproperties 'flags) '())
            (string-append "--defines=" header)
            "-o" out in))
    #t
    '()))

(define host-ld-libs (list "-ldl" "-lpthread" "-lm" "-lrt"))
(define (generate-ld-rule object-rules ld-cmd libflags ldflags stl target rsp out dependencies)
  (make-makefile-rule
    (list out)
    (filter (lambda (file)
              (equal? "o" (car (reverse (string-split file #\.)))))
            (flatten (map makefile-rule-out object-rules)))
    (flatten
      (list ld-cmd (string-append "@" rsp) libflags "-o" out ldflags
            (match stl
              ((or () #f "libc++" "libc++_static")
               (cons "-lc++" host-ld-libs))
              ((or "none") '("-nostdlib" "-nostdlib++")))
            "-v"))
    #f
    dependencies))

(define (generate-ar-rule object-rules ar-cmd arflags rsp out dependencies)
  (make-makefile-rule
    (list out)
    (filter (lambda (file)
              (equal? "o" (car (reverse (string-split file #\.)))))
            (flatten (map makefile-rule-out object-rules)))
    (flatten
      (list ar-cmd arflags out (string-append "@" rsp)))
    #f
    dependencies))

(define (generate-ar-with-libs-rule object-rules ar-cmd ar-obj-flags ar-lib-flags
                                    arflags ar-libs rsp out dependencies)
  (make-makefile-rule
    (list out)
    (filter (lambda (file)
              (equal? "o" (car (reverse (string-split file #\.)))))
            (flatten (map makefile-rule-out object-rules)))
    (list (flatten (list ar-cmd ar-obj-flags arflags out (string-append "@" rsp)))
          (flatten (list ar-cmd ar-lib-flags arflags out ar-libs)))
    #f
    dependencies))

(define (target-clang-flags target)
  (match target
    (#f '())
    (t (list (string-append "--target=" t)))))

(define* (delete-all lst . el)
  (let loop ((lst lst) (els el))
    (match els
      (() lst)
      ((el els ...)
       (loop (delete el lst) els)))))

(define (build-rules get-input config proto-conf target)
  (define (filter-flags flags)
    (filter (lambda (flag) (or (not (string-prefix? "-W" flag)) (string-prefix? "-Wl," flag))) flags))
  (define (get-objpath file)
    (match (reverse (string-split file #\.))
      ((ext . rest)
       (string-join
         (reverse (cons "o" rest))
         "."))))
  ;; soong/cc/compiler.go and soong/cc/config/global.go
  (let* ((cstd (match (cc-config-c-std config)
                 ("experimental" "gnu11")
                 ("" "gnu99")
                 (cstd cstd)))
         (cppstd (match (cc-config-cpp-std config)
                   ("experimental" "gnu++2a")
                   ("" "gnu++17")
                   (cppstd cppstd)))
         (deps-include
           (map (cut get-input <>)
                (append
                   (cc-config-header-libs config)
                   (cc-config-shared-libs config)
                   (cc-config-static-libs config)
                   (cc-config-whole-static-libs config)
                   (map
                     (lambda (dir)
                       (string-append
                         "android-"
                         (string-join (string-split dir #\/) "-")
                         "-include"))
                     (cc-config-include-dirs config))
                   (if (cc-config-use-version-lib config)
                       (list "libbuildversion")
                       '()))))
         (include-flags
           (append
             (map (cut string-append "-I" <> "/include") deps-include)
             (map (lambda (pkg) (string-append "-I" (get-input pkg)
                                               "/share/android/out"))
                  (cc-config-generated-headers config))
             (map (cut string-append "-I" <>) (or (cc-config-local-include-dirs config) '()))
             (map (cut string-append "-I" <>) (or (cc-config-export-include-dirs config) '()))
             (map (cut string-append "-I" <>) (or (cc-config-export-system-include-dirs config) '()))
             (list "-I.")
             (if (cc-config-include-build-directory config)
                 (list "-Iinclude")
                 '())))
         (srcs (expand-srcs (cc-config-srcs config) (cc-config-exclude-srcs config)
                            get-input))
         (cppflags
           (uniq
             (append
               common-global-cflags
               linux-cflags
               clang-extra-cflags
               (if (cc-config-rtti config) (list "-frtti") (list "-fno-rtti"))
               (list (string-append "-std=" cppstd))
               (filter-flags (cc-config-cppflags config))
               (filter-flags (cc-config-cflags config))
               (filter-flags (cc-config-clang-cflags config))
               clang-extra-cppflags
               include-flags
               (target-clang-flags target)
               (list (string-append "-I" (proto-config-dir proto-conf))))))
         (cppflags (if (member "-O0" cppflags)
                       (delete-all cppflags "-D_FORTIFY_SOURCE=2" "-U_FORTIFY_SOURCE"
                                   "-O2" "-O3")
                       cppflags))
         (cflags
           (uniq
             (append
               common-global-cflags
               linux-cflags
               clang-extra-cflags
               (if (cc-config-rtti config) (list "-frtti") (list "-fno-rtti"))
               (list (string-append "-std=" cstd))
               (filter-flags (cc-config-conlyflags config))
               (filter-flags (cc-config-cflags config))
               (filter-flags (cc-config-clang-cflags config))
               include-flags
               (target-clang-flags target)
               (list (string-append "-I" (proto-config-dir proto-conf))))))
         (cflags (if (member "-O0" cflags)
                     (delete "-D_FORTIFY_SOURCE=2" (delete "-U_FORTIFY_SOURCE" cflags))
                     cflags))
         (asflags
           (append
             common-global-cflags
             (cc-config-asflags config)
             (list "-D__ASSEMBLY__")))
         (object-deps
           (append
             (cc-config-header-libs config)
             (cc-config-shared-libs config)
             (cc-config-static-libs config)
             (cc-config-whole-static-libs config)
             (cc-config-generated-headers config)
             (filter-map
               (lambda (src)
                 (and (string-prefix? ":" src)
                      (substring src 1)))
               (cc-config-srcs config))
             (filter-map
               (lambda (src)
                 (and (string-prefix? ":" src)
                      (substring src 1)))
               (cc-config-exclude-srcs config))
             (if (cc-config-use-version-lib config)
                 (list "libbuildversion")
                 '()))))
  (flatten
    (filter-map
      (lambda (file)
        (let ((suffix (match (reverse (string-split file #\.))
                        ((ext . rest) ext)))
              (objpath (get-objpath file)))
          (match suffix
            ("o" #f)
            ("h" #f)
            ((or "ll" "l")
             (let* ((cpp? (equal? suffix "ll"))
                    (dir (dirname file))
                    (generated-source (string-append dir (if (equal? dir "") "" "/")
                                                     (basename file suffix)
                                                     (if cpp? "cpp" "c")))
                    (generated-obj (get-objpath generated-source))
                    (cc-flags (if cpp? cppflags cflags)))
               (list
                 (generate-lex-rule (cc-config-lex config) file generated-source)
                 (generate-cc-object-rule (if cpp? "clang++" "clang")
                                          cc-flags
                                          generated-source
                                          generated-obj
                                          object-deps))))
            ((or "yy" "y")
             (let* ((cpp? (equal? suffix "yy"))
                    (dir (dirname file))
                    (generated-source (string-append dir (if (equal? dir "") "" "/")
                                                     (basename file suffix)
                                                     (if cpp? "cpp" "c")))
                    (generated-header (string-append dir (if (equal? dir "") "" "/")
                                                     (basename file suffix) "h"))
                    (generated-obj (get-objpath generated-source))
                    (cc-flags (if cpp? cppflags cflags)))
               (list
                 (generate-yacc-rule (cc-config-yacc config) get-input file
                                     generated-header generated-source)
                 (generate-cc-object-rule (if cpp? "clang++" "clang")
                                          cc-flags
                                          generated-source
                                          generated-obj
                                          object-deps))))
            ("proto"
             (let* ((cpp? (proto-cc-cpp? proto-conf))
                    (generated-source (proto-cc-generated-source proto-conf file))
                    (generated-obj (get-objpath generated-source))
                    (cc-flags (append
                                (if cpp? cppflags cflags)
                                (list
                                  "-DGOOGLE_PROTOBUF_NO_RTTI"))))
               (list
                 (generate-cc-proto-rule proto-conf file)
                 (generate-cc-object-rule (if cpp? "clang++" "clang")
                                          cc-flags
                                          generated-source
                                          generated-obj
                                          object-deps))))
            ((or "cc" "cpp")
             (generate-cc-object-rule "clang++" cppflags file objpath object-deps))
            ("c"
             (generate-cc-object-rule "clang" cflags file objpath object-deps))
            ((or "S" "s")
             (generate-cc-object-rule "clang" asflags file objpath object-deps)))))
      srcs))))

;; Other rules, custom

(define (install-rules get-input config proto-conf module-type name)
  ;; install headers
  (let* ((dirs (append
                 (cc-config-export-include-dirs config)
                 (cc-config-export-system-include-dirs config)
                 (map (lambda (p) (string-append (get-input p) "/include"))
                        (append
                          (cc-config-export-header-lib-headers config)
                          (cc-config-export-shared-lib-headers config)
                          (cc-config-export-static-lib-headers config)))))
         (dependencies (append (cc-config-export-header-lib-headers config)
                               (cc-config-export-shared-lib-headers config)
                               (cc-config-export-static-lib-headers config)))
         (hdr-rules
           (map
             (lambda (dir)
               `(dir ,dir "include" ,dependencies))
             dirs))
         (export-proto-headers (proto-config-export-proto-headers proto-conf))
         (proto-rules
           (filter-map
             (lambda (file)
               (match (reverse (string-split file #\.))
                 (("proto" . _)
                  (let ((header (proto-cc-generated-header proto-conf file)))
                    `(file ,header
                           ,(string-append "include/"
                                           (substring
                                             (dirname header)
                                             (string-length
                                               (proto-config-dir proto-conf))))
                           ())))
                 (_ #f)))
             (expand-srcs (cc-config-srcs config) (cc-config-exclude-srcs config)
                          get-input))))
    (append
      (if export-proto-headers proto-rules '())
      hdr-rules)))

(define (generate-bin-rule config rules get-input module-type name target)
  (let* ((dynamic-libs (cc-config-shared-libs config))
         (static-libs (sort-libs (cc-config-static-libs config) get-input))
         (whole-static-libs
           (append (cc-config-whole-static-libs config)
                   (if (cc-config-use-version-lib config)
                       (list "libbuildversion")
                       '())))
         (ldflags (cc-config-ldflags config))
         (dependencies (get-lib-dependencies config get-input))
         (whole-static-libflags
           (map
             (lambda (lib)
               (string-append (get-input lib) "/lib/" lib ".a"))
             whole-static-libs))
         (static-libflags
           (map
             (lambda (lib)
               (string-append (get-input lib) "/lib/" lib ".a"))
             static-libs))
         (libflags
           (map
             (lambda (lib)
               (string-append (get-input lib) "/lib/" lib ".so"))
             dynamic-libs)))
    (generate-ld-rule
      rules "clang++"
      ;; see TransformObjToDynamicBinary from builder.go
      ;; XXX: some of these actually depend on the target
      (flatten
        (list
          ldflags
          "-fuse-ld=lld"
          (if (null? whole-static-libflags)
              '()
              (list
                "-Wl,--whole-archive"
                whole-static-libflags
                "-Wl,--no-whole-archive"))
          (if (cc-config-group-static-libs config)
              '("-Wl,--start-group")
              '())
          static-libflags
          (if (cc-config-group-static-libs config)
              '("-Wl,--end-group")
              '())
          libflags))
      (target-clang-flags target)
      (cc-config-stl config)
      target
      "out.rsp" name dependencies)))

(define (compare-static-libs get-input lib1 lib2)
  (define (lib-deps lib)
    (catch #t
      (lambda _
        (let* ((bp (get-module (list
                                 (string-append (get-input lib)
                                                "/share/android/Android.bp"))
                               #f; type
                               lib))
               (lib-config (blueprint-module->cc-config bp))
               (libs
                 (append
                   (cc-config-static-libs lib-config)
                   (cc-config-whole-static-libs lib-config)
                   (cc-config-shared-libs lib-config))))
          (append
            libs
            (map
              lib-deps
              libs))))
      (lambda _
        '())))

  ;; if lib1 depends on lib2, it needs to be first
  (member lib2 (lib-deps lib1)))

(define (sort-libs lst get-input)
  (define (insert e lst)
    (match lst
      (() (list e))
      ((e1 lst ...)
       (if (compare-static-libs get-input e e1)
           (cons* e e1 lst)
           (cons e1 (insert e lst))))))

  (let loop ((lst lst) (res '()))
    (match lst
      (() res)
      ((e lst ...)
       (loop lst (insert e res))))))

(define (generate-static-lib-rule config rules get-input module-type name target
                                  dependencies)
  (let* ((dynamic-libs (cc-config-shared-libs config))
         (static-libs (sort-libs (cc-config-static-libs config) get-input))
         (whole-static-libs
           (append (cc-config-whole-static-libs config)
                   (if (cc-config-use-version-lib config)
                       (list "libbuildversion")
                       '())))
         (static-libs (cc-config-whole-static-libs config)))
      (if (equal? (length static-libs) 0)
        (generate-ar-rule
          rules "llvm-ar" (list "crsPD" "--format=gnu") "out.rsp"
          (string-append name ".a")
          dependencies)
        (generate-ar-with-libs-rule
          rules "llvm-ar" (list "crsPD") (list "cqsL")
          (list "--format=gnu")
          (map
            (lambda (name)
              (string-append (get-input name) "/lib/" name ".a"))
            static-libs)
          "out.rsp" (string-append name ".a") dependencies))))

(define (generate-dynamic-lib-rule config rules get-input module-type name target
                                   dependencies)
  (generate-ld-rule
    rules "clang++"
    (map
      (lambda (lib)
        (string-append (get-input lib) "/lib/" lib ".so"))
      (cc-config-shared-libs config))
    (append
      (list "-fuse-ld=lld")
      (target-clang-flags target)
      (cc-config-ldflags config)
      (list "-shared" (string-append "-Wl,-soname," name ".so")))
    (cc-config-stl config)
    target
    "out.rsp" (string-append name ".so") dependencies))

(define (resolve-conditional labels conditional config)
  (match labels
    (() config)
    ((label labels ...)
     (let ((keyvals (assoc-ref conditional label)))
       (if keyvals
           (begin
             (resolve-conditional labels conditional (extend-cc-config config keyvals)))
           (resolve-conditional labels conditional config))))))

(define (resolve-soong-variables config)
  "Return a new, extended config, with additional configuration from the
soong_config_variables dictionary."
  (let ((soong-config (cc-config-soong-config-variables config))
        (config-labels '("source_build")))
    (resolve-conditional config-labels soong-config config)))

(define (resolve-codegen config target)
  "Return a new, extended config, with additional configuration from the
codegen dictionary."
 (let ((codegen-config (cc-config-codegen config))
       (codegen-labels
         (cond
           ((string-contains target "armhf") '(arm))
           ((string-contains target "aarch64") '(arm64))
           ((string-contains target "i686") '(x86))
           ((string-contains target "x86_64") '(x86-64))
           (else (throw 'unknow-arch target)))))
   (resolve-conditional codegen-labels codegen-config config)))

(define (resolve-arch config target)
  "Return a new, extended config, with additional configuration from the
arch dictionary."
 (let ((arch-config (cc-config-arch config))
       (arch-labels
         (cond
           ((string-contains target "armhf") '(arm))
           ((string-contains target "aarch64") '(arm64))
           ((string-contains target "i686") '(x86))
           ((string-contains target "x86_64") '(x86-64))
           (else (throw 'unknow-arch target)))))
   (resolve-conditional arch-labels arch-config config)))

(define (resolve-target config target)
  "Return a new, extended config, with additional configuration from the
target dictionary."
 (let ((target-config (cc-config-target config))
       (target-labels
         (cond
           ((and (string-contains target "-android")
                 (string-contains target "arm"))
            '(android android_arm))
           ((string-contains target "-android") '(android))
           ((and (string-contains target "x86_64")
                 (string-contains target "linux"))
            '(host not-windows linux linux-glibc linux-x86-64 host-linux))
           ((and (string-contains target "i686")
                 (string-contains target "linux"))
            '(host not-windows linux linux-glibc linux-x86 host-linux))
           ((and (string-contains target "armhf")
                 (string-contains target "linux"))
            '(host not-windows linux linux-glibc linux-arm host-linux))
           ((and (string-contains target "aarch64")
                 (string-contains target "linux"))
            '(host not-windows linux linux-glibc linux-arm64 host-linux))
           ((string-contains target "-linux") '(host not-windows linux linux-glibc host-linux))
           ((string-contains target "i586-gnu") '(host not-windows))
           (else (throw 'unknow-arch target)))))
   (resolve-conditional target-labels target-config config)))

(define (extend-art-config get-input config)
  ;; See art/build/art.go (in android, not in soong)
  ;; XXX: Assuming no environment variable is set, and sanitize_* is false.
  ;; globalFlags
  (let* ((cflags (list "-O3"
                       "-DART_DEFAULT_GC_TYPE_IS_CMS"
                       "-DART_DEFAULT_COMPACT_DEX_LEVEL=fast"
                       "-DART_STACK_OVERFLOW_GAP_arm=8192"
                       "-DART_STACK_OVERFLOW_GAP_arm64=8192"
                       "-DART_STACK_OVERFLOW_GAP_x86=8192"
                       "-DART_STACK_OVERFLOW_GAP_x86_64=8192"
                       ;; "not false"
                       "-DART_USE_GENERATIONAL_CC=1"
                       "-DUSE_D8_DESUGAR=1"))
         ;; deviceFlags
         (device-cflags (list "-Wframe-larger-than=1736"
                              "-DART_FRAME_SIZE_LIMIT=1736"
                              "-DART_BASE_ADDRESS=0x70000000"
                              "-DART_BASE_ADDRESS_MIN_DELTA=-0x1000000"
                              "-DART_BASE_ADDRESS_MAX_DELTA=0x1000000"))
         ;; hostFlags
         (host-cflags (list ;"-Wframe-larger-than=1736" ; breaks dmtracedump
                            "-DART_FRAME_SIZE_LIMIT=1736"
                            "-DART_BASE_ADDRESS=0x60000000"
                            "-DART_BASE_ADDRESS_MIN_DELTA=-0x1000000"
                            "-DART_BASE_ADDRESS_MAX_DELTA=0x1000000"
                            (string-append "-DART_CLANG_PATH=\"" (get-input "clang") "\""))))
    (extend-cc-config config
      `((cflags . ,cflags)
        (target . ((host . ((cflags . ,host-cflags)))
                   (android . ((cflags . ,device-cflags)))))))))

(define (generate-rsp objects rsp)
  (with-output-to-file rsp
    (lambda _
      (format #t "~{~a~%~}~%"
              (filter
                (lambda (file)
                  (equal? "o" (car (reverse (string-split file #\.)))))
                objects)))))

(define* (generate-cc-makefile
           cc-config
           #:key get-input module-type name system target
           ;; indicates whether we are building or not (import does a dry run)
           (dry-run? #f)
           #:allow-other-keys)
  (let* ((cc-config (if (string-prefix? "art_cc_" module-type)
                        (extend-art-config get-input cc-config)
                        cc-config))
         (cc-config (resolve-target cc-config (or target system)))
         (cc-config (resolve-soong-variables cc-config))
         (cc-config (resolve-arch cc-config (or target system)))
         (cc-config (resolve-codegen cc-config (or target system)))
         (proto-conf (cc-config-proto cc-config))
         (proto-conf (keyvals->proto-config proto-conf))
         (proto-conf (extend-proto-with-defaults proto-conf))
         (proto-conf (extend-proto-with-cc proto-conf get-input))
         (cc-config (extend-cc-with-proto cc-config proto-conf))
         (install-rules (install-rules get-input cc-config proto-conf module-type name))
         (object-rules (build-rules get-input cc-config proto-conf target))
         (bin-rule (generate-bin-rule cc-config object-rules get-input module-type name
                                      target))
         (dependencies (get-lib-dependencies cc-config get-input))
         (static-lib-rule (generate-static-lib-rule
                            cc-config object-rules get-input module-type name
                            target dependencies))
         (dynamic-lib-rule (generate-dynamic-lib-rule
                             cc-config object-rules get-input module-type name
                             target dependencies))
         (srcs (expand-srcs (cc-config-srcs cc-config) (cc-config-exclude-srcs cc-config)
                            get-input))
         (object-sources (filter
                           (lambda (file)
                             (string-suffix? ".o" file))
                           srcs)))
    (unless dry-run?
      (generate-rsp (flatten (append (map makefile-rule-out object-rules)
                                     object-sources))
                    "out.rsp"))
    (let ((build-rules
           (append object-rules
                   (match module-type
                     ((or "cc_binary" "cc_binary_host" "art_cc_binary")
                      (list bin-rule))
                     ("cc_object" '())
                     ((or "cc_library" "cc_library_host" "art_cc_library")
                      (list dynamic-lib-rule static-lib-rule))
                     ((or "cc_library_static" "cc_library_host_static" "art_cc_library_static")
                      (list static-lib-rule))
                     ((or "cc_library_dynamic" "cc_library_host_dynamic")
                      (list dynamic-lib-rule))
                     (_ '()))))
          (install-rules
            (append install-rules
                    (match module-type
                      ((or "cc_binary" "cc_binary_host" "art_cc_binary")
                       `((executable ,name "bin" ())))
                      ("cc_object"
                       (map
                         (lambda (file)
                           `(file ,file "share/android/out" ()))
                         (flatten
                           (append
                             (map makefile-rule-out object-rules)
                             object-sources))))
                      ((or "cc_library" "cc_library_host" "art_cc_library")
                       `((executable ,(string-append name ".so") "lib" ())
                         (file ,(string-append name ".a") "lib" ())))
                      ((or "cc_library_static" "cc_library_host_static"
                           "art_cc_library_static")
                       `((file ,(string-append name ".a") "lib" ())))
                      ((or "cc_library_dynamic" "cc_library_host_dynamic")
                       `((executable ,(string-append name ".so") "lib" ())))
                      (_ '())))))
      (make-makefile '() build-rules install-rules))))
