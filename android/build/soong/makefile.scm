;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android build soong makefile)
  #:use-module (guix build utils)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:export (generate-soong-makefile
            flatten

            makefile-rule
            make-makefile-rule
            makefile-rule?
            makefile-rule-out
            makefile-rule-deps
            makefile-rule-cmd
            makefile-rule-source?
            makefile-rule-dependencies
            update-makefile-rule

            makefile
            make-makefile
            makefile?
            makefile-variables
            makefile-build-rules
            makefile-install-rules
            update-makefile))

(define-record-type makefile-rule
  (make-makefile-rule out deps cmd source? dependencies)
  makefile-rule?
  (out          makefile-rule-out)
  (deps         makefile-rule-deps)
  (cmd          makefile-rule-cmd)
  (source?      makefile-rule-source?)
  (dependencies makefile-rule-dependencies))

(define* (update-makefile-rule rule #:key
                               (out          (makefile-rule-out rule))
                               (deps         (makefile-rule-deps rule))
                               (cmd          (makefile-rule-cmd rule))
                               (source?      (makefile-rule-source? rule))
                               (dependencies (makefile-rule-dependencies rule)))
  (make-makefile-rule out deps cmd source? dependencies))

(define-record-type makefile
  (make-makefile variables build-rules install-rules)
  makefile?
  (variables     makefile-variables)
  (build-rules   makefile-build-rules)
  (install-rules makefile-install-rules))

(define* (update-makefile makefile #:key
                          (variables     (makefile-variables makefile))
                          (build-rules   (makefile-build-rules makefile))
                          (install-rules (makefile-install-rules makefile)))
  (make-makefile variables build-rules install-rules))

(define* (generate-soong-makefile rules #:key (name "Makefile"))
  (define escape
    (match-lambda
      ("" "\"\"")
      ((? string? s)
       (string-join (string-split s #\") "\\\""))
      ((? list? l) (map escape l))))

  (define print-rule
    (match-lambda
      (($ makefile-rule out deps cmd source? dependencies)
       (format #t "~{~a ~}: ~{~a ~}~%"
               out (if source? deps (cons "generated-sources" deps)))
       (for-each
         (lambda (out)
           (format #t "\tmkdir -p ~a~%" (dirname out)))
         out)
       (let ((cmd (escape cmd)))
         (match cmd
           ((? string? s)
            (format #t "\t~a~%" s))
           (((? list? l) ...)
            (format #t "~{\t~{~a ~}~%~}~%" cmd))
           (((? string? s) ...)
            (format #t "\t~{~a ~}~%" cmd)))))))

  (define print-install-rule
    (match-lambda
      (('dir dir dest deps)
       (format #t "\tcd ~a; for f in `find .`; do \
if [ -d $$f ]; then install -m 755 -d $(PREFIX)/~a/$$f; else \
install -t $(PREFIX)/~a/$$(dirname $$f) $$f; fi; \
done~%" dir dest dest))
      (('file file dest deps)
       (format #t "\tinstall -m 644 -t $(PREFIX)/~a ~a~%" dest file))
      (('executable file dest deps)
       (format #t "\tinstall -m 755 -t $(PREFIX)/~a ~a~%" dest file))))

  (define (uniq lst)
    (let loop ((res '()) (lst lst))
      (match lst
        (() res)
        ((e lst ...)
         (if (member e res)
             (loop res lst)
             (loop (cons e res) lst))))))

  (define (all-dests install-rules)
    (uniq
      (map
        (match-lambda
          (('dir dir dest deps) dest)
          (('file file dest deps) dest)
          (('executable file dest deps) dest))
        install-rules)))

  (with-output-to-file name
    (lambda _
      (match rules
        (($ makefile variables build-rules install-rules)
         (for-each
           (match-lambda
             ((key . val)
              (format #t "~a = ~a~%" key val)))
           variables)
         (format #t "all: ~{~{~a ~} ~}~%" (map makefile-rule-out build-rules))
         (format #t "generated-sources: ~{~{~a ~} ~}~%~%"
                 (map makefile-rule-out
                      (filter (lambda (rule) (makefile-rule-source? rule))
                              build-rules)))
         (for-each print-rule build-rules)
         (format #t "install:~%")
         (for-each
           (lambda (dest)
             (format #t "\tinstall -d $(PREFIX)/~a~%" dest))
           (all-dests install-rules))
         (for-each print-install-rule install-rules))))))

(define (flatten lst)
  (match lst
    (() '())
    (((? list? l) lst ...)
     (append (flatten l) (flatten lst)))
    ((e lst ...)
     (cons e (flatten lst)))))
