;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android build soong genrule)
  #:use-module (android build blueprint)
  #:use-module (android build soong)
  #:use-module (android build soong makefile)
  #:use-module (ice-9 peg)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (blueprint-module->genrule-config
            generate-genrule-makefile

            genrule-config
            make-genrule-config
            genrule-config?
            extend-genrule

            genrule-config-cmd
            genrule-config-srcs
            genrule-config-exclude-srcs
            genrule-config-out
            genrule-config-tools
            genrule-config-tools-files))

(define-module-record-type genrule-config
  make-genrule-config genrule-config? extend-genrule-config
  (cmd genrule-config-cmd (default ""))
  (srcs genrule-config-srcs (default '()))
  (exclude-srcs genrule-config-exclude-srcs (default '()))
  (out  genrule-config-out (default '()))
  (tools genrule-config-tools (default '()))
  (tool-files genrule-config-tool-files (default '())))

;; peg parser for the cmd syntax
(define-peg-pattern chr body (or (range #\x00 #\x23) (range #\x25 #\xffff)))
(define-peg-pattern in all (ignore "$(in)"))
(define-peg-pattern out all (ignore "$(out)"))
(define-peg-pattern depfile all (ignore "$(depfile)"))
(define-peg-pattern gendir all (ignore "$(genDir)"))
(define-peg-pattern location all
                    (and (ignore "$(location")
                         (? (and (ignore " ") var-name))
                         (ignore ")")))
(define-peg-pattern var-name body (* (or (range #\a #\z) (range #\A #\Z)
                                         (range #\0 #\9) "_" ":")))
(define-peg-pattern cmd-pat body (* (or chr "$$" in out depfile gendir location)))

(define* (get-location get-input var #:key (dry-run? #f))
  (match (expand-srcs (list (string-append ":" var)) '() get-input)
    ((file) file)
    (() (if dry-run? "" (throw 'location-not-found var)))))

(define* (cmd->line config get-input #:key (dry-run? #f))
  (let* ((cmd (genrule-config-cmd config))
         (parse-tree (peg:tree (match-pattern cmd-pat cmd))))
    (if (list? parse-tree)
        (flatten
          (map
            (match-lambda
              ((? string? s) s)
              ('in (expand-srcs (genrule-config-srcs config)
                                (genrule-config-exclude-srcs config)
                                get-input))
              ('out (genrule-config-out config))
              (('location var)
               (get-location get-input var #:dry-run? dry-run?)))
            parse-tree))
        (list parse-tree))))

(define* (generate-genrule-makefile
           config #:key get-input (dry-run? #f) #:allow-other-keys)
  (make-makefile
    '()
    (list
      (make-makefile-rule
        (genrule-config-out config)
        (filter
          (lambda (file)
            (not (string-prefix? ":" file)))
          (genrule-config-srcs config))
        (cmd->line config get-input #:dry-run? dry-run?)
        #f
        (append
          (filter-map
            (lambda (src)
              (and (string-prefix? ":" src) (substring src 1)))
            (genrule-config-srcs config))
          (map
            (lambda (src)
              (if (string-prefix? ":" src)
                  (substring src 1)
                  src))
            (genrule-config-tools config)))))
    (map
      (lambda (file)
        `(file ,file "share/android/out" ()))
      (genrule-config-out config))))
