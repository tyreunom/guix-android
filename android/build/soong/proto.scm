;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android build soong proto)
  #:use-module (android build blueprint)
  #:use-module (android build soong makefile)
  #:use-module (ice-9 peg)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (keyvals->proto-config
            generate-proto-rule
            extend-proto-with-defaults

            extend-proto-with-cc
            proto-cc-generated-header
            proto-cc-generated-source
            proto-cc-cpp?
            generate-cc-proto-rule

            extend-proto-with-java
            proto-java-generated-source
            generate-java-proto-rule

            proto-config
            make-proto-config
            proto-config?
            extend-proto

            proto-config-type
            proto-config-plugin
            proto-config-export-proto-headers
            proto-config-canonical-path-from-root

            proto-config-flags
            proto-config-dir
            proto-config-subdir
            proto-config-out-type
            proto-config-out-params
            proto-config-deps))

;; See soong/android/proto.go
(define-module-record-type proto-config
  make-proto-config proto-config? extend-proto-config
  (type proto-config-type (default #f))
  (plugin proto-config-plugin (default #f))
  ;; seems to be ignored
  (export-proto-headers proto-config-export-proto-headers
                        (default #f))
  (canonical-path-from-root proto-config-canonical-path-from-root
                            (default #t))
  (include-dirs proto-config-include-dirs (default '()))
  (local-include-dirs proto-config-local-include-dirs (default '()))

  (flags proto-config-flags (default '()))
  (dir proto-config-dir (default ""))
  (subdir proto-config-subdir (default ""))
  (out-type proto-config-out-type (default ""))
  (out-params proto-config-out-params (default '()))
  (deps proto-config-deps (default '())))

(define (generate-proto-rule config protofile outputs)
  (make-makefile-rule
    outputs
    (list protofile)
    (flatten
      (list "aprotoc"
            (string-append (proto-config-out-type config) "="
                           (string-join (proto-config-out-params config) ",")
                           ":" (proto-config-dir config))
            "-I" "."
            (proto-config-flags config)
            protofile))
    #t
    (list "aprotoc")))

(define (proto-cc-generated-source config protofile)
  (string-append "gen/proto/" (dirname protofile) "/" (basename protofile ".proto")
                 ".pb" (if (proto-cc-cpp? config) ".cc" ".c")))

(define (proto-cc-generated-header config protofile)
  (string-append "gen/proto/" (dirname protofile) "/" (basename protofile ".proto")
                 ".pb.h"))

(define (generate-cc-proto-rule config protofile)
  (let ((outputs (list (proto-cc-generated-source config protofile)
                       (proto-cc-generated-header config protofile))))
    (generate-proto-rule config protofile outputs)))

(define (extend-proto-with-defaults config)
  ;; TODO: Support plugins.
  (when (proto-config-plugin config)
    (throw 'unsupported-option-plugin))

  (let* ((flags (map (cut string-append "-I" <>) (proto-config-local-include-dirs config)))
         (flags (append flags
                        (map (cut string-append "-I" <>) (proto-config-include-dirs config))))
         (dir "gen/proto")
         (subdir "gen/proto"))
    (extend-proto-config
      config
      `((flags . ,flags)
        (dir . ,dir)
        (subdir . ,subdir)))))

(define (extend-proto-with-cc config get-input)
  (let ((out-type (if (proto-config-plugin config)
                      (proto-config-out-type config)
                      (match (proto-config-type config)
                        ((or "nanopb-c" "nanopb-c-enable_malloc" "nanopb-c-16bit"
                             "nanopb-c-enable_malloc-16bit" "nanopb-c-32bit"
                             "nanopb-c-enable_malloc-32bit")
                         "--nanopb_out")
                        ((or "full" "lite" #f)
                         "--cpp_out")
                        (type (throw 'unknown-type type)))))
        (out-params (if (and (not (proto-config-plugin config))
                             (equal? (proto-config-type config) "lite"))
                        (list "lite")
                        '()))
        (flags (if (and (not (proto-config-plugin config))
                        (member (proto-config-type config)
                                '("nanopb-c" "nanopb-c-enable_malloc" "nanopb-c-16bit"
                                  "nanopb-c-enable_malloc-16bit" "nanopb-c-32bit"
                                  "nanopb-c-enable_malloc-32bit")))
                   (list (string-append "--plugin="
                                        (get-input "protoc-gen-nanopb")))
                   '())))
    (extend-proto-config
      config
      `((out-type . ,out-type)
        (out-params . ,out-params)))))

(define (proto-cc-cpp? config)
  (and
    (not (proto-config-plugin config))
    (not (member (proto-config-type config)
                 '("nanopb-c" "nanopb-c-enable_malloc" "nanopb-c-16bit"
                   "nanopb-c-enable_malloc-16bit" "nanopb-c-32bit"
                   "nanopb-c-enable_malloc-32bit")))))

(define (extend-proto-with-java config get-input)
  (let ((out-type (if (proto-config-plugin config)
                      (proto-config-out-type config)
                      (match (proto-config-type config)
                        ("micro" "--javamicro_out")
                        ("nano" "--javanano_out")
                        ((or "lite" "full" #f) "--java_out")
                        (type (throw 'unknown-type type)))))
        (out-params (if (and (not (proto-config-plugin config))
                             (equal? (proto-config-type config) "lite"))
                        (list "lite")
                        '()))
        (flags (if (not (proto-config-plugin config))
                   (match (proto-config-type config)
                     ("micro" "--plugin=protoc-gen-javamicro")
                     ("nano" "--plugin=protoc-gen-javanano")
                     (_ '()))
                   '())))
    (extend-proto-config
      config
      `((out-type . ,out-type)
        (out-params . ,out-params)))))

(define (proto-java-generated-source config protofile)
  (string-append "gen/proto/" (dirname protofile) "/" (basename protofile ".proto")
                 ".pb.java"))

(define (generate-java-proto-rule config protofile)
  (let ((outputs (list (proto-java-generated-source config protofile))))
    (generate-proto-rule config protofile outputs)))
