;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2022 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android build java-intellij)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (sxml simple)
  #:use-module (guix build utils)
  #:export (set-source-dir
            copy-resources
            get-resources))

(define (get-dependencies iml)
  "Given an IML file, return the dependencies declared in it."
  (let ((doc (with-input-from-file iml
               (lambda _ (xml->sxml #:trim-whitespace? #t)))))
    (match doc
      (('*TOP* ('*PI* _ ...) ('module ('@ _ ...) component ...))
       (let loop ((components component))
         (match components
           (()
            (error
              (format #f "This module does not define a project: ~a"
                      iml)))
           ((('component ('@ attrs ...) tags ...) components ...)
            (if (equal? (assoc-ref attrs 'name) '("NewModuleRootManager"))
                (let loop2 ((tags tags) (dependencies '()))
                  (match tags
                    (() dependencies)
                    ((('orderEntry ('@ attrs ...)) . tags)
                     (match (assoc-ref attrs 'type)
                       (("module")
                        (if (assoc-ref attrs 'scope)
                            (loop2 tags dependencies)
                            (loop2 tags
                                   (cons (car (assoc-ref attrs 'module-name))
                                         dependencies))))
                       (("library")
                        (format #t "External dependency: ~a~%"
                                (car (assoc-ref attrs 'name)))
                        (loop2 tags dependencies))
                       (_
                         (loop2 tags dependencies))))
                    ((_ tags ...) (loop2 tags dependencies))))
                (loop components)))))))))

(define (get-sources iml)
  "Given an IML file, return the list of sources it defines."
  (let ((doc (with-input-from-file iml
               (lambda _ (xml->sxml #:trim-whitespace? #t)))))
    (match doc
      (('*TOP* ('*PI* _ ...) ('module ('@ _ ...) component ...))
       (let loop ((components component))
         (match components
           (()
            (error
              (format #f "This module does not define a project: ~a"
                      iml)))
           ((('component ('@ attrs ...) tags ...) components ...)
            (if (equal? (assoc-ref attrs 'name) '("NewModuleRootManager"))
                (let loop2 ((tags tags) (sources '()))
                  (match tags
                    (() sources)
                    ((('content ('@ attrs ...) . content) tags ...)
                     (let ((base (car (assoc-ref attrs 'url))))
                       (let loop3 ((content content) (locations '()))
                         (match content
                           (() (loop2 tags (append sources locations)))
                           ((('sourceFolder ('@ attrs ...)) content ...)
                            (let ((test? (equal? (assoc-ref attrs 'isTestSource)
                                                 '("true")))
                                  (url (car (assoc-ref attrs 'url))))
                              (if (or test? (assoc-ref attrs 'type))
                                  (loop3 content locations)
                                  (loop3 content
                                         (cons (substring url 20)
                                               locations)))))
                           ((_ content ...) (loop3 content locations))))))
                    ((_ tags ...) (loop2 tags sources))))
                (loop components)))))))))

(define (get-resources iml)
  "Given an IML file, return the list of sources it defines."
  (let ((doc (with-input-from-file iml
               (lambda _ (xml->sxml #:trim-whitespace? #t)))))
    (match doc
      (('*TOP* ('*PI* _ ...) ('module ('@ _ ...) component ...))
       (let loop ((components component))
         (match components
           (()
            (error
              (format #f "This module does not define a project: ~a"
                      iml)))
           ((('component ('@ attrs ...) tags ...) components ...)
            (if (equal? (assoc-ref attrs 'name) '("NewModuleRootManager"))
                (let loop2 ((tags tags) (sources '()))
                  (match tags
                    (() sources)
                    ((('content ('@ attrs ...) . content) tags ...)
                     (let ((base (car (assoc-ref attrs 'url))))
                       (let loop3 ((content content) (locations '()))
                         (match content
                           (() (loop2 tags (append sources locations)))
                           ((('sourceFolder ('@ attrs ...)) content ...)
                            (if (equal? (assoc-ref attrs 'type) '("java-resource"))
                                (let ((url (car (assoc-ref attrs 'url))))
                                  (loop3 content
                                         (cons (substring url 20)
                                               locations)))
                                (loop3 content locations)))
                           ((_ tags ...) (loop3 content locations))))))
                    ((_ tags ...) (loop2 tags sources))))
                (loop components)))))))))

(define (get-iml module-name)
  "Return the IML filename corresponding to MODULE-NAME."
  (match (filter
           (lambda (file)
             (not (string-contains file "testData")))
           (find-files "." (string-append "^" module-name ".iml$")))
    (() (error (format #f "Module not found: ~a" module-name)))
    ((iml) iml)
    (_ (error (format #f "Too many modules with the same name: ~a"
                      module-name)))))

(define (gather-recursive-dependencies modules)
  "Return the list of MODULES and their recursive dependencies"
  (let loop ((resolved '()) (unresolved modules))
    (match unresolved
      (() resolved)
      ((module unresolved ...)
       (if (member module resolved)
           (loop resolved unresolved)
           (let ((dependencies (get-dependencies (get-iml module))))
             (loop (cons module resolved)
                   (append unresolved dependencies))))))))

(define (set-source-dir modules)
  (lambda _
    (let ((sources (gather-recursive-dependencies modules)))
      (substitute* "build.xml"
        (("SOURCEDIR")
         (pk 'source-dir
         (string-join
           (append-map
             (lambda (module)
               (let ((iml (get-iml module)))
                 (map
                   (lambda (dir)
                     (string-append (dirname iml) "/" dir))
                   (get-sources iml))))
             sources)
           ":")))))))

(define (copy-resources modules)
  (lambda _
    (let* ((modules (gather-recursive-dependencies modules)))
      (for-each
        (lambda (module)
          (let* ((iml (get-iml module))
                 (dir (dirname iml)))
            (for-each
              (lambda (res)
                (copy-recursively (string-append dir "/" res) "build/classes"))
              (get-resources iml))
            (for-each
              (lambda (src)
                (let ((meta-inf (string-append dir "/" src "/META-INF"))
                      (messages (string-append dir "/" src "/messages")))
                  (when (file-exists? meta-inf)
                    (copy-recursively meta-inf "build/classes/META-INF"))
                  (when (file-exists? messages)
                    (copy-recursively messages "build/classes/messages"))))
              (get-sources iml))))
        modules))))
