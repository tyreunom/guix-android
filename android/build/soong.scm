;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020, 2021 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (android build soong)
  #:use-module (android build blueprint)
  #:use-module (android build soong makefile)
  #:use-module (android build soong cc)
  #:use-module (android build soong genrule)
  #:use-module (android build soong java)
  #:use-module (guix build utils)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (expand-srcs
            get-makefile
            resolve-dep
            dummy-resolve-dep))

(define (expand-src-list srcs get-input)
  "Replaces @var{srcs} entries such as :foo with the output of the package,
and patterns such as src/*.c with the corresponding files."
  (define (predicate name)
    (let* ((name (string-join (string-split name #\.) "\\."))
           (name (string-join (string-split name #\*) ".*")))
      (file-name-predicate name)))

  (define (find-pat dir pat)
    (match pat
      (() '())
      ((file pat ...)
       (cond
         ((equal? file "**")
          ;; ** = . || */**
          (append
            (find-pat dir pat)
            (find-pat dir (cons* "*" "**" pat))))
         ((string-prefix? "**" file)
          ;; **.java => *.java || **/*.java
          (append
            (find-pat dir (cons (string-append "*" (substring file 2)) pat))
            (find-pat dir (cons* "**" (string-append "*" (substring file 2)) pat))))
         ((string-contains file "**")
          ;; foo**bar => foo*bar || foo/**bar
          (let ((pos (string-contains file "**")))
            (append
              (find-pat dir (string-append (substring file 0 pos)
                                           (substring file (+ pos 1))))
              (find-pat dir (cons
                              (string-append (substring file 0 pos) "*")
                              (substring file pos))))))
         ((null? pat)
          ;; file pattern
          (let ((pred (predicate file)))
            (let loop ((dirstream (opendir dir)) (res '()))
              (let ((f (readdir dirstream)))
                (if (eof-object? f)
                    res
                    (let* ((full-f (string-append dir "/" f))
                           (st (stat full-f))
                           (type (stat:type st)))
                      (loop dirstream
                            (if (and (equal? type 'regular) (pred f stat))
                                (cons full-f res)
                                res))))))))
         ((equal? file ".")
          ;; ./... -> ...
          (find-pat dir pat))
         ((not (string-contains file "*"))
          ;; single directory
          (find-pat (string-append dir "/" file) pat))
         (else
          ;; directory pattern, followed by other patterns
          (let ((pred (predicate file)))
            (let loop ((dirstream (opendir dir)) (res '()))
              (let ((f (readdir dirstream)))
                (if (eof-object? f)
                    res
                    (let* ((full-f (string-append dir "/" f))
                           (st (stat full-f))
                           (type (stat:type st)))
                      (loop dirstream
                            (if (and (equal? type 'directory)
                                     (not (equal? f "..")) (not (equal? f "."))
                                     (pred f stat))
                                (append (find-pat full-f pat) res)
                                res))))))))))))
  (flatten
    (map
      (lambda (src)
        (cond
         ((string-prefix? ":" src)
          (let* ((input (substring src 1))
                 (files
                   (filter
                     (lambda (file)
                       (and (not (string-suffix? "/Android.bp" file))
                            (not (string-contains file "share/doc"))
                            (not (string-suffix? "etc/ld.so.cache" file))))
                     (find-files (get-input input) "."))))
            files))
         ((string-any #\* src)
          (let ((pat (string-split src #\/)))
            (find-pat "." pat)))
         (else src)))
      srcs)))

(define (expand-srcs srcs excludes get-input)
  (let ((srcs (expand-src-list srcs get-input))
        (excludes (expand-src-list excludes get-input)))
    (filter (lambda (src) (not (member src excludes))) srcs)))

(define* (get-makefile get-input module module-type module-name system target
                       #:key (dry-run? #f))
  (let ((generate-makefile
          (match module-type
            ((? (lambda (s) (or (string-prefix? "cc_" s) (string-prefix? "art_cc_" s))) type)
             generate-cc-makefile)
            ((or "genrule" "java_genrule")
             generate-genrule-makefile)
            ((? (lambda (s) (string-prefix? "java_" s)) type)
             generate-java-makefile)
            ((? (lambda (s) (string-suffix? "_defaults" s)) type)
             (const (make-makefile '() '() '())))))
        (config
          (match module-type
            ((? (lambda (s) (or (string-prefix? "cc_" s) (string-prefix? "art_cc_" s))) type)
             (blueprint-module->cc-config module))
            ((or "genrule" "java_genrule")
             (blueprint-module->genrule-config module))
            ((? (lambda (s) (string-prefix? "java_" s)) type)
             (blueprint-module->java-config module))
            ((? (lambda (s) (string-suffix? "_defaults" s)) type)
             #t))))
    (generate-makefile config
      #:get-input get-input
      #:module-type module-type
      #:name module-name
      #:system system
      #:target target
      #:dry-run? dry-run?)))

(define (resolve-dep inputs)
  (lambda (name)
    (or (assoc-ref inputs name)
        (assoc-ref inputs (android-name->guix-name name))
        (throw 'no-dep name))))

(define (dummy-resolve-dep name)
  (string-append "xxx-dummy-" name))
