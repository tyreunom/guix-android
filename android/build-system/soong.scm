;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020, 2021 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
(define-module (android build-system soong)
  #:use-module (android build blueprint)
  #:use-module (guix build-system)
  #:use-module (guix build-system gnu)
  #:use-module (guix derivations)
  #:use-module (guix gexp)
  #:use-module (guix monads)
  #:use-module (guix packages)
  #:use-module (guix search-paths)
  #:use-module (guix store)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:re-export (%latest-android-10
               %latest-android-11
               %latest-android-12
               %latest-android-13)
  #:export (%soong-build-system-modules
            lower
            soong-build
            soong-build-system))

;; Commentary:
;;
;; Standard build procedure for packages using soong, the Android build system.
;; This is implemented as an extension of `gnu-build-system'.
;;
;; Android modules (packages) use soong as their standard build system.  Build
;; instructions are described in an Android.bp file that soong reads to
;; implement its various build procedures.
;;
;; Since soong itself makes many assumptions as to what is available and where
;; (including that the package and all its dependencies are available as sources
;; in a single hierarchy), we cannot build and use it easily.  Instead, we
;; re-implement all the various types of modules inside this build system.
;;
;; Code:

(define %soong-build-system-modules
  ;; Build-side modules imported by default.
  `((android build soong-build-system)
    (android build blueprint)
    (android build soong)
    (android build soong makefile)
    (android build soong cc)
    (android build soong genrule)
    (android build soong java)
    (android build soong proto)
    ,@%gnu-build-system-modules))

(define (default-clang)
  "Return the default clang package."

  ;; Do not use `@' to avoid introducing circular dependencies.
  (let ((module (resolve-interface '(gnu packages llvm))))
    (module-ref module 'clang-14)))

(define (default-lld)
  "Return the default lld package."

  ;; Do not use `@' to avoid introducing circular dependencies.
  (let ((module (resolve-interface '(gnu packages llvm))))
    (module-ref module 'lld-wrapper)))

(define (default-llvm)
  "Return the default llvm package."

  ;; Do not use `@' to avoid introducing circular dependencies.
  (let ((module (resolve-interface '(gnu packages llvm))))
    (module-ref module 'llvm-12)))

(define (default-libcxx)
  "Return the default libcxx package."

  ;; Do not use `@' to avoid introducing circular dependencies.
  (let ((module (resolve-interface '(android packages clang))))
    (module-ref module 'libcxx-12)))

(define (default-libcxxabi)
  "Return the default libcxx package."

  ;; Do not use `@' to avoid introducing circular dependencies.
  (let ((module (resolve-interface '(android packages clang))))
    (module-ref module 'libcxxabi-12)))

(define (default-header-packages)
  "Return the default list of header packages."

  ;; Do not use `@' to avoid introducing circular dependencies.
  (let ((module (resolve-interface '(android packages android-headers))))
    (module-ref module '%global-android-host-headers)))

(define (default-jdk)
  "Return the default openjdk package."

  ;; Do not use `@' to avoid introducing circular dependencies.
  (let ((module (resolve-interface '(gnu packages java))))
    (module-ref module 'openjdk9)))

(define (default-zip)
  "Return the default zip package."

  ;; Do not use `@' to avoid introducing circular dependencies.
  (let ((module (resolve-interface '(gnu packages compression))))
    (module-ref module 'zip)))

(define (default-unzip)
  "Return the default unzip package."

  ;; Do not use `@' to avoid introducing circular dependencies.
  (let ((module (resolve-interface '(gnu packages compression))))
    (module-ref module 'unzip)))

(define default-clang-for-target
  (let ((module (resolve-interface '(android packages clang))))
    (module-ref module 'clang-for-target)))

(define (default-cross-ld-wrapper target)
  (let* ((cross-module (resolve-interface '(gnu packages cross-base)))
         (module (resolve-interface '(gnu packages base)))
         (make-ld-wrapper (module-ref module 'make-ld-wrapper))
         (cross-binutils (module-ref cross-module 'cross-binutils)))
    (make-ld-wrapper (string-append "ld-wrapper-" target)
                     #:target (const target)
                     #:binutils (cross-binutils target))))

(define* (lower name
                #:key source inputs native-inputs outputs system target
                module-type
                (clang (default-clang-for-target (default-clang)))
                (cross-libc? #t)
                (llvm (default-llvm))
                (libcxx (default-libcxx))
                (libcxxabi (default-libcxxabi))
                (lld (default-lld))
                (jdk (default-jdk))
                (zip (default-zip))
                (unzip (default-unzip))
                (header-packages (default-header-packages))
                #:allow-other-keys
                #:rest arguments)
  "Return a bag for NAME."
  (define private-keywords
    '(#:inputs #:native-inputs #:clang #:cross-libc? #:lld
      #:llvm #:jdk #:zip #:unzip #:libcxx #:libcxxabi #:header-packages))

  (bag
    (name name)
    (system system)
    (target target)
    (host-inputs (if target
                     (append
                       `(,@(if libcxx `(("libcxx" ,libcxx)) '())
                         ,@(if libcxxabi `(("libcxxabi" ,libcxxabi)) '()))
                       header-packages
                       inputs)
                     '()))
    (build-inputs
      (append
        (if source
             `(("source" ,source))
             '())
        (match module-type
          ((? (lambda (s) (or (string-prefix? "cc_" s) (string-prefix? "art_cc_" s))) type)
           `(("clang" ,clang)
             ,@(if lld `(("lld" ,lld)) '())
             ,@(if llvm `(("llvm" ,llvm)) '())))
          ((? (lambda (s) (string-prefix? "genrule" s)) type)
           '())
          ((? (lambda (s) (string-prefix? "java_" s)) type)
           `(("jdk" ,jdk "jdk")
             ("zip" ,zip)
             ("unzip" ,unzip)))
          (_ '()))
        (if target
            '()
            `(,@(if libcxx `(("libcxx" ,libcxx)) '())
              ,@(if libcxxabi `(("libcxxabi" ,libcxxabi)) '())))
        (if target '() header-packages)
        native-inputs
        (if target '() inputs)
        (if target
            (let ((cross-packages (standard-cross-packages target 'host)))
              (cons
                ;; make sure this is first, so clang can find cross-ld-wrapper
                (list "ld-wrapper-cross" (default-cross-ld-wrapper target))
                (alist-delete "cross-gcc" cross-packages)))
            '())
        (alist-delete "gcc" (standard-packages))))
    (target-inputs
      (if target
          (append
            (if target
                `(,@(if libcxx `(("libcxx" ,libcxx)) '())
                  ,@(if libcxxabi `(("libcxxabi" ,libcxxabi)) '()))
                '())
            (if cross-libc?
                (alist-delete "cross-gcc" (standard-cross-packages target 'target))
                '())
            header-packages)
          '()))
    (outputs outputs)
    (build (if target soong-cross-build soong-build))
    (arguments (strip-keyword-arguments private-keywords arguments))))

(define* (soong-build name inputs
                      #:key
                      source
                      (guile #f)
                      (outputs '("out"))
                      (configure-flags ''())
                      (search-paths '())
                      (make-flags ''())
                      (module-type #f)
                      (module #f)
                      (blueprint #f)
                      (base-package #f)
                      (validate-runpath? #t)
                      (patch-shebangs? #t)
                      (strip-binaries? #t)
                      (strip-flags ''("--strip-debug"))
                      (strip-directories ''("lib" "lib64" "libexec"
                                            "bin" "sbin"))
                      (phases '(@ (android build soong-build-system)
                                  %standard-phases))
                      (system (%current-system))
                      (target (%current-target-system))
                      (imported-modules %soong-build-system-modules)
                      (modules '((android build soong-build-system)
                                 (android build soong makefile)
                                 (android build soong cc)
                                 (android build soong genrule)
                                 (android build soong java)
                                 (android build blueprint)
                                 (guix build utils))))
  "Build SOURCE that normally uses soong, and with INPUTS."
  (define builder
    (with-imported-modules imported-modules
      #~(begin
          (use-modules #$@(sexp->gexp modules))
          (soong-build #:source #+source
                       #:system #$system
                       #:target #$target
                       #:outputs #$(outputs->gexp outputs)
                       #:inputs #$(input-tuples->gexp inputs)
                       #:search-paths '#$(sexp->gexp
                                           (map search-path-specification->sexp
                                                search-paths))
                       #:phases #$phases
                       #:make-flags #$make-flags
                       #:module-type #$module-type
                       #:module #$module
                       #:blueprint #$blueprint
                       #:base-package #$base-package
                       #:validate-runpath? #$validate-runpath?
                       #:patch-shebangs? #$patch-shebangs?
                       #:strip-binaries? #$strip-binaries?
                       #:strip-flags #$strip-flags
                       #:strip-directories #$strip-directories))))

  (mlet %store-monad ((guile (package->derivation (or guile (default-guile))
                                                  system #:graft? #f)))
    (gexp->derivation name builder
                      #:system system
                      #:guile-for-build guile)))

(define* (soong-cross-build name
                            #:key
                            target
                            build-inputs target-inputs host-inputs
                            (guile #f)
                            source
                            (outputs '("out"))
                            (configure-flags ''())
                            (search-paths '())
                            (native-search-paths '())
                            (make-flags ''())
                            (module-type #f)
                            (module #f)
                            (blueprint #f)
                            (base-package #f)
                            (validate-runpath? #t)
                            (patch-shebangs? #t)
                            (strip-binaries? #t)
                            (strip-flags ''("--strip-debug"))
                            (strip-directories ''("lib" "lib64" "libexec"
                                                  "bin" "sbin"))
                            (phases '(@ (android build soong-build-system)
                                        %standard-phases))
                            (system (%current-system))
                            (imported-modules %soong-build-system-modules)
                            (modules '((android build soong-build-system)
                                       (android build soong makefile)
                                       (android build soong cc)
                                       (android build soong genrule)
                                       (android build soong java)
                                       (guix build utils))))
  "Cross-build SOURCE that normally uses soong, and with INPUTS."
  (define builder
    (with-imported-modules imported-modules
      #~(begin
          (use-modules #$@(sexp->gexp modules))

          (define %build-host-inputs
            #+(input-tuples->gexp build-inputs))

          (define %build-target-inputs
            (append #$(input-tuples->gexp host-inputs)
                    #+(input-tuples->gexp target-inputs)))

          (soong-build #:source #+source
                       #:system #$system
                       #:target #$target
                       #:outputs #$(outputs->gexp outputs)
                       #:inputs %build-target-inputs
                       #:native-inputs %build-host-inputs
                       #:search-paths '#$(sexp->gexp
                                           (map search-path-specification->sexp
                                                search-paths))
                       #:native-search-paths '#$(sexp->gexp
                                                  (map
                                                   search-path-specification->sexp
                                                   native-search-paths))
                       #:phases #$phases
                       #:make-flags #$make-flags
                       #:module-type #$module-type
                       #:module #$module
                       #:blueprint #$blueprint
                       #:base-package #$base-package
                       #:validate-runpath? #$validate-runpath?
                       #:patch-shebangs? #$patch-shebangs?
                       #:strip-binaries? #$strip-binaries?
                       #:strip-flags #$strip-flags
                       #:strip-directories #$strip-directories))))

  (mlet %store-monad ((guile (package->derivation (or guile (default-guile))
                                                  system #:graft? #f)))
    (gexp->derivation name builder
                      #:system system
                      #:target target
                      #:guile-for-build guile)))


(define soong-build-system
  (build-system
    (name 'soong)
    (description "The standard Soong build system for Android")
    (lower lower)))

;;; soong.scm ends here
